<?php

$to = 'joseph@radicalcinema.org';
$subject = "Confirmez votre publication";
$title = "screugneugneu d'article";
$signature = "Joseph Paris";
$twauteur = "josephparis";

$headers = "From: Radical Cinema <no-reply@radicalcinema.org> \r\n";
$headers .= "Reply-To: no-reply@radicalcinema.org \r\n";
$headers .= "MIME-Version: 1.0\r\n";
$headers .= "Content-Type: text/html; charset=UTF-8\r\n";

$message_fr = "<p><b>Bonjour,</b></p>";
$message_fr .= "<p>Merci de bien vouloir confirmer votre proposition de publication :</p>";
$message_fr .= "<p>Titre : <b>$title</b></p>";
$message_fr .= "<p>Signature : <b>$signature</b></p>";
$message_fr .= "<p>Twitter : <b>@$twauteur</b></p>";

$message_en = "<p><b>Hello,</b></p>";
$message_en .= "<p>Please confirm your publication proposal:</p>";
$message_en .= "<p>Title: <b>$title</b></p>";
$message_en .= "<p>Signature: <b>$signature</b></p>";
$message_en .= "<p>Twitter: <b>@$twauteur</b></p>";



$html_message = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">
<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"fr\">";
$html_message .= '<head><meta charset="UTF-8" /><meta http-equiv="X-UA-Compatible" content="IE=edge"><meta name="viewport" content="width=device-width, initial-scale=1">';
$html_message .= "<style>body,html{-webkit-font-smoothing:antialiased}.logo,.logo a{display:inline-block}body,html{height:100%}body{color:#333;font-family:AvenirLTStd-Roman,Arial,serif;font-size:14px;line-height:25px;background:#f8f8f8;margin:0;text-align:center}.content{width:100%;max-width:400px;margin:40px auto;text-align:left}.contentmessage{padding:40px 10px 20px;line-height:20px}.contentmessage p{padding:0;line-height:20px}.logo,.logo a,.sidebarLogo{padding:0!important}.sidebarLogo{margin:0 auto!important;text-align:center}.logo,.logo a,.logo a span{margin:0!important}.logo{font-family:'Source Sans Pro',sans-serif;font-weight:600;text-transform:uppercase;line-height:20px!important}.logo a{color:#444;background:0 0;text-decoration:none;position:relative;white-space:nowrap;border:0!important;letter-spacing:1px;font-size:13px;float:left;-webkit-transition:all 1s ease-out;-moz-transition:all 1s ease-out;-ms-transition:all 1s ease-out;-o-transition:all 1s ease-out;transition:all 1s ease-out}.logo a span{padding:7px 10px;border:2px solid #626262;-webkit-transition:all .5s ease-out;-moz-transition:all .5s ease-out;-ms-transition:all .5s ease-out;-o-transition:all .5s ease-out;transition:all .5s ease-out}.logo a:hover,.logo a:hover span{-webkit-transition:all .5s ease-out;-moz-transition:all .5s ease-out;-ms-transition:all .5s ease-out;-o-transition:all .5s ease-out}.logo a:hover{transition:all .5s ease-out}.logo a:hover span{color:#000;border:2px solid #000;transition:all .5s ease-out}a.buttongreen,a.buttonred{margin:0 10px;display:inline-block;padding:6px 30px;border-width:1px;border-style:solid;font-family:Arial,Helvetica,Sans-serif;font-size:14px;color:#fff;font-weight:700;text-decoration:none}a.buttongreen{background:#66ab04;-webkit-box-shadow:0 1px 0 0 #82ba31 inset,0 0 0 4px #e6e6e6;-moz-box-shadow:0 1px 0 0 #82ba31 inset,0 0 0 4px #e6e6e6;box-shadow:0 1px 0 0 #82ba31 inset,0 0 0 4px #e6e6e6;-webkit-border-radius:2px;-moz-border-radius:2px;border-radius:2px;-webkit-transition:all .2s ease-in-out;-moz-transition:all .2s ease-in-out;-o-transition:all .2s ease-in-out;transition:all .2s ease-in-out;text-shadow:0 1px 1px #497a03;border-color:#5b9904}a.buttongreen:hover{background:#9bce00;-webkit-box-shadow:0 1px 0 0 #add72e inset,0 0 0 4px #d5efab;-moz-box-shadow:0 1px 0 0 #add72e inset,0 0 0 4px #d5efab;box-shadow:0 1px 0 0 #add72e inset,0 0 0 4px #d5efab;text-shadow:0 1px 1px #698c00;border-color:#93c300}a.buttonred{background:#f5513c;-webkit-box-shadow:0 1px 0 0 #ff8272 inset,0 0 0 4px #e6e6e6;-moz-box-shadow:0 1px 0 0 #ff8272 inset,0 0 0 4px #e6e6e6;box-shadow:0 1px 0 0 #ff8272 inset,0 0 0 4px #e6e6e6;-webkit-border-radius:2px;-moz-border-radius:2px;border-radius:2px;-webkit-transition:all 0s 0;-moz-transition:all 0s 0;-o-transition:all 0s 0;transition:all 0s 0;text-shadow:0 1px 1px #9c3528;border-color:#ec513c}a.buttonred:hover{background:#ff6551;-webkit-box-shadow:0 0 0 4px #ffbfab;-moz-box-shadow:0 0 0 4px #ffbfab;box-shadow:0 0 0 4px #ffbfab;border-color:#ed5e4b}</style></head><body>";

$html_message .= '<div class="content"><div class="sidebarLogo"><div class="logo"><a href="https://radicalcinema.org/"><span>Radical Cinema</span></a></div></div>';
$html_message .= '<div class="contentmessage">'.$message_fr.'</div>';
$html_message .= '<p style="text-align:center;padding-bottom:40px;"><a href="https://radicalcinema.org/sandbox/article_identify.php?tk='.$token.'&tki='.$token_identity.'&confirm=yes" class="buttongreen">Confirmer</a> <a href="https://radicalcinema.org/sandbox/article_identify.php?tk='.$token.'&tki='.$token_identity.'&confirm=no" class="buttonred">Annuler</a></p></div></body></html>';

mail($to, $subject, $html_message, $headers);

echo "email envoyé";
?>




