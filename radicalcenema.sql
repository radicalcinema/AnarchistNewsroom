-- phpMyAdmin SQL Dump
-- version 4.4.13.1
-- http://www.phpmyadmin.net
--
-- Client :  radicalcenema.mysql.db
-- Généré le :  Sam 30 Avril 2016 à 17:15
-- Version du serveur :  5.5.46-0+deb7u1-log
-- Version de PHP :  5.4.45-0+deb7u2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `radicalcenema`
--

-- --------------------------------------------------------

--
-- Structure de la table `rc_articles`
--

CREATE TABLE IF NOT EXISTS `rc_articles` (
  `id` int(11) NOT NULL,
  `token` varchar(250) NOT NULL,
  `id_user` int(11) NOT NULL,
  `token_user` varchar(250) NOT NULL,
  `edition` varchar(20) NOT NULL,
  `tag` varchar(50) NOT NULL,
  `titre` varchar(250) NOT NULL,
  `description` varchar(250) NOT NULL,
  `edito` text NOT NULL,
  `texte` text NOT NULL,
  `signature` varchar(100) NOT NULL,
  `auteur_edito` varchar(250) NOT NULL,
  `traducteur` varchar(250) NOT NULL,
  `statut` varchar(100) NOT NULL,
  `date_save` datetime NOT NULL,
  `deadline_proposal` datetime NOT NULL,
  `date_published` datetime NOT NULL,
  `license` varchar(200) NOT NULL,
  `twauteur` varchar(250) NOT NULL,
  `twauteuredito` varchar(50) NOT NULL,
  `thumb_photographer` varchar(250) NOT NULL,
  `thumb_license` varchar(250) NOT NULL,
  `thumb_legende` varchar(250) NOT NULL,
  `thumb` varchar(250) NOT NULL,
  `message` text NOT NULL,
  `soundcloud` text NOT NULL,
  `slug` varchar(250) NOT NULL,
  `index_slug` int(11) NOT NULL,
  `vimeo1` varchar(100) NOT NULL,
  `vimeo2` varchar(100) NOT NULL,
  `vimeo3` varchar(100) NOT NULL,
  `vimeo4` varchar(100) NOT NULL,
  `youtube1` varchar(100) NOT NULL,
  `youtube2` varchar(100) NOT NULL,
  `youtube3` varchar(100) NOT NULL,
  `youtube4` varchar(100) NOT NULL,
  `identity` varchar(70) NOT NULL,
  `email` varchar(200) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `rc_articles_modifs`
--

CREATE TABLE IF NOT EXISTS `rc_articles_modifs` (
  `id` int(11) NOT NULL,
  `id_article` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `detail` varchar(250) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `rc_articles_views`
--

CREATE TABLE IF NOT EXISTS `rc_articles_views` (
  `id` int(11) NOT NULL,
  `id_article` int(11) NOT NULL,
  `id_reply` int(11) NOT NULL,
  `id_user` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `rc_avis`
--

CREATE TABLE IF NOT EXISTS `rc_avis` (
  `id` int(11) NOT NULL,
  `id_thread` int(11) NOT NULL,
  `id_article` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `avis` varchar(50) NOT NULL,
  `date` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `rc_briefs`
--

CREATE TABLE IF NOT EXISTS `rc_briefs` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `lang` varchar(10) NOT NULL,
  `titre` varchar(100) NOT NULL,
  `message` text NOT NULL,
  `embed` text NOT NULL,
  `date` datetime NOT NULL,
  `statut` varchar(50) NOT NULL,
  `slug` varchar(250) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `rc_briefs_views`
--

CREATE TABLE IF NOT EXISTS `rc_briefs_views` (
  `id` int(11) NOT NULL,
  `id_brief` int(11) NOT NULL,
  `id_reply` int(11) NOT NULL,
  `id_user` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `rc_colleges`
--

CREATE TABLE IF NOT EXISTS `rc_colleges` (
  `id` int(11) NOT NULL,
  `titre` varchar(100) NOT NULL,
  `title` varchar(200) NOT NULL,
  `description` varchar(250) NOT NULL,
  `tag` varchar(100) NOT NULL,
  `tarif_simple` int(11) NOT NULL,
  `tarif_reduit` int(11) NOT NULL,
  `tarif_structure` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



INSERT INTO `rc_colleges` (`id`, `titre`, `title`, `description`, `tag`, `tarif_simple`, `tarif_reduit`, `tarif_structure`) VALUES
(1, 'CollÃ¨ge solidaire', 'Solidarity College', '', 'sol', 0, 0, 0),
(2, 'CollÃ¨ge des membres permanents', 'College of permanent members', '', 'perm', 50, 20, 100),
(3, 'CollÃ¨ge des membres actifs', 'College of active members', '', 'act', 50, 20, 100),
(4, 'CollÃ¨ge des membres bienfaiteurs', 'College of benefactors', '', 'bien', 200, 200, 300),
(5, 'CollÃ¨ge des membres d''honneur', 'College of Honorary Members', '', 'hon', 0, 0, 0),
(6, 'CollÃ¨ge des membres spectateurs', '', '', 'spec', 15, 10, 50);



-- --------------------------------------------------------

--
-- Structure de la table `rc_compta`
--

CREATE TABLE IF NOT EXISTS `rc_compta` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `description` varchar(250) NOT NULL,
  `montant` decimal(10,2) NOT NULL,
  `type` varchar(50) NOT NULL,
  `date` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `rc_cotisations`
--

CREATE TABLE IF NOT EXISTS `rc_cotisations` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `montant` int(11) NOT NULL,
  `paiement` varchar(50) NOT NULL,
  `statut` varchar(50) NOT NULL,
  `date_debut` datetime NOT NULL,
  `date_paiement` datetime NOT NULL,
  `ref` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `rc_donations`
--

CREATE TABLE IF NOT EXISTS `rc_donations` (
  `id` int(11) NOT NULL,
  `prenom` varchar(50) NOT NULL,
  `nom` varchar(50) NOT NULL,
  `email` varchar(150) NOT NULL,
  `montant` int(11) NOT NULL,
  `statut` varchar(50) NOT NULL,
  `date` date NOT NULL,
  `id_user` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `rc_events`
--

CREATE TABLE IF NOT EXISTS `rc_events` (
  `id` int(11) NOT NULL,
  `slug` varchar(250) NOT NULL,
  `date` datetime NOT NULL,
  `capacite` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `rc_notifications`
--

CREATE TABLE IF NOT EXISTS `rc_notifications` (
  `id` int(11) NOT NULL,
  `id_auteur` int(11) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  `destination` varchar(50) NOT NULL,
  `message` varchar(250) NOT NULL,
  `variable` varchar(250) NOT NULL,
  `lien` varchar(250) NOT NULL,
  `date` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `rc_notifications_vues`
--

CREATE TABLE IF NOT EXISTS `rc_notifications_vues` (
  `id` int(11) NOT NULL,
  `id_notification` int(11) NOT NULL,
  `id_user` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `rc_replies`
--

CREATE TABLE IF NOT EXISTS `rc_replies` (
  `id` int(11) NOT NULL,
  `id_thread` int(11) NOT NULL,
  `id_article` int(11) NOT NULL,
  `id_brief` int(11) NOT NULL,
  `privacy` int(11) NOT NULL,
  `message` text NOT NULL,
  `id_user` int(11) NOT NULL,
  `posted` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `rc_reservations`
--

CREATE TABLE IF NOT EXISTS `rc_reservations` (
  `id` int(11) NOT NULL,
  `event` varchar(250) NOT NULL,
  `prenom` varchar(250) NOT NULL,
  `nom` varchar(250) NOT NULL,
  `email` varchar(250) NOT NULL,
  `nb` int(11) NOT NULL,
  `date_resa` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `rc_ressources_sections`
--

CREATE TABLE IF NOT EXISTS `rc_ressources_sections` (
  `id` int(11) NOT NULL,
  `id_section` int(11) NOT NULL,
  `titre` varchar(200) NOT NULL,
  `last_update` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `rc_tags`
--

CREATE TABLE IF NOT EXISTS `rc_tags` (
  `id` int(11) NOT NULL,
  `titre_fr` varchar(25) NOT NULL,
  `titre_en` varchar(25) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `rc_threads`
--

CREATE TABLE IF NOT EXISTS `rc_threads` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_college` int(11) NOT NULL,
  `vote` varchar(50) NOT NULL,
  `deadline` datetime NOT NULL,
  `replies` int(11) NOT NULL,
  `posted` datetime NOT NULL,
  `last_reply` datetime NOT NULL,
  `statut` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `rc_threads_views`
--

CREATE TABLE IF NOT EXISTS `rc_threads_views` (
  `id` int(11) NOT NULL,
  `id_thread` int(11) NOT NULL,
  `id_reply` int(11) NOT NULL,
  `id_user` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `rc_users`
--

CREATE TABLE IF NOT EXISTS `rc_users` (
  `id` int(11) NOT NULL,
  `token_user` varchar(250) NOT NULL,
  `lang` varchar(5) NOT NULL,
  `id_college` int(11) NOT NULL,
  `email` varchar(250) NOT NULL,
  `password` varchar(250) NOT NULL,
  `prenom` varchar(250) NOT NULL,
  `nom` varchar(250) NOT NULL,
  `solidaire` varchar(50) NOT NULL,
  `statut` varchar(250) NOT NULL,
  `date_inscription` datetime NOT NULL,
  `last_activity` datetime NOT NULL,
  `tarif` varchar(50) NOT NULL,
  `twitter` varchar(100) NOT NULL,
  `facebook` varchar(250) NOT NULL,
  `website` varchar(250) NOT NULL,
  `email2` varchar(100) NOT NULL,
  `notif_articles_fr` int(50) NOT NULL,
  `notif_articles_en` int(11) NOT NULL,
  `notif_ag` int(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `rc_vimeo`
--

CREATE TABLE IF NOT EXISTS `rc_vimeo` (
  `id` int(11) NOT NULL,
  `id_article` int(11) NOT NULL,
  `titre` varchar(200) NOT NULL,
  `vimeo` varchar(50) NOT NULL,
  `autopause` int(11) NOT NULL,
  `autoplay` int(11) NOT NULL,
  `loop` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `rc_votes`
--

CREATE TABLE IF NOT EXISTS `rc_votes` (
  `id` int(11) NOT NULL,
  `id_thread` int(11) NOT NULL,
  `id_article` int(11) NOT NULL,
  `id_brief` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `vote` varchar(50) NOT NULL,
  `date` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Index pour les tables exportées
--

--
-- Index pour la table `rc_articles`
--
ALTER TABLE `rc_articles`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `rc_articles_modifs`
--
ALTER TABLE `rc_articles_modifs`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `rc_articles_views`
--
ALTER TABLE `rc_articles_views`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `rc_avis`
--
ALTER TABLE `rc_avis`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `rc_briefs`
--
ALTER TABLE `rc_briefs`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `rc_briefs_views`
--
ALTER TABLE `rc_briefs_views`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `rc_colleges`
--
ALTER TABLE `rc_colleges`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `rc_compta`
--
ALTER TABLE `rc_compta`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `rc_cotisations`
--
ALTER TABLE `rc_cotisations`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `rc_donations`
--
ALTER TABLE `rc_donations`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `rc_events`
--
ALTER TABLE `rc_events`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `rc_notifications`
--
ALTER TABLE `rc_notifications`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `rc_notifications_vues`
--
ALTER TABLE `rc_notifications_vues`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `rc_replies`
--
ALTER TABLE `rc_replies`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `rc_reservations`
--
ALTER TABLE `rc_reservations`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `rc_ressources_sections`
--
ALTER TABLE `rc_ressources_sections`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `rc_tags`
--
ALTER TABLE `rc_tags`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `rc_threads`
--
ALTER TABLE `rc_threads`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `rc_threads_views`
--
ALTER TABLE `rc_threads_views`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `rc_users`
--
ALTER TABLE `rc_users`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `rc_vimeo`
--
ALTER TABLE `rc_vimeo`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `rc_votes`
--
ALTER TABLE `rc_votes`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `rc_articles`
--
ALTER TABLE `rc_articles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `rc_articles_modifs`
--
ALTER TABLE `rc_articles_modifs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `rc_articles_views`
--
ALTER TABLE `rc_articles_views`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `rc_avis`
--
ALTER TABLE `rc_avis`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `rc_briefs`
--
ALTER TABLE `rc_briefs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `rc_briefs_views`
--
ALTER TABLE `rc_briefs_views`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `rc_colleges`
--
ALTER TABLE `rc_colleges`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `rc_compta`
--
ALTER TABLE `rc_compta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `rc_cotisations`
--
ALTER TABLE `rc_cotisations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `rc_donations`
--
ALTER TABLE `rc_donations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `rc_events`
--
ALTER TABLE `rc_events`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `rc_notifications`
--
ALTER TABLE `rc_notifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `rc_notifications_vues`
--
ALTER TABLE `rc_notifications_vues`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `rc_replies`
--
ALTER TABLE `rc_replies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `rc_reservations`
--
ALTER TABLE `rc_reservations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `rc_ressources_sections`
--
ALTER TABLE `rc_ressources_sections`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `rc_tags`
--
ALTER TABLE `rc_tags`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `rc_threads`
--
ALTER TABLE `rc_threads`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `rc_threads_views`
--
ALTER TABLE `rc_threads_views`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `rc_users`
--
ALTER TABLE `rc_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `rc_vimeo`
--
ALTER TABLE `rc_vimeo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `rc_votes`
--
ALTER TABLE `rc_votes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
