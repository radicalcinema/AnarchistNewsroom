<?php
$private_rule="waiting";
if($lang=="fr"){
$title_page="Paramètres";
}else{ 
$title_page="Settings";
}
$private="yes"; // accès réservé
include("inc/variables.php");
require("inc/login.php"); // Contrôle d'identité :-/ 

						// recup infos
						$sql = mysql_query("SELECT * FROM `".$prefix."users` WHERE `id` = '$id_user_online'");
						while($r = mysql_fetch_array($sql)) {
						$id_user = $r['id'];
						$prenom_user = $r['prenom'];
						$nom_user = $r['nom'];
						$email_user = $r['email'];
						$lang_user = $r['lang'];
						$statut_user=$r['statut'];
						$solidaire_user=$r['solidaire'];
						$college_user=$r['id_college'];
						$twitter_user=$r['twitter'];
						$site_user=$r['website'];
						$facebook_user=$r['facebook'];
						$notif_articles_fr=$r['notif_articles_fr'];
						$notif_articles_en=$r['notif_articles_en'];
						$date_inscription=date("d M Y à H:i", strtotime($r[date_inscription]));
						}// End of Array
						$title_college=nomcollege($college_user,$lang);
						// definition des options obligatoires
						if($college_user==2) { $visibilite="oui"; } // pour les membres permanents
						
if(isset($_POST['change'])){

    $prenom = $_POST['prenom'];
    $nom = $_POST['nom'];
    $email = $_POST['email'];
    $twitter = $_POST['twitter'];
    $facebook = $_POST['facebook'];
    $change_lang = $_POST['change_lang'];
    $siteweb = $_POST['siteweb'];
    $notif_articles_fr = $_POST['notif_articles_fr'];
    $notif_articles_en = $_POST['notif_articles_en'];
    if($prenom == ''){
        $errors[] = "Vous ne pouvez pas effacer votre prénom. Ça se garde toute la vie ce genre de truc..";
    }
    if($nom == ''){
        $errors[] = "Vous ne pouvez pas effacer votre nom, on aimerait bien parfois.";
    }
    if($email == ''){
        $errors[] = "Vous ne pouvez pas effacer votre email.";
    }
    if($solidaire_user == 'oui'){
        $notif_articles_fr = "1";
        $notif_articles_en = "1";
    }
    if($notif_articles_fr == ''){
        $notif_articles_fr = "0";
    }
    if($notif_articles_en == ''){
        $notif_articles_en = "0";
    }
	// definition des options obligatoires
	if($solidaire_user=="oui") { $visibilite="oui"; } // pour les membres permanents: visibilité obligatoire dans la liste des membres


	if(count($errors) == 0){ //si pas d'erreurs, on enregistre
	mysql_query("UPDATE ".$prefix."users SET prenom = '".$prenom."' WHERE id = '".$id_user_online."'");
	mysql_query("UPDATE ".$prefix."users SET nom = '".$nom."' WHERE id = '".$id_user_online."'");
	mysql_query("UPDATE ".$prefix."users SET email = '".$email."' WHERE id = '".$id_user_online."'");
	mysql_query("UPDATE ".$prefix."users SET twitter = '".$twitter."' WHERE id = '".$id_user_online."'");
	mysql_query("UPDATE ".$prefix."users SET lang = '".$change_lang."' WHERE id = '".$id_user_online."'");
	mysql_query("UPDATE ".$prefix."users SET facebook = '".$facebook."' WHERE id = '".$id_user_online."'");
	mysql_query("UPDATE ".$prefix."users SET website = '".$siteweb."' WHERE id = '".$id_user_online."'");
	mysql_query("UPDATE ".$prefix."users SET notif_articles_fr = '".$notif_articles_fr."' WHERE id = '".$id_user_online."'");
	mysql_query("UPDATE ".$prefix."users SET notif_articles_en = '".$notif_articles_en."' WHERE id = '".$id_user_online."'");
	//mysql_query("UPDATE ".$prefix."users SET notif_ag = '".$notif_ag."' WHERE id = '".$id_user_online."'");
	header('Location: settings.php?c=1');
 	}// end pas d'erreur
 	
}// endif(isset($_POST['change']))

include("inc/template_top.php"); // template
?>
  





<?php if($lang=="fr"){ ?> 
<h4 class="sub-header">Mes paramètres</h4>
<?php }else{  ?>
<h4 class="sub-header">My settings</h4>
<?php } ?>


<div class="row no-gutters">
<div class="col-md-7 col-xs-12">
<form class="form-horizontal" role="form" action="settings.php" method="post">



  <div class="form-group">
<?php if($lang=="fr"){ ?> 
    <label for="prenom" class="col-sm-3 control-label">Prénom</label>
<?php }else{  ?>
    <label for="prenom" class="col-sm-3 control-label">First name</label>
<?php } ?>
    <div class="col-sm-9">
      <input type="text" class="form-control" id="prenom" name="prenom" value="<?php echo $prenom_user; ?>">
    </div>
  </div>  
  
  <div class="form-group">
<?php if($lang=="fr"){ ?> 
    <label for="nom" class="col-sm-3 control-label">Nom</label>
<?php }else{  ?>
    <label for="nom" class="col-sm-3 control-label">Name</label>
<?php } ?>
    <div class="col-sm-9">
      <input type="text" class="form-control" id="nom" name="nom" value="<?php echo $nom_user; ?>">
    </div>
  </div>
  
  <div class="form-group">
    <label class="col-sm-3 control-label">Email</label>
    <div class="col-sm-9">
      <input type="text" class="form-control" id="email" name="email" value="<?php echo $email_user; ?>">
    </div>
  </div>
  
  <div class="form-group">
<?php if($lang=="fr"){ ?> 
    <label class="col-sm-3 control-label">Mot de passe</label>
<?php }else{  ?>
    <label class="col-sm-3 control-label">Password</label>
<?php } ?>
    <div class="col-sm-9">
<?php if($lang=="fr"){ ?> 
      <p class="form-control-static cmembre"><a href="changepassword.php" class="btn-dark-small"><i class="fa fa-wrench"></i> Modifier</a></p>
<?php }else{  ?>
      <p class="form-control-static cmembre"><a href="changepassword.php" class="btn-dark-small"><i class="fa fa-wrench"></i> Change</a></p>
<?php } ?>
    </div>
  </div>

  <div class="form-group">
    <label class="col-sm-3 control-label">Langue</label>
    <div class="col-sm-9">
<select name="change_lang" class="form-control">
  <option value="fr"<?php if($lang_user=="fr"){ echo " selected"; } ?>>Français</option>
  <option value="en"<?php if($lang_user=="en"){ echo " selected"; } ?>>English</option>
</select>
    </div>
  </div>



  <div class="form-group">
    <label for="twitter" class="col-sm-3 control-label">Twitter</label>
    <div class="col-sm-9">
			<div class="input-group">
		      <div class="input-group-addon">@</div>
		      <input type="text" class="form-control" id="twitter" name="twitter" value="<?php echo $twitter_user; ?>" placeholder="username">
		    </div>
      
    </div>
  </div>

  <div class="form-group">
    <label for="facebook" class="col-sm-3 control-label">Facebook</label>
    <div class="col-sm-9">
			<div class="input-group">
		      <div class="input-group-addon">https://facebook.com/</div>
		      <input type="text" class="form-control" id="facebook" name="facebook" value="<?php echo $facebook_user; ?>" placeholder="username">
		    </div>
    </div>
  </div>

  <div class="form-group">
<?php if($lang=="fr"){ ?> 
    <label for="siteweb" class="col-sm-3 control-label">Site web</label>
<?php }else{  ?>
    <label for="siteweb" class="col-sm-3 control-label">Website</label>
<?php } ?>
    <div class="col-sm-9">
		      <input type="text" class="form-control" id="siteweb" name="siteweb" value="<?php echo $site_user; ?>" placeholder="http://your-domain.tld">
    </div>
  </div>




  
  <div class="form-group">
<?php if($lang=="fr"){ ?> 
    <label class="col-sm-4 control-label">Date d'adhésion</label>
<?php }else{  ?>
    <label class="col-sm-4 control-label">Subscription date</label>
<?php } ?>
    <div class="col-sm-8">
      <p class="form-control-static"><?php echo $date_inscription; ?></p>
    </div>
  </div>


  <div class="form-group">
<?php if($lang=="fr"){ ?> 
    <label class="col-sm-4 control-label">Collège</label>
<?php }else{  ?>
    <label class="col-sm-4 control-label">College</label>
<?php } ?>
    <div class="col-sm-8">
      <p class="form-control-static"><?php echo $title_college; ?></p>
<?php if($solidaire_user=="oui"){?> 
	<?php if($lang=="fr"){ ?> 
	<p class='small'>Et membre du collège des membres solidaires.</p>
	<?php }else{  ?>
	<p class='small'>And member of the Solidarity College</p>
	<?php } ?>
<?php } ?>


    </div>
  </div>




  <div class="form-group">
    <label class="col-sm-4 control-label">Notifications</label>
    <div class="col-sm-8">

  <div class="checkbox">
    
<?php if($lang=="fr"){ ?> 
<label>
      <input type="checkbox" name="notif_articles_fr" value="1" <?php if($notif_articles_fr=="1"){ echo " checked"; } if($solidaire_user=="oui"){ echo " disabled"; } ?>> Recevoir les propositions de publication de l'édition française
      <?php if($solidaire_user=="oui"){ ?><br /> <span class="small">(obligatoire pour les membres du collège solidaire)</span><?php } ?>
</label>
<label>
      <br /><input type="checkbox" name="notif_articles_en" value="1" <?php if($notif_articles_en=="1"){ echo " checked"; } if($solidaire_user=="oui"){ echo " disabled"; } ?>> Recevoir les propositions de publication de l'edition anglaise
      <?php if($solidaire_user=="oui"){ ?><br /> <span class="small">(obligatoire pour les membres du collège solidaire)</span><?php } ?>
</label>
<?php }else{  ?>
<label>
      <input type="checkbox" name="notif_articles_fr" value="1" <?php if($notif_articles_fr=="1"){ echo " checked"; } if($solidaire_user=="oui"){ echo " disabled"; } ?>> Receive the proposals of publication of the french edition
      <?php if($solidaire_user=="oui"){ ?><br /> <span class="small">(Required for members of the solidarity college)</span><?php } ?>
 </label>
 <label>
      <br /><input type="checkbox" name="notif_articles_en" value="1" <?php if($notif_articles_en=="1"){ echo " checked"; } if($solidaire_user=="oui"){ echo " disabled"; } ?>> Receive the proposals of publication of the english edition
      <?php if($solidaire_user=="oui"){ ?><br /> <span class="small">(Required for members of the solidarity college)</span><?php } ?>
</label>
<?php } ?>
    
  </div>
<?php if($solidaire_user=="oui"){ ?> <?php } ?>

    </div>
  </div>



  <input type="hidden" name="change" value="yes">
<?php if($lang=="fr"){ ?> 
  <div class="padd10"><input type="submit" class="btn-article" value="Modifier" /></div>
<?php }else{  ?>
  <div class="padd10"><input type="submit" class="btn-article" value="Modify" /></div>
<?php } ?>

</form>
</div>
</div>










<?php
include("inc/template_bottom.php"); // insert bas de page
?>