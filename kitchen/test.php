<?php
include("inc/variables.php");
$title="test";
$signature="signature";
$subject = $title;
$to="joseph.kassandre@gmail.com";
//$to="joseph@radicalcinema.org";
$headers = "From: Radical Cinema <no-reply@radicalcinema.org> \r\n";
$headers .= "Reply-To: no-reply@radicalcinema.org \r\n";
$headers .= "MIME-Version: 1.0\r\n";
$headers .= "Content-Type: text/html; charset=UTF-8\r\n";
$p="<p style=\"padding: 0;line-height: 20px;text-align: left\">";

$message_fr = $p."<b>Bonjour,</b></p>";
$message_fr .= $p."Merci de bien vouloir confirmer votre proposition de publication :</p>";
$message_fr .= $p."Titre : <b>$title</b></p>";
$message_fr .= $p."Signature : <b>$signature</b></p>";
if($twauteur){$message_fr .= $p."Twitter : <b>@$twauteur</b></p>";}

$message_en = $p."<b>Hello,</b></p>";
$message_en .= $p."Please confirm your publication proposal:</p>";
$message_en .= $p."Title: <b>$title</b></p>";
$message_en .= $p."Signature: <b>$signature</b></p>";
if($twauteur){$message_en .= $p."Twitter : <b>@$twauteur</b></p>";}



$html_message="<html xmlns=\"http://www.w3.org/1999/xhtml\" xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"fr\"><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" /><meta charset=\"UTF-8\" /><meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\" /><meta name=\"viewport\" content=\"width=device-width, initial-scale=1\" /></head><body style=\"-webkit-font-smoothing: antialiased;height: 100%;color: #333;font-family: AvenirLTStd-Roman, Arial, serif;font-size: 14px;line-height: 25px;background: #f8f8f8;margin: 0;text-align: center\"><div class=\"content\" style=\"width: 100%;max-width: 400px;margin: 40px auto\"><div class=\"sidebarLogo\" style=\"padding: 0 !important;margin: 0 auto !important;text-align: center\"><div class=\"logo\" style=\"display: inline-block;padding: 0 !important;margin: 30px 0px 0px 0px !important;font-family: &quot;Source Sans Pro&quot;, sans-serif;font-weight: 600;text-transform: uppercase;line-height: 20px !important\"><a href=\"https://radicalcinema.org/\" style=\"display: inline-block;padding: 0 !important;margin: 0 !important;color: #444;background: 0 0;text-decoration: none;position: relative;white-space: nowrap;border: 0 !important;letter-spacing: 1px;font-size: 13px;float: left;-webkit-transition: all 1s ease-out;-moz-transition: all 1s ease-out;-ms-transition: all 1s ease-out;-o-transition: all 1s ease-out;transition: all 1s ease-out\"><span style=\"margin: 0 !important;padding: 7px 10px;border: 2px solid #626262;-webkit-transition: all 0.5s ease-out;-moz-transition: all 0.5s ease-out;-ms-transition: all 0.5s ease-out;-o-transition: all 0.5s ease-out;transition: all 0.5s ease-out\">Radical Cinema</span></a></div></div><div class=\"contentmessage\" style=\"padding: 40px 10px 20px;line-height: 20px\">";
if($lang=="en"){
$html_message .= $message_en; 
$html_message .= "</div><div style=\"text-align:center;padding-bottom:40px;\"><a href=\"".$http.$domain.$sandbox."article.php?tk=".$token."&tki=".$token_identity."&confirm=tes\" class=\"buttongreen\" style=\"margin: 0 10px;display: inline-block;padding: 6px 30px;border-width: 1px;border-style: solid;font-family: Arial, Helvetica, Sans-serif;font-size: 14px;color: #fff;font-weight: 700;text-decoration: none;background: #66ab04;-webkit-box-shadow: 0 1px 0 0 #82ba31 inset, 0 0 0 4px #e6e6e6;-moz-box-shadow: 0 1px 0 0 #82ba31 inset, 0 0 0 4px #e6e6e6;box-shadow: 0 1px 0 0 #82ba31 inset, 0 0 0 4px #e6e6e6;-webkit-border-radius: 2px;-moz-border-radius: 2px;border-radius: 2px;-webkit-transition: all 0.2s ease-in-out;-moz-transition: all 0.2s ease-in-out;-o-transition: all 0.2s ease-in-out;transition: all 0.2s ease-in-out;text-shadow: 0 1px 1px #497a03;border-color: #5b9904\"> Confirm</a> <a href=\"".$http.$domain.$sandbox."article.php?tk=".$token."&tki=".$token_identity."&confirm=no\" class=\"buttoncencel\" style=\"margin: 0 10px;display: inline-block;padding: 6px 30px;border-width: 1px;border-style: solid;font-family: Arial, Helvetica, Sans-serif;font-size: 14px;color: #fff;font-weight: 700;text-decoration: none;background: #f4425f;-webkit-box-shadow: 0 1px 0 0 #ba307b inset, 0 0 0 4px #e6e6e6;-moz-box-shadow: 0 1px 0 0 #ba307b inset, 0 0 0 4px #e6e6e6;box-shadow: 0 1px 0 0 #ba307b inset, 0 0 0 4px #e6e6e6;-webkit-border-radius: 2px;-moz-border-radius: 2px;border-radius: 2px;-webkit-transition: all 0.2s ease-in-out;-moz-transition: all 0.2s ease-in-out;-o-transition: all 0.2s ease-in-out;transition: all 0.2s ease-in-out;text-shadow: 0 1px 1px #7b024f;border-color: #990444\"> Cancel</a>";
}else{ 
$html_message .= $message_fr;
$html_message .= "</div><div style=\"text-align:center;padding-bottom:40px;\"><a href=\"".$http.$domain.$sandbox."article.php?tk=".$token."&tki=".$token_identity."&confirm=yes\" class=\"buttongreen\" style=\"margin: 0 10px;display: inline-block;padding: 6px 30px;border-width: 1px;border-style: solid;font-family: Arial, Helvetica, Sans-serif;font-size: 14px;color: #fff;font-weight: 700;text-decoration: none;background: #66ab04;-webkit-box-shadow: 0 1px 0 0 #82ba31 inset, 0 0 0 4px #e6e6e6;-moz-box-shadow: 0 1px 0 0 #82ba31 inset, 0 0 0 4px #e6e6e6;box-shadow: 0 1px 0 0 #82ba31 inset, 0 0 0 4px #e6e6e6;-webkit-border-radius: 2px;-moz-border-radius: 2px;border-radius: 2px;-webkit-transition: all 0.2s ease-in-out;-moz-transition: all 0.2s ease-in-out;-o-transition: all 0.2s ease-in-out;transition: all 0.2s ease-in-out;text-shadow: 0 1px 1px #497a03;border-color: #5b9904\"> Afficher</a> <a href=\"".$http.$domain.$sandbox."article.php?tk=".$token."&tki=".$token_identity."&confirm=no\" class=\"buttoncencel\" style=\"margin: 0 10px;display: inline-block;padding: 6px 30px;border-width: 1px;border-style: solid;font-family: Arial, Helvetica, Sans-serif;font-size: 14px;color: #fff;font-weight: 700;text-decoration: none;background: #f4425f;-webkit-box-shadow: 0 1px 0 0 #ba307b inset, 0 0 0 4px #e6e6e6;-moz-box-shadow: 0 1px 0 0 #ba307b inset, 0 0 0 4px #e6e6e6;box-shadow: 0 1px 0 0 #ba307b inset, 0 0 0 4px #e6e6e6;-webkit-border-radius: 2px;-moz-border-radius: 2px;border-radius: 2px;-webkit-transition: all 0.2s ease-in-out;-moz-transition: all 0.2s ease-in-out;-o-transition: all 0.2s ease-in-out;transition: all 0.2s ease-in-out;text-shadow: 0 1px 1px #7b024f;border-color: #990444\"> Annuler</a>";
}     
$html_message .= "</div></div></body></html>";

//mail($to, $subject, $html_message, $headers);
echo $html_message;
?>




