<?php
$title_page="Assemblée Générale";
include("inc/variables.php");
require("inc/login.php");
$token=$_GET['tk'];
$action=$_GET['action'];
$confirm=$_POST['confirm'];
$time = time();
$maintenant = date("Y-m-d H:i:s", $time);


if($token!=""){ // Article bdd
$sql_article = mysql_query("SELECT * FROM ".$prefix."articles WHERE token = '".$token."'");
if (mysql_num_rows($sql_article)==0) { $errors[] = "Wrong token."; }
else{  while($article = mysql_fetch_array($sql_article)) {
$id_article = $article['id'];
$id_user_article = $article['id_user'];
$token_user_article = $article['token_user'];
$title = $article['titre']; $title=stripcslashes($title); $title=strip_tags($title);
$statut = $article['statut'];
$thumb = $article['thumb'];
$license = $article['license'];
$thumb_license = $article['thumb_license'];
$photographe = $article['thumb_photographer'];
$legende = $article['thumb_legende'];
$description = $article['description']; $description=stripcslashes($description); $description=strip_tags($description);
$texte = $article['texte']; $texte=stripslashes($texte);
$hashtag=$article['tag'];
$vimeo1=$article['vimeo1'];
$vimeo2=$article['vimeo2'];
$vimeo3=$article['vimeo3'];
$vimeo4=$article['vimeo4'];
$youtube1=$article['youtube1'];
$youtube2=$article['youtube2'];
$youtube3=$article['youtube3'];
$youtube4=$article['youtube4'];
$edition=$article['edition'];
if($edition!="") {
$slug=$article['slug'];
$index_slug=$article['index_slug'];
if($edition=="fr") { $lang_dir="1.fr"; }
if($edition=="en") { $lang_dir="2.en"; }
if($edition=="fr"){ $lang="fr"; }else{ $lang="en"; }
}
$soundcloud=$article['soundcloud']; $soundcloud=stripslashes($soundcloud);
if($statut=="published") { $ladate = date("c",strtotime($article['date_published'])); }
else{ $ladate = date("c",strtotime($article['deadline_proposal'])); }
$deadline_proposal=date("c",strtotime($article['deadline_proposal']));
$deadline_fin_display = date("d M Y à  H:i", strtotime($article['deadline_proposal']));
$deadline_proposal_cd = date("Y/m/d H:i:s", strtotime($article['deadline_proposal']));
$signature = $article['signature'];
$twauteur = $article['twauteur'];
$identity=$article['identity'];
include("inc/getvotes.php"); // Get votes
} }
// si requete ajout d'un nouveau sujet

}else{ $errors[] = "Token missing"; }





$result_votes = mysql_query("SELECT * FROM ".$prefix."votes WHERE id_article = '".$id_article."'");
$num_votes = mysql_num_rows($result_votes);
if ($num_votes!=0){ $someone_voted="yes";  }else{ $someone_voted="no";  }
$past_vote=vote_user_article($id_user_online,$id_article);

if($statut=="published" and $solidaire_user_online=="oui"){ 
	if($lang=="fr"){
	$messages[] = "Attention, cet article est déjà publié. En tant que membre solidaire vous avez le droit d'éditer cet article, mais seulement pour des modifications mineures (mise en page, orthographe, etc..), et en aucun cas vous êtes autorisé.e à modifier le propos de l'article, qui a été validé par un vote. ";
	}else{ 
	$messages[] = "Careful, this article is already published. As a member of the Solidarity College you have the right to edit this article, but only for minor changes (layout, spelling, etc..), and under no circumstances you would be allowed to amend the meaning, which has been approved by a vote.";
	}
}elseif($statut=="proposed" and $token_user_online==$token_user_article and $num_votes==0){

	if($lang=="fr"){
	$messages[] = "Vous pouvez modifier votre article tant qu'aucun.e membre du comité de rédaction n'a voté.";
	}else{ 
	$messages[] = "You can change your article as long as no member of the editorial board has voted.";
	}

}else{ 

if($lang=="fr"){ $errors[] = "Accès refusé, désolé."; }else{ $errors[] = "Access denied, sorry."; }

}





// ###### SUPPRESION PUBLISHED ###
if($statut=="published" and $action=="del" and $confirm!="" and $solidaire_user_online=="oui" and count($errors) == 0){
$dir="../content/".$lang_dir."/".$index_slug.".".$slug."/";
$maintenant = date("Y-m-d H:i:s", $time);


if($thumb!=""){ // on deplace thumb
	$temp = $dir."thumb.jpg";
	$new_file_name = "thumb_" . $token . '.jpg';
	$dest_thumb = "../public/uploads/".$new_file_name;
	copy($temp, $dest_thumb);
	chmod($dest_thumb, fileperms($temp));
	unlink($temp);
	mysql_query("UPDATE ".$prefix."articles SET thumb = 'public/uploads/$new_file_name' WHERE id = '".$id_article."'");
}//fin deplacement fichiers images


if($id_user_online!=$id_user_article and $past_vote==""){ // On insere un vote
mysql_query("INSERT INTO `".$prefix."votes` (`id`, `id_article`, `id_user`, `vote`, `date`) VALUES (NULL, '$id_article', '$id_user_online', 'nono', '$maintenant')");
}elseif($id_user_online!=$id_user_article and $past_vote!=""){ // on update un vote
mysql_query("UPDATE ".$prefix."votes SET vote = 'nono' WHERE id_article = '".$id_article."' AND id_user = '".$id_user_online."'");
mysql_query("UPDATE ".$prefix."votes SET date = '$maintenant' WHERE id_article = '".$id_article."' AND id_user = '".$id_user_online."'");
}
unlink($dir."article.yml");
rmdir($dir);
mysql_query("INSERT INTO `".$prefix."articles_modifs` (`id`, `id_article`, `id_user`, `date`, `detail`) VALUES (NULL, '$id_article', '$id_user_online', '$maintenant', 'unpublish');");
mysql_query("UPDATE ".$prefix."articles SET statut = 'unpublished' WHERE id = '".$id_article."'");
header('Location: ../article.php?tk='.$token);
}// ###### SUPPRESION PUBLISHED ###


// ###### FEATURED ###
if($statut=="published" and $action=="featured" and $solidaire_user_online=="oui" and count($errors) == 0){
$actual_slug=$edition."/".$slug;
$nomfichier="../content/".$lang_dir."/edition.yml";
$datafile = fopen($nomfichier, 'r+');
ftruncate($datafile, 0); // on efface le contenu
$newcontent="bypass_cache: true\r\n";
if($lang=="fr"){
$newcontent.="title: ArticlesFR\r\n";
}else{ 
$newcontent.="title: ArticlesEN\r\n";
}
$newcontent.="featured: bigpicture\r\n";
$newcontent.="bigpicture_url: ".$actual_slug."/\r\n";
fputs($datafile, $newcontent);
fclose($datafile);
$date_modif = date("Y-m-d H:i:s", strtotime('now'));;
mysql_query("INSERT INTO `".$prefix."articles_modifs` (`id`, `id_article`, `id_user`, `date`, `detail`) VALUES (NULL, '$id_article', '$id_user_online', '$date_modif', 'featured');");
header('Location: ../article.php?tk='.$token);
}// ###### END FEATURED ###


// ###### UNFEATURED ###
if($statut=="published" and $action=="unfeatured" and $solidaire_user_online=="oui" and count($errors) == 0){
$nomfichier="../content/".$lang_dir."/edition.yml";
$datafile = fopen($nomfichier, 'r+');
ftruncate($datafile, 0); // on efface le contenu
$newcontent="bypass_cache: true\r\n";
if($lang=="fr"){
$newcontent.="title: ArticlesFR\r\n";
}else{ 
$newcontent.="title: ArticlesEN\r\n";
}
$newcontent.="featured: none\r\n";
$newcontent.="bigpicture_url: \r\n";
fputs($datafile, $newcontent);
fclose($datafile);
$date_modif = date("Y-m-d H:i:s", strtotime('now'));;
mysql_query("INSERT INTO `".$prefix."articles_modifs` (`id`, `id_article`, `id_user`, `date`, `detail`) VALUES (NULL, '$id_article', '$id_user_online', '$date_modif', 'unfeatured');");
header('Location: ../article.php?tk='.$token);
}// ###### END UNFEATURED ###





// ###### CHANGE_AUTHOR ###
$changeauthor=$_GET['changeauthor'];
if(($statut=="published" or $statut=="proposed" or $statut=="accepted") and $changeauthor!="" and $solidaire_user_online=="oui" and count($errors) == 0 and $token_user_article==$token_user_online){

if($changeauthor=="me"){
$signature=$prenom_user_online." ".$nom_user_online;
$twauteur=$twitter_user_online;
mysql_query("UPDATE ".$prefix."articles SET signature = '$signature' WHERE id = '".$id_article."'");
mysql_query("UPDATE ".$prefix."articles SET twauteur = '$twauteur' WHERE id = '".$id_article."'");
$file = "../content/$lang_dir/$index_slug.$slug/article.yml";
include("inc/inc_write_article_yml.php");
header('Location: ../article.php?tk='.$token);
}

if($changeauthor=="rc"){
$signature="Radical Cinema";
$twauteur="radicalcinema";
mysql_query("UPDATE ".$prefix."articles SET signature = '$signature' WHERE id = '".$id_article."'");
mysql_query("UPDATE ".$prefix."articles SET twauteur = '$twauteur' WHERE id = '".$id_article."'");
$file = "../content/$lang_dir/$index_slug.$slug/article.yml";
include("inc/inc_write_article_yml.php");
header('Location: ../article.php?tk='.$token);
}

}// ###### END CHANGE_AUTHOR ###







################ EDIT ARTICLE
	if($token!="" and $_POST['change']!=""){ // update bdd
$title=$_POST['titre']; $title=remplaceGuillemets($title); $title=strip_tags($title); $title = str_replace(":", ";", $title); 
$ladescription=$_POST['ladescription'];  $ladescription=strip_tags($ladescription); $ladescription=remplaceGuillemets($ladescription); $ladescription = str_replace(":", ";", $ladescription);
$hashtag=$_POST['hashtag'];    $hashtag = preg_replace("#[^a-zA-Z]#", "", $hashtag);
$texte=$_POST['texte']; 
$vimeo1=$_POST['vimeo1'];
$vimeo2=$_POST['vimeo2'];
$vimeo3=$_POST['vimeo3'];
$vimeo4=$_POST['vimeo4'];
$youtube1=$_POST['youtube1'];
$youtube2=$_POST['youtube2'];
$youtube3=$_POST['youtube3'];
$youtube4=$_POST['youtube4'];
if($youtube1=="" and $youtube2!=""){ $youtube1=$youtube2; $youtube2="";  }
if($youtube2=="" and $youtube3!=""){ $youtube2=$youtube3; $youtube3="";  }
if($youtube3=="" and $youtube4!=""){ $youtube3=$youtube4; $youtube4="";  }
if($youtube1=="" and $youtube2!=""){ $youtube1=$youtube2; $youtube2="";  }
if($youtube2=="" and $youtube3!=""){ $youtube2=$youtube3; $youtube3="";  }
if($youtube3=="" and $youtube4!=""){ $youtube3=$youtube4; $youtube4="";  }
if($vimeo1=="" and $vimeo2!=""){ $vimeo1=$vimeo2; $vimeo2="";  }
if($vimeo2=="" and $vimeo3!=""){ $vimeo2=$vimeo3; $vimeo3="";  }
if($vimeo3=="" and $vimeo4!=""){ $vimeo3=$vimeo4; $vimeo4="";  }
if($vimeo1=="" and $vimeo2!=""){ $vimeo1=$vimeo2; $vimeo2="";  }
if($vimeo2=="" and $vimeo3!=""){ $vimeo2=$vimeo3; $vimeo3="";  }
if($vimeo3=="" and $vimeo4!=""){ $vimeo3=$vimeo4; $vimeo4="";  }


 

if($statut=="proposed" and $past_vote==""){  //Empecher Changement edition
$edition=$_POST['edition']; 
}


if($edition=="fr") { $lang_dir="1.fr"; }
else{ $lang_dir="2.en"; }
$soundcloud=$_POST['soundcloud_code'];
	if($title==""){ $errors[] = "Title missing"; }
	if($texte==""){ $errors[] = "Text missing"; }
	$description=$ladescription;
		if(count($errors) == 0){ //si pas d'erreurs

			$title = addslashes($title);
			$texte = addslashes($texte);
			$description = addslashes($description);
			$soundcloud = addslashes($soundcloud);
			mysql_query("UPDATE ".$prefix."articles SET titre = '".$title."' WHERE token = '".$token."'");
			mysql_query("UPDATE ".$prefix."articles SET description = '".$description."' WHERE token = '".$token."'");
			mysql_query("UPDATE ".$prefix."articles SET texte = '".$texte."' WHERE token = '".$token."'");
			mysql_query("UPDATE ".$prefix."articles SET tag = '".$hashtag."' WHERE token = '".$token."'");
			mysql_query("UPDATE ".$prefix."articles SET vimeo1 = '".$vimeo1."' WHERE token = '".$token."'");
			mysql_query("UPDATE ".$prefix."articles SET vimeo2 = '".$vimeo2."' WHERE token = '".$token."'");
			mysql_query("UPDATE ".$prefix."articles SET vimeo3 = '".$vimeo3."' WHERE token = '".$token."'");
			mysql_query("UPDATE ".$prefix."articles SET vimeo4 = '".$vimeo4."' WHERE token = '".$token."'");
			mysql_query("UPDATE ".$prefix."articles SET youtube1 = '".$youtube1."' WHERE token = '".$token."'");
			mysql_query("UPDATE ".$prefix."articles SET youtube2 = '".$youtube2."' WHERE token = '".$token."'");
			mysql_query("UPDATE ".$prefix."articles SET youtube3 = '".$youtube3."' WHERE token = '".$token."'");
			mysql_query("UPDATE ".$prefix."articles SET youtube4 = '".$youtube4."' WHERE token = '".$token."'");
			mysql_query("UPDATE ".$prefix."articles SET soundcloud = '".$soundcloud."' WHERE token = '".$token."'");
			mysql_query("UPDATE ".$prefix."articles SET edition = '".$edition."' WHERE token = '".$token."'");

			if($statut=="published" and $index_slug!="0"){ //si article déjà publié, on met à jour le fichier du site public
			
			$file = "../content/$lang_dir/$index_slug.$slug/article.yml";
			include("inc/inc_write_article_yml.php");
			
			}

			header('Location: ../article.php?tk='.$token);

		
		} // fin pas d'erreur
	} // fin nouvel article




if($action=="del" and $confirm==""){ // ###### CONFIRMATION SUPPRESION ###
unset($messages);
$messages = array();
include("inc/template_top.php");
?>
<form class="form-horizontal" role="form" action="edit-article.php?tk=<?php echo $token; ?>&action=del" method="post">
<div class="row no-gutters">
<div class="col-md-8 col-xs-12">
<h4><b>" <?php echo $title;  ?> "</b></h4>
  <div class="form-group">
<?php if($lang=="fr"){ ?> 
    <label for="confirm" class="col-sm-8 control-label cblack">Voulez-vous vraiment dé-publier cet article?</label>
<?php }else{  ?>
    <label for="confirm" class="col-sm-8 control-label">Do you really want to un-publish this article?</label>
<?php } ?>
    <div class="col-sm-4 col-xs-12">
<?php if($lang=="fr"){ ?> 
      <input type="submit" name="confirm" value="Dépublier" class="btn-delete-small">
      <a href="../article.php?tk=<?php echo $token; ?>" class="btn-article-small">Annuler</a>
<?php }else{  ?>
      <input type="submit" name="confirm" value="Un-publish" class="btn-delete-small">
      <a href="../article.php?tk=<?php echo $token; ?>" class="btn-article-small">Cancel</a>
<?php } ?>
    </div>
  </div>  
</div>
</div>
</form>

<?php
include("inc/template_bottom.php");
die;
}// ###### CONFIRMATION SUPPRESION ###





include("inc/template_top.php");
?>
<p>&nbsp;</p>




<form class="form-horizontal" role="form" action="edit-article.php?tk=<?php echo $token; ?>" method="post">

	<div class="row">
    
        <div class="timeline-centered">


<!-- PREMIER SUJET -->
        <article class="timeline-entry">

            <div class="timeline-entry-inner">

                <div class="timeline-icon bg-thread">
                    <i class="fa fa-comment"></i>
                </div>

                <div class="timeline-label">


<?php
$title=stripcslashes($title);
$description=stripcslashes($description);
$hashtag=stripcslashes($hashtag);
$legende=stripcslashes($legende);
$photographe=stripcslashes($photographe);
$edito=stripcslashes($edito);
$texte=stripcslashes($texte);
?>


<div class="row no-gutters"> 

<?php if($lang=="fr"){ ?> 
    <label for="titre" class="col-sm-3 control-label">Titre</label>
<?php }else{  ?>
    <label for="titre" class="col-sm-3 control-label">Title</label>
<?php } ?>
    <div class="col-sm-9">
      <input type="text" class="form-control" id="titre" name="titre" value="<?php echo $title; ?>">
    </div>
 
  
  

<?php if($lang=="fr"){ ?> 
    <label for="description" class="col-sm-3 control-label">Sous-titre</label>
<?php }else{  ?>
    <label for="description" class="col-sm-3 control-label">Sub-title</label>
<?php } ?>
    <div class="col-sm-9">
      <input type="text" class="form-control" id="ladescription" name="ladescription" value="<?php echo $description; ?>" maxlength="200">
    </div>

  

    <label for="hashtag" class="col-sm-3 control-label">Hashtag</label>
    <div class="col-sm-9">
      <input type="text" class="form-control" id="hashtag" name="hashtag" value="<?php echo $hashtag; ?>">
    </div>

<?php if($lang=="fr"){ ?> 
    <label for="edition" class="col-sm-3 control-label">Langue</label>
<?php }else{  ?>
    <label for="edition" class="col-sm-3 control-label">Lang</label>
<?php } ?>
    <div class="col-sm-9">
		<select name="edition" class="form-control"<?php if($statut=="published"){ echo " disabled"; } ?>>
		  <option value="fr"<?php if($lang=="fr"){ echo " selected"; } ?>>Français</option>
		  <option value="en"<?php if($lang=="en"){ echo " selected"; } ?>>English</option>
		</select>
    </div>

<div class="clear">&nbsp;</div>
 <div style="margin:20px 0px!important;border-bottom:1px solid #e1e3e4;">&nbsp;</div>


    <div class="col-sm-12">

		<script>
		tinymce.init({
		selector: 'div#texte',
		inline: true,
		plugins: [
		'link',
		'code'
		],
		toolbar: 'bold italic link',
		menubar: false
		});
		</script>
		<div class="" id="texte"><?php echo $texte; ?></div>

    </div>



  

 <div style="margin:20px 0px!important;border-bottom:1px solid #e1e3e4;">&nbsp;</div>
 
    <label for="hashtag" class="col-sm-3 control-label">Vimeo</label>
    <div class="col-sm-9">
	 	<div class="input-group">
		  <div class="input-group-addon">https://vimeo.com/</div>
		  <input type="text" class="form-control" id="vimeo1" name="vimeo1" value="<?php echo $vimeo1; ?>" placeholder="id vimeo">
		</div>
		<div class="input-group">
		  <div class="input-group-addon">https://vimeo.com/</div>
		  <input type="text" class="form-control" id="vimeo2" name="vimeo2" value="<?php echo $vimeo2; ?>" placeholder="id vimeo">
		</div>
		<div class="input-group">
		  <div class="input-group-addon">https://vimeo.com/</div>
		  <input type="text" class="form-control" id="vimeo3" name="vimeo3" value="<?php echo $vimeo3; ?>" placeholder="id vimeo">
		</div>
		<div class="input-group">
		  <div class="input-group-addon">https://vimeo.com/</div>
		  <input type="text" class="form-control" id="vimeo4" name="vimeo4" value="<?php echo $vimeo4; ?>" placeholder="id vimeo">
		</div>
    </div>

 <div style="margin:20px 0px!important;border-bottom:1px solid #e1e3e4;">&nbsp;</div>


    <label for="hashtag" class="col-sm-3 control-label">Youtube</label>
    <div class="col-sm-9">
		  	<div class="input-group">
			  <div class="input-group-addon">https://www.youtube.com/watch?v=</div>
			  <input type="text" class="form-control" id="youtube1" name="youtube1" value="<?php echo $youtube1; ?>" placeholder="id youtube">
			</div>
			<div class="input-group">
			  <div class="input-group-addon">https://www.youtube.com/watch?v=</div>
			  <input type="text" class="form-control" id="youtube2" name="youtube2" value="<?php echo $youtube2; ?>" placeholder="id youtube">
			</div>
			<div class="input-group">
			  <div class="input-group-addon">https://www.youtube.com/watch?v=</div>
			  <input type="text" class="form-control" id="youtube3" name="youtube3" value="<?php echo $youtube3; ?>" placeholder="id youtube">
			</div>
			<div class="input-group">
			  <div class="input-group-addon">https://www.youtube.com/watch?v=</div>
			  <input type="text" class="form-control" id="youtube4" name="youtube4" value="<?php echo $youtube4; ?>" placeholder="id youtube">
			</div>
    </div>

 <div style="margin:20px 0px!important;border-bottom:1px solid #e1e3e4;">&nbsp;</div>


    <label for="hashtag" class="col-sm-3 control-label">Soundcloud</label>
    <div class="col-sm-9">
      <textarea name="soundcloud_code" id="soundcloud_code"  class="form-control"><?php echo $soundcloud; ?></textarea>
    </div>

   
  
  </div><!-- .row -->
  
   
  
 
  
  <input type="hidden" name="change" value="yes">
<?php if($lang=="fr"){ ?> 
  <div class="padd10"><input type="submit" class="btn-article" value="Modifier" /> <a href="../article.php?tk=<?php echo $token; ?>"  class="btn-article">Annuler</a></div>
 <?php }else{  ?>
  <div class="padd10"><input type="submit" class="btn-article" value="Modify" />  <a href="../article.php?tk=<?php echo $token; ?>"  class="btn-article">Cancel</a></div>  
<?php } ?>


                </div>
            </div>

        </article>
<!-- #PREMIER SUJET -->




        <article class="timeline-entry begin">

            <div class="timeline-entry-inner">

                <div class="timeline-icon  bg-black" style="-webkit-transform: rotate(-90deg); -moz-transform: rotate(-90deg);">
                    <i class="entypo-flight"></i> +
                </div>

            </div>

        </article>

    </div>

    
	</div><!-- .row -->
</form>



<?php
include("inc/template_bottom.php");
?>