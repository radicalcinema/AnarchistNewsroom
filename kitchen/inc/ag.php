<?php $item_per_page=5; ?>
<script type="text/javascript">
$(document).ready(function() {

	var track_click = 0; //track user click on "load more" button, righ now it is 0 click
	
	var total_pages = <?php echo $total_pages; ?>;
	$('#results').load("inc/fetch_topics.php", {'page':track_click}, function() {track_click++;}); //initial data to load

	$("#load_more_button").click(function (e) { //user clicks on button
	
		$(this).hide(); //hide load more button on click
		$('.animation_image').show(); //show loading image

		if(track_click <= total_pages) //make sure user clicks are still less than total pages
		{
			//post page number and load returned data into result element
			$.post('inc/fetch_topics.php',{'page': track_click}, function(data) {
			
				$("#load_more_button").show(); //bring back load more button
				
				$("#results").append(data); //append data received from server
				
				//scroll page to button element
				$("html, body").animate({scrollTop: $("#load_more_button").offset().top}, 500);
				
				//hide loading image
				$('.animation_image').hide(); //hide loading image once data is received
	
				track_click++; //user click increment on load button
			
			}).fail(function(xhr, ajaxOptions, thrownError) { 
				alert(thrownError); //alert any HTTP error
				$("#load_more_button").show(); //bring back load more button
				$('.animation_image').hide(); //hide loading image once data is received
			});
			
			
			if(track_click >= total_pages-1)
			{
				//reached end of the page yet? disable load button
				$("#load_more_button").attr("disabled", "disabled");
			}
		 }
		  
		});
});
</script>
<?php if($lang=="fr"){ ?> 
<p class="petit_titre"><span>Assemblée Générale</span></p>
<?php }else{  ?>
<p class="petit_titre"><span>General Assembly</span></p>
<?php } ?>
<div id="results"><div class="animation_image"><img src="images/ajax-loader.gif"> Loading...</div></div>

<div align="center">
<button class="btn-grey-small" id="load_more_button"><i class="fa fa-plus-square"></i> Afficher plus</button>
<div class="animation_image" style="display:none;"><img src="images/ajax-loader.gif"> Loading...</div>
</div>
