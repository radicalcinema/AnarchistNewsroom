<?php
include("variables.php");
require("login.php");
$token = $_POST['token'];
$legende = stripcslashes($_POST['legende']);
$photographe = $_POST['photographe'];
$thumb_license = $_POST['license'];
############ Configuration ##############
$thumb_square_size 		= 500; //Thumbnails will be cropped to 200x200 pixels
$min_image_width 		= 1280; //Maximum image size (height and width)
$min_image_height 		= 720; //Maximum image size (height and width)
$max_image_size 		= 1280; //Maximum image size (height and width)
$thumb_prefix			= "_thumb-medium"; //Normal thumb Prefix
$thumb_width = 600;
$thumb_height = 338;
$destination_folder		= '../../public/uploads/'; //upload directory ends with / (slash)
$jpeg_quality 			= 100; //jpeg quality
##########################################


$sql_article = mysql_query("SELECT * FROM ".$prefix."articles WHERE token = '".$token."'");
if (mysql_num_rows($sql_article)==0) { $errors[] = "Wrong token."; }
else{  while($article = mysql_fetch_array($sql_article)) {
$id_article = $article['id'];
$id_user_article = $article['id_user'];
$token_user_article = $article['token_user'];
$title = $article['titre']; $title=stripcslashes($title); 
$statut = $article['statut'];
$thumb = $article['thumb'];

$description = $article['description']; $description=stripcslashes($description);
$texte = $article['texte']; $texte=stripslashes($texte);
$edito = $article['edito']; $edito=stripcslashes($edito);
$hashtag=$article['tag'];
$vimeo1=$article['vimeo1'];
$vimeo2=$article['vimeo2'];
$vimeo3=$article['vimeo3'];
$vimeo4=$article['vimeo4'];
$youtube1=$article['youtube1'];
$youtube2=$article['youtube2'];
$youtube3=$article['youtube3'];
$youtube4=$article['youtube4'];
$edition=$article['edition'];
if($edition!="") {
$slug=$article['slug'];
$index_slug=$article['index_slug'];
if($edition=="fr") { $lang_dir="1.fr"; }
if($edition=="en") { $lang_dir="2.en"; }
}
$soundcloud=$article['soundcloud'];
if($statut=="published") { $ladate = date("c",strtotime($article['date_published'])); }
else{ $ladate = date("c",strtotime($article['deadline_proposal'])); }
$deadline_proposal=date("c",strtotime($article['deadline_proposal']));
$deadline_fin_display = date("d M Y à  H:i", strtotime($article['deadline_proposal']));
$deadline_proposal_cd = date("Y/m/d H:i:s", strtotime($article['deadline_proposal']));
$signature = $article['signature'];
$twauteur = $article['twauteur'];
$identity=$article['identity'];

include("getvotes.php");

}

}




##########################################			
if($token_user_article!=$token_user_online and $solidaire_user_online!="oui"){  $errors[] = "Access denied, sorry-".$solidaire_user_online; }
if($token_user_article==$token_user_online and $solidaire_user_online!="oui"){ 
if($statut!="proposed"){ $errors[] = "Access granted only when article = proposed."; }
}
if($solidaire_user_online=="oui"){ 
if($statut!="published" and $statut!="proposed"){ $errors[] = "Access denied for the moment."; }
}













//continue only if $_POST is set and it is a Ajax request
if(isset($_POST) && isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){

	// check $_FILES['ImageFile'] not empty
	if(!isset($_FILES['image_file']) || !is_uploaded_file($_FILES['image_file']['tmp_name'])){
			die('Image file is Missing!'); // output error when above checks fail.
	}
	if(!isset($token)){
			die('token is Missing!'); // output error when above checks fail.
	}
	if(count($errors) != 0){
		foreach($errors as $e){ echo "<p class='ctomato padd10 txtcenter'><i class='fa fa-exclamation-triangle'></i> ".$e."</p>"; }
		die('Denied. Sorry.');
	}

	if($statut=="proposed") {  $destination_folder = '../../public/uploads/'; $destination_folder2="public/uploads/"; }
	elseif($statut=="published") {  $destination_folder = "../../content/$lang_dir/$index_slug.$slug/"; $destination_folder2 = "content/$lang_dir/$index_slug.$slug/";}
	else{ die('Wrong destination folder');}
	
	
	//uploaded file info we need to proceed
	$image_name = $_FILES['image_file']['name']; //file name
	$image_size = $_FILES['image_file']['size']; //file size
	$image_temp = $_FILES['image_file']['tmp_name']; //file temp

	$image_size_info 	= getimagesize($image_temp); //get image size
	
	if($image_size_info){
		$image_width 		= $image_size_info[0]; //image width
		$image_height 		= $image_size_info[1]; //image height
		$image_type 		= $image_size_info['mime']; //image type
	}else{
		die("Make sure image file is valid!");
	}


	//if($image_width < $min_image_width or $image_height < $min_image_height){
	//		die('Image trop petite. Min: 1280px / 720px'); // output error when above checks fail.
	//}

	if($image_type!="image/jpeg"){
			die('Format JPG obligatoire.'); // output error when above checks fail.
	}



	//switch statement below checks allowed image type 
	//as well as creates new image from given file 
	switch($image_type){
		case 'image/png':
			$image_res =  imagecreatefrompng($image_temp); break;
		case 'image/gif':
			$image_res =  imagecreatefromgif($image_temp); break;			
		case 'image/jpeg': case 'image/pjpeg':
			$image_res = imagecreatefromjpeg($image_temp); break;
		default:
			$image_res = false;
	}

	if($image_res){
		//Get file extension and name to construct new file name 
		$image_info = pathinfo($image_name);
		$image_extension = strtolower($image_info["extension"]); //image extension
		$image_name_only = strtolower($image_info["filename"]);//file name only, no extension
		
		//create a random name for new image (Eg: fileName_293749.jpg) ;
		
		if($statut=="proposed"){ $new_file_name = "thumb_" . $token . '.' . $image_extension;  }
		elseif($statut=="published"){ $new_file_name = "thumb" . '.' . $image_extension; }
 
		//folder path to save resized images and thumbnails
		$image_save_folder 	= $destination_folder . $new_file_name;

		if (file_exists($image_save_folder)) { unlink($image_save_folder); }

		//call normal_resize_image() function to proportionally resize image
		if(normal_resize_image($image_res, $image_save_folder, $image_type, $max_image_size, $image_width, $image_height, $jpeg_quality))
		{


			
			/* We have succesfully resized and created thumbnail image
			We can now output image to user's browser or store information in the database*/

			//mysql_query("UPDATE ".$prefix."articles SET headpic = '$head_save_folder' WHERE id = '".$id_article."'");
			mysql_query("UPDATE ".$prefix."articles SET thumb = '".$destination_folder2.$new_file_name."' WHERE token = '".$token."'"); 
		
		
			mysql_query("UPDATE ".$prefix."articles SET thumb_photographer = '$photographe' WHERE token = '".$token."'");
			mysql_query("UPDATE ".$prefix."articles SET thumb_license = '$thumb_license' WHERE token = '".$token."'");
			mysql_query("UPDATE ".$prefix."articles SET thumb_legende = '$legende' WHERE token = '".$token."'");


			if($statut=="published" and $index_slug!="0"){ //si article déjà publié, on met à jour le fichier du site public
			$file = "../../content/$lang_dir/$index_slug.$slug/article.yml";
			include("inc_write_article_yml.php");
			$date_modif = date("Y-m-d H:i:s", strtotime('now'));;
			mysql_query("INSERT INTO `".$prefix."articles_modifs` (`id`, `id_article`, `id_user`, `date`, `detail`) VALUES (NULL, '$id_article', '$id_user_online', '$date_modif', 'thumb');");
			}
?>
			 
			<?php if($lang=="fr"){ ?>
			<button class="btn-article-small" onclick="javascript:location.href='article.php?tk=<?php echo $token; ?>'"><i class="fa fa-thumbs-up"></i> Retour à l'article</button>
			<br /><b>C'est fait !</b> <i class='fa fa-check cgreen'></i>
			<?php }else{  ?>
			<button class="btn-article-small" onclick="javascript:location.href='article.php?tk=<?php echo $token; ?>'"><i class="fa fa-thumbs-up"></i> Back to the article</button>
			<br /><b>Done !</b> <i class='fa fa-check cgreen'></i>
			<?php } ?>
			<?php

		}
		
		imagedestroy($image_res); //freeup memory
	}
}

#####  This function will proportionally resize image ##### 
function normal_resize_image($source, $destination, $image_type, $max_size, $image_width, $image_height, $quality){
	
	if($image_width <= 0 || $image_height <= 0){return false;} //return false if nothing to resize
	
	//do not resize if image is smaller than max size
	if($image_width <= $max_size && $image_height <= $max_size){
		if(save_image($source, $destination, $image_type, $quality)){
			return true;
		}
	}
	
	//Construct a proportional size of new image
	$image_scale	= min($max_size/$image_width, $max_size/$image_height);
	$new_width		= ceil($image_scale * $image_width);
	$new_height		= ceil($image_scale * $image_height);
	
	$new_canvas		= imagecreatetruecolor( $new_width, $new_height ); //Create a new true color image
	
	//Copy and resize part of an image with resampling
	if(imagecopyresampled($new_canvas, $source, 0, 0, 0, 0, $new_width, $new_height, $image_width, $image_height)){
		save_image($new_canvas, $destination, $image_type, $quality); //save resized image
	}

	return true;
}

##### This function corps image to create exact square, no matter what its original size! ######
function crop_image_square($source, $destination, $image_type, $square_size, $image_width, $image_height, $quality){
	if($image_width <= 0 || $image_height <= 0){return false;} //return false if nothing to resize
	
	if( $image_width > $image_height )
	{
		$y_offset = 0;
		$x_offset = ($image_width - $image_height) / 2;
		$s_size 	= $image_width - ($x_offset * 2);
	}else{
		$x_offset = 0;
		$y_offset = ($image_height - $image_width) / 2;
		$s_size = $image_height - ($y_offset * 2);
	}
	$new_canvas	= imagecreatetruecolor( $square_size, $square_size); //Create a new true color image
	
	//Copy and resize part of an image with resampling
	if(imagecopyresampled($new_canvas, $source, 0, 0, $x_offset, $y_offset, $square_size, $square_size, $s_size, $s_size)){
		save_image($new_canvas, $destination, $image_type, $quality);
	}

	return true;
}


function crop_image_head($source, $destination, $image_type, $head_width, $head_height, $image_width, $image_height, $quality){
	if($image_width <= 0 || $image_height <= 0){return false;} //return false if nothing to resize
	
	
	if ($image_width > $image_height) {
	  $y_offset = $image_height / 2;
	  $x_offset = 0;
	  $s_size = $image_height;
	} else {
	  $x_offset = 0;
	  $y_offset = ($image_height - $image_width) / 2;
	  $s_size = $image_width;
	}

	$new_canvas	= imagecreatetruecolor( $head_width, $head_height); //Create a new true color image
	
	//Copy and resize part of an image with resampling
	if(imagecopyresampled($new_canvas, $source, 0, 0, $x_offset, $y_offset, $head_width, $head_height, $head_width, $head_height)){
		save_image($new_canvas, $destination, $image_type, $quality);
	}

	return true;
}



##### Saves image resource to file ##### 
function save_image($source, $destination, $image_type, $quality){
	switch(strtolower($image_type)){//determine mime type
		case 'image/png': 
			imagepng($source, $destination); return true; //save png file
			break;
		case 'image/gif': 
			imagegif($source, $destination); return true; //save gif file
			break;          
		case 'image/jpeg': case 'image/pjpeg': 
			imagejpeg($source, $destination, $quality); return true; //save jpeg file
			break;
		default: return false;
	}
}