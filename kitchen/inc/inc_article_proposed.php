<div class="infobox">
<?php
if($lang=="fr"){ 
echo '<p class="infoarticle">Cet article est en attente de validation</p>';
}else{ 
echo '<p class="infoarticle">This article is awaiting validation</p>';
}
if($signature=="Anonymous" and $identity=="confirmed"){ 
	if($lang=="fr"){ echo "<p class='infoauteur'>Proposition anonyme</p>"; 
	}else{ echo "<p class='infoauteur'>Anonymous proposal</p>"; }
} 
if($signature!="Anonymous" and $identity=="confirmed"){ 
	if($lang=="fr"){ echo "<p class='infoauteur'>Proposé par <b>$signature</b></p>"; 
	}else{ echo "<p class='infoauteur'>Proposed by <b>$signature</b></p>"; }
} 
if($signature!="Anonymous" and $identity!="confirmed" and $identity!=""){
	if($lang=="fr"){ echo "<p class='infoauteur'>Signature en attente de confirmation de l'auteur.e</p>"; 
	}else{ echo "<p class='infoauteur'>Signature awaiting confirmation from the author</p>"; }
}

include("inc_article_changeauthor.php");

if($lang=="fr"){ 
echo '<div class="clock">Date limite du vote</div>';
}else{ 
echo '<div class="clock">Voting Deadline</div>';
}
?>
			
<div id="clock1"></div>
  <script type="text/javascript">
 $('#clock1').countdown("<?php echo $deadline_proposal_cd; ?>", function(event) {
   var $this = $(this).html(event.strftime(''
     + '<span>%H</span>:'
     + '<span>%M</span>:'
     + '<span>%S</span>')); 
 });

 </script>

 </div><!-- #infobox -->

<div class="infobox_transparent"> 
<?php
if($id_user_online){ // user connected
include("inc_article_myvote.php");
}else{ // user not connected
?>
	<?php
	if($lang=="fr"){ 
	 ?>
	 <p>Connectez-vous pour participer au débat</p>
	 <a href="<?php echo $http.$domain.$sandbox; ?>login/fr/" class="btn-article-small">Connexion</a>
	 <a href="<?php echo $http.$domain.$sandbox; ?>join/fr/" class="btn-article-small">Adhésion</a> 
	 <?php
	}else{ 
	 ?>
	 <p>Login to participate in the debate</p>
	 <a href="<?php echo $http.$domain.$sandbox; ?>login/" class="btn-article-small">Login</a>
	 <a href="<?php echo $http.$domain.$sandbox; ?>join/" class="btn-article-small">Join</a> 
	 <?php
	}
	?>
<?php } /* fin user not connected */ ?>
</div> 
 
 
 
 
 