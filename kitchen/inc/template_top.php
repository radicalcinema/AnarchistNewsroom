<!DOCTYPE html>
<html lang="en" class="no-js">
	<head>
<!-- Chrome, Firefox OS, Opera and Vivaldi -->
<meta name="theme-color" content="#000000">
<!-- Windows Phone -->
<meta name="msapplication-navbutton-color" content="#000000">
<!-- iOS Safari -->
<meta name="apple-mobile-web-app-status-bar-style" content="#000000">
	<?php $file=basename($_SERVER['PHP_SELF']); 
	$kitchen=""; 
	if($file=="article.php" or $file=="article_cover.php" or $file=="article_identity.php"){ $kitchen="kitchen/"; } 
	?>
	<?php $currentpage=basename($_SERVER['PHP_SELF']); ?>
		<title>Radical Cinema | <?php echo $title_page; ?></title>
		<meta charset="UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge"> 
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="Radical Cinema est un collectif d'artistes et d'activistes. /// Radical Cinema is a collective of artists and activists.">
		<meta name="author" content="Radical Cinema">
		<link rel="shortcut icon" href="<?php echo $http.$domain.$sandbox; ?>public/images/favicon.ico">
    <link rel="icon" href="<?php echo $http.$domain.$sandbox; ?>public/images/favicon.ico">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
	<link href='https://fonts.googleapis.com/css?family=Droid+Serif' rel='stylesheet' type='text/css'>
    <!-- Custom styles for this template -->

<?php if($file=="register.php" or $file=="payer.php"){ ?>
    <link href="<?php echo $http.$domain.$sandbox; ?>public/css/style-black.css" rel="stylesheet">
	<script type="text/javascript" src="<?php echo $http.$domain.$sandbox; ?>public/js/jquery-2.1.1.min.js"></script>
<?php }else{ ?>
    <link href="<?php echo $http.$domain.$sandbox; ?>public/css/style.css" rel="stylesheet">
	<script type="text/javascript" src="<?php echo $http.$domain.$sandbox; ?>public/js/jquery-2.1.1.min.js"></script>
<?php } ?>
<?php if($file=="article_identity.php"){ ?>
    <link href="<?php echo $http.$domain.$sandbox; ?>public/css/publish.css" rel="stylesheet">
<?php } ?>
<?php if($file=="article_cover.php"){ ?>
    <link href="<?php echo $http.$domain.$sandbox; ?>public/css/article.css" rel="stylesheet">
	<script type="text/javascript" src="<?php echo $http.$domain.$sandbox; ?>public/js/jquery.form.min.js"></script>
<script type='text/javascript'>//<![CDATA[
$(window).load(function(){
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function (e) {
                $('#blah').attr('src', e.target.result);
                $("#blah").show();
            }
            
            reader.readAsDataURL(input.files[0]);
        }
    }
    
    $("#imageInput").change(function(){
        readURL(this);
    });
});//]]> 

</script>
<script type="text/javascript">
$(document).ready(function() { 
	var options = { 
			target: '#output',   // target element(s) to be updated with server response 
			beforeSubmit: beforeSubmit,  // pre-submit callback 
			success: afterSuccess,  // post-submit callback 
			resetForm: true        // reset the form after successful submit 
		}; 
		
	 $('#MyUploadForm').submit(function() { 
			$(this).ajaxSubmit(options);  			
			// always return false to prevent standard browser submit and page navigation 
			return false; 
		}); 
}); 




function afterSuccess()
{
	$('#submit-btn').show(); //show submit button
	$('#loading-img').hide(); //hide submit button

}

//function to check file size before uploading.
function beforeSubmit(){
    //check whether browser fully supports all File API
   if (window.File && window.FileReader && window.FileList && window.Blob)
	{
		
		if( !$('#imageInput').val()) //check empty input filed
		{
			$("#output").html("Are you kidding me?");
			return false
		}
		
		var fsize = $('#imageInput')[0].files[0].size; //get file size
		var ftype = $('#imageInput')[0].files[0].type; // get file type
		

		//allow only valid image file types 
		switch(ftype)
        {
            case 'image/png': case 'image/gif': case 'image/jpeg': case 'image/pjpeg':
                break;
            default:
                $("#output").html("<b>"+ftype+"</b> Unsupported file type!");
				return false
        }
		
		//Allowed file size is less than 1 MB (1048576)
		if(fsize>1048576) 
		{
			$("#output").html("<b>"+bytesToSize(fsize) +"</b> Too big Image file! <br />Please reduce the size of your photo using an image editor.");
			return false
		}
				
		$('#submit-btn').hide(); //hide submit button
		$('#cancel-btn').hide();
		$('#delbutbut').hide();
		
		$('#loading-img').show(); //show loading button
		$("#output").html("");  
	}
	else
	{
		//Output error to older browsers that do not support HTML5 File API
		$("#output").html("Please upgrade your browser, because your current browser lacks some new features we need!");
		return false;
	}
}

//function to format bites bit.ly/19yoIPO
function bytesToSize(bytes) {
   var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
   if (bytes == 0) return '0 Bytes';
   var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
   return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
}

</script>

<?php } ?>
<?php if($file=="edit-article.php"){ ?>
    <link href="<?php echo $http.$domain.$sandbox; ?>public/css/article.css" rel="stylesheet">
<?php } ?>

<?php if($file=="article.php" or $file=="index.php"){ ?>
    <link href="<?php echo $http.$domain.$sandbox; ?>public/css/article.css" rel="stylesheet">

    <script type="text/javascript" src="<?php echo $http.$domain.$sandbox; ?>kitchen/js/jquery.countdown.min.js"></script>

<?php if($statut_vote=="open"){ ?>
<script type="text/javascript">
 $(document).ready(function() {

    (function worker() {               
	$.ajax({ 
	    type: 'GET', 
	    url: 'getvotes.php?tk=<?php echo $token; ?>', 
	    data: { get_param: 'value' }, 
	    dataType: 'json',
	    success: function (data) { 

      window.chartOptions = {
        segmentShowStroke: false,
        percentageInnerCutout: 60,
        animation : false
      };
      
        
        // Replace the chart canvas element
        $('#votes').replaceWith('<canvas id="votes" height="150" width="150" style="margin:0px auto;padding:0px;"></canvas>');
        $("#yesyes").html(data[0]); 
        $("#yes").html(data[1]); 
        $("#no").html(data[2]); 
        $("#nono").html(data[3]); 
        // Draw the chart
        var ctx = $('#votes').get(0).getContext("2d");
        var percentDiff = 100-data[3]-data[2]-data[1]-data[0];
        new Chart(ctx).Doughnut([
          {
			        value: data[3],
			        color:"#fd0080", 
			        label: "Pas du tout d'accord"
			    },
			    {
			        value: data[2],
			        color: "#f1c40f",
			        label: "Moyennement d'accord"
			    },
			    {
			        value: data[1],
			        color: "#3498db",
			        label: "Plutôt d'accord"
			    },
			    {
			        value: data[0],
			        color: "#1abc9c",
			        label: "Tout à fait d'accord"
			    },
			    {
			        value: percentDiff,
			        color: "#ccc",
			        label: "vide"
			    }],
          window.chartOptions);
          // Replace the chart canvas element
        $('#avis').replaceWith('<canvas id="avis" height="150" width="150" style="margin:0px auto;padding:0px;"></canvas>');
        $("#yesyes2").html(data[4]); 
        $("#yes2").html(data[5]); 
        $("#no2").html(data[6]); 
        $("#nono2").html(data[7]); 
        // Draw the chart
        var ctx = $('#avis').get(0).getContext("2d");
        var percentDiff = 100-data[7]-data[6]-data[5]-data[4];
        new Chart(ctx).Doughnut([
          {
			        value: data[7],
			        color:"#fd0080", 
			        label: "Pas du tout d'accord"
			    },
			    {
			        value: data[6],
			        color: "#f1c40f",
			        label: "Moyennement d'accord"
			    },
			    {
			        value: data[5],
			        color: "#3498db",
			        label: "Plutôt d'accord"
			    },
			    {
			        value: data[4],
			        color: "#1abc9c",
			        label: "Tout à fait d'accord"
			    },
			    {
			        value: percentDiff,
			        color: "#ccc",
			        label: "vide"
			    }],
          window.chartOptions);    
	
	 
	    },
		complete: function() {
      // Schedule the next request when the current one's complete
      setTimeout(worker, 9999);
    }
  });
})();


});

</script>
<?php }else{ /*si vote clos */ ?>



<?php } ?>
<?php } ?>




<?php if($file=="vote.php" or $file=="topic.php" or $file=="edit.php" or $file=="edit-article.php"  or $file=="new-topic.php" or $file=="new-vote.php" or $file=="article.php"){ ?>

<script src='<?php echo $http.$domain.$sandbox; ?>public/tinymce/tinymce.min.js'></script>


<?php } ?>




    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->    
	<script type="text/javascript" src="<?php echo $http.$domain.$sandbox; ?>kitchen/js/chart.js"></script>
    <link href="<?php echo $http.$domain.$sandbox; ?>public/css/kitchen.css" rel="stylesheet">
    <link href="<?php echo $http.$domain.$sandbox; ?>public/css/buttons.css" rel="stylesheet">

	</head>
  <body id="home">




	



	 <!-- FR -->
	
		
												

					

	<!-- #FR -->
	<!-- #bigIF -->
	

<div id="menuzone">


<div class="logo"><a href="<?php echo $http.$domain.$sandbox; ?>fr/"><span>Radical Cinema</span></a></div>
 
 
 
 <ul id="actions">
<?php if($lang=="fr"){  ?>
    <li>
        <a href="<?php echo $http.$domain.$sandbox; ?>publish/fr/">
            <i class="fa fa-pencil"></i>
        </a>
    </li>
    <li>
        <a href="<?php echo $http.$domain.$sandbox; ?>about/fr/">
            <i class="fa fa-info"></i>
        </a>
    </li>
    <li class="dropdown">
        <a href="" data-toggle="dropdown" aria-expanded="false">
            <i class="fa fa-ellipsis-v"></i>
        </a>
        
        <ul class="dropdown-menu dropdown-menu-right">
            <li>
                <a href="<?php echo $http.$domain.$sandbox; ?>kitchen/?lang=fr"><i class="fa fa-users"></i> Assemblée Générale</a>
            </li>
            <li>
                <a href="<?php echo $http.$domain.$sandbox; ?>join/fr/"><i class="fa fa-star"></i>  Adhérer</a>
            </li>
            <li>
                <a href="<?php echo $http.$domain.$sandbox; ?>prod/fr/"><i class="fa fa-industry"></i> Productions</a>
            </li>
             <li>
                <a href="<?php echo $http.$domain.$sandbox; ?>archives/fr/"><i class="fa fa-archive"></i> Archives</a>
            </li>
            <li role="separator" class="divider"></li>
			<li>
				<form action="https://radicalcinema.org/archives/fr/" name="search">
			    <div class="input-group">
			      <input type="text" class="form-control" placeholder="Rechercher..." name="q" id="search-field" autocomplete="off" spellcheck="false">
			      <span class="input-group-btn">
			        <button class="btn btn-default" type="button" onclick="document.search.submit()"><i class="fa fa-search" aria-hidden="true"></i></button>
			      </span>
			    </div><!-- /input-group -->
				</form>
			</li>
            <li role="separator" class="divider"></li>
            <li class="social">
				<div id="socialmenu"><a href="https://twitter.com/radicalcinema" target="_blank" title="Twitter"><i class="fa fa-twitter"></i></a><a href="https://www.facebook.com/radicalcinema" target="_blank" title="Facebook"><i class="fa fa-facebook-square"></i></a><a href="https://soundcloud.com/radicalcinema/" target="_blank" title="Soundcloud"><i class="fa fa-soundcloud"></i></a><a href="https://vimeo.com/josephparis" target="_blank" title="Vimeo"><i class="fa fa-vimeo-square"></i></a><a href="https://www.youtube.com/channel/UCw7l-uBgDsVdHywlALNFZhw" target="_blank" title="Youtube"><i class="fa fa-youtube"></i></a></div> 
            </li>
<?php if($logged=="yes"){ ?> 
			<li role="separator" class="dividerblack"></li>
            <li class="txtcenter">
                <a href="<?php echo $http.$domain.$sandbox; ?>kitchen/logout.php"><i class="fa fa-unlock-alt"></i>  Déconnexion</a>
            </li>
<?php  } ?>
        </ul>
    </li>
    <li class="dropdown">
        <a href="" data-toggle="dropdown" aria-expanded="false">
            <i class="fa fa-exchange"></i>
        </a>
        <ul class="dropdown-menu dropdown-menu-right">
            <li>
                <a href="<?php echo $http.$domain.$sandbox; ?>kitchen/?change_lang=en"><i class="fa fa-exchange"></i> English</a>
            </li>
        </ul>
    </li>
<?php }else{ ?>
    <li>
        <a href="<?php echo $http.$domain.$sandbox; ?>publish/">
            <i class="fa fa-pencil"></i>
        </a>
    </li>
    <li>
        <a href="<?php echo $http.$domain.$sandbox; ?>about/">
            <i class="fa fa-info"></i>
        </a>
    </li>
    <li class="dropdown">
        <a href="" data-toggle="dropdown" aria-expanded="false">
            <i class="fa fa-ellipsis-v"></i>
        </a>
        
        <ul class="dropdown-menu dropdown-menu-right">
            <li>
                <a href="<?php echo $http.$domain.$sandbox; ?>kitchen/?lang=en"><i class="fa fa-users"></i> General Assembly</a>
            </li>
            <li>
                <a href="<?php echo $http.$domain.$sandbox; ?>join/"><i class="fa fa-star"></i>  Join us</a>
            </li>
            <li>
                <a href="<?php echo $http.$domain.$sandbox; ?>prod/"><i class="fa fa-industry"></i> Productions</a>
            </li>
             <li>
                <a href="<?php echo $http.$domain.$sandbox; ?>archives/"><i class="fa fa-archive"></i> Archives</a>
            </li>
            <li role="separator" class="divider"></li>
			<li>
				<form action="https://radicalcinema.org/archives/" name="search">
			    <div class="input-group">
			      <input type="text" class="form-control" placeholder="Search for..." name="q" id="search-field" autocomplete="off" spellcheck="false">
			      <span class="input-group-btn">
			        <button class="btn btn-default" type="button" onclick="document.search.submit()"><i class="fa fa-search" aria-hidden="true"></i></button>
			      </span>
			    </div><!-- /input-group -->
				</form>
			</li>
            <li role="separator" class="divider"></li>
            <li class="social">
				<div id="socialmenu"><a href="https://twitter.com/radicalcinema" target="_blank" title="Twitter"><i class="fa fa-twitter"></i></a><a href="https://www.facebook.com/radicalcinema" target="_blank" title="Facebook"><i class="fa fa-facebook-square"></i></a><a href="https://soundcloud.com/radicalcinema/" target="_blank" title="Soundcloud"><i class="fa fa-soundcloud"></i></a><a href="https://vimeo.com/josephparis" target="_blank" title="Vimeo"><i class="fa fa-vimeo-square"></i></a><a href="https://www.youtube.com/channel/UCw7l-uBgDsVdHywlALNFZhw" target="_blank" title="Youtube"><i class="fa fa-youtube"></i></a></div> 
            </li>
<?php if($logged=="yes"){ ?> 
			<li role="separator" class="dividerblack"></li>
            <li class="txtcenter">
                <a href="<?php echo $http.$domain.$sandbox; ?>kitchen/logout.php"><i class="fa fa-unlock-alt"></i>  Logout</a>
            </li>
<?php  } ?>
        </ul>
    </li>
    <li class="dropdown">
        <a href="" data-toggle="dropdown" aria-expanded="false">
            <i class="fa fa-exchange"></i>
        </a>
        <ul class="dropdown-menu dropdown-menu-right">
            <li>
                <a href="<?php echo $http.$domain.$sandbox; ?>kitchen/?change_lang=fr"><i class="fa fa-exchange"></i> Français</a>
            </li>
        </ul>
    </li>
<?php } ?>
</ul>
                        

</div>
<div class="container"><!-- container -->

    
<?php 
if($logged=="yes" and count($errors) == 0 and $file!="payer.php"){ include($kitchen."inc/inc_menu.php");}
 ?>
    
   
 
 


    
    
<?php
if(count($messages) != 0){ foreach($messages as $m){ echo "<p class='cpurple padd10 txtcenter'><i class='fa fa-info-circle'></i> ".$m."</p>"; }}
if(count($errors) != 0){ foreach($errors as $e){ echo "<p class='ctomato padd10 txtcenter'><i class='fa fa-exclamation-triangle'></i> ".$e."</p>"; }
include("template_bottom.php");
die;
 }

?>

<?php if($_GET['c']=="1"){ ?>
	<?php if($lang=="fr"){ ?> 
	<span class="cgreen"><i class="fa fa-check"></i> Enregistré!</span>
	<?php }else{  ?>
	<span class="cgreen"><i class="fa fa-check"></i> Saved!</span>
	<?php } ?>
<?php } ?>    
    
    
    
    