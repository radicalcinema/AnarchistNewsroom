<div class="infobox">
<p class="cgreen txtcenter"><i class="fa fa-check"></i></p>
<?php
if($signature=="Anonymous" and $identity=="confirmed"){ 
	if($lang=="fr"){ echo "<p class='infoauteur'>Proposition anonyme, publiée "; ?><time datetime="<?php echo $ladate; ?>" class="age"><?php echo $ladate; ?></time><?php echo ".</p>"; 
	}else{ echo "<p class='infoauteur'>Anonymous proposal, published "; ?><time datetime="<?php echo $ladate; ?>" class="age"><?php echo $ladate; ?></time><?php echo ".</p>"; }
} 
if($signature!="Anonymous" and $identity=="confirmed"){ 
	if($lang=="fr"){ echo "<p class='infoauteur'>Proposé par <b>$signature</b>, <br />et publié "; ?><time datetime="<?php echo $ladate; ?>" class="age"><?php echo $ladate; ?></time><?php echo ".</p>"; 
	}else{ echo "<p class='infoauteur'>Proposed by <b>$signature</b>, <br />and published "; ?><time datetime="<?php echo $ladate; ?>" class="age"><?php echo $ladate; ?></time><?php echo ".</p>"; }
} 
?>

<?php include("inc_article_changeauthor.php"); ?>

<div class="padd10 txtcenter">
<?php if($lang=="fr"){ ?> 
<a href="<?php echo $http.$domain.$sandbox.$edition."/".$slug; ?>" class="btn-article-small">Afficher l'article</a>
<?php }else{  ?>
<a href="<?php echo $http.$domain.$sandbox.$edition."/".$slug; ?>" class="btn-article-small">Display the article</a>
<?php } ?>

</div>



</div>			

<div class="infobox_transparent"> 
<?php if($id_user_online){ // user connected
include("inc_article_myvote.php");
 }else{ // user not connected
	if($lang=="fr"){ 
	 ?>
	 <p>Connectez-vous pour participer aux prochains votes</p>
	 <a href="<?php echo $http.$domain.$sandbox; ?>login/fr/" class="btn-article-small">Connexion</a>
	 <a href="<?php echo $http.$domain.$sandbox; ?>join/fr/" class="btn-article-small">Adhésion</a> 
	 <?php
	}else{ 
	 ?>
	 <p>Login to participate in the next votes</p>
	 <a href="<?php echo $http.$domain.$sandbox; ?>login/" class="btn-article-small">Login</a>
	 <a href="<?php echo $http.$domain.$sandbox; ?>join/" class="btn-article-small">Join</a> 
	 <?php
	}
} // fin user not connected
?>
</div> 
 
 
 
 
 