<?php
function remove_accent($str)
{
  $a = array('À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Æ', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ð',
                'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'Ø', 'Ù', 'Ú', 'Û', 'Ü', 'Ý', 'ß', 'à', 'á', 'â', 'ã',
                'ä', 'å', 'æ', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ñ', 'ò', 'ó', 'ô', 'õ',
                'ö', 'ø', 'ù', 'ú', 'û', 'ü', 'ý', 'ÿ', 'Ā', 'ā', 'Ă', 'ă', 'Ą', 'ą', 'Ć', 'ć', 'Ĉ',
                'ĉ', 'Ċ', 'ċ', 'Č', 'č', 'Ď', 'ď', 'Đ', 'đ', 'Ē', 'ē', 'Ĕ', 'ĕ', 'Ė', 'ė', 'Ę', 'ę',
                'Ě', 'ě', 'Ĝ', 'ĝ', 'Ğ', 'ğ', 'Ġ', 'ġ', 'Ģ', 'ģ', 'Ĥ', 'ĥ', 'Ħ', 'ħ', 'Ĩ', 'ĩ', 'Ī', 'ī',
                'Ĭ', 'ĭ', 'Į', 'į', 'İ', 'ı', 'Ĳ', 'ĳ', 'Ĵ', 'ĵ', 'Ķ', 'ķ', 'Ĺ', 'ĺ', 'Ļ', 'ļ', 'Ľ', 'ľ',
                'Ŀ', 'ŀ', 'Ł', 'ł', 'Ń', 'ń', 'Ņ', 'ņ', 'Ň', 'ň', 'ŉ', 'Ō', 'ō', 'Ŏ', 'ŏ', 'Ő', 'ő', 'Œ',
                'œ', 'Ŕ', 'ŕ', 'Ŗ', 'ŗ', 'Ř', 'ř', 'Ś', 'ś', 'Ŝ', 'ŝ', 'Ş', 'ş', 'Š', 'š', 'Ţ', 'ţ', 'Ť', 
                'ť', 'Ŧ', 'ŧ', 'Ũ', 'ũ', 'Ū', 'ū', 'Ŭ', 'ŭ', 'Ů', 'ů', 'Ű', 'ű', 'Ų', 'ų', 'Ŵ', 'ŵ', 'Ŷ', 
                'ŷ', 'Ÿ', 'Ź', 'ź', 'Ż', 'ż', 'Ž', 'ž', 'ſ', 'ƒ', 'Ơ', 'ơ', 'Ư', 'ư', 'Ǎ', 'ǎ', 'Ǐ', 'ǐ',
                'Ǒ', 'ǒ', 'Ǔ', 'ǔ', 'Ǖ', 'ǖ', 'Ǘ', 'ǘ', 'Ǚ', 'ǚ', 'Ǜ', 'ǜ', 'Ǻ', 'ǻ', 'Ǽ', 'ǽ', 'Ǿ', 'ǿ');

  $b = array('A', 'A', 'A', 'A', 'A', 'A', 'AE', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'D', 'N', 'O',
                'O', 'O', 'O', 'O', 'O', 'U', 'U', 'U', 'U', 'Y', 's', 'a', 'a', 'a', 'a', 'a', 'a', 'ae', 'c',
                'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'n', 'o', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u',
                'y', 'y', 'A', 'a', 'A', 'a', 'A', 'a', 'C', 'c', 'C', 'c', 'C', 'c', 'C', 'c', 'D', 'd', 'D',
                'd', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'G', 'g', 'G', 'g', 'G', 'g', 'G', 'g',
                'H', 'h', 'H', 'h', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'IJ', 'ij', 'J', 'j', 'K',
                'k', 'L', 'l', 'L', 'l', 'L', 'l', 'L', 'l', 'L', 'l', 'N', 'n', 'N', 'n', 'N', 'n', 'n', 'O', 'o',
                'O', 'o', 'O', 'o', 'OE', 'oe', 'R', 'r', 'R', 'r', 'R', 'r', 'S', 's', 'S', 's', 'S', 's', 'S',
                's', 'T', 't', 'T', 't', 'T', 't', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'W',
                'w', 'Y', 'y', 'Y', 'Z', 'z', 'Z', 'z', 'Z', 'z', 's', 'f', 'O', 'o', 'U', 'u', 'A', 'a', 'I', 'i',
                'O', 'o', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'A', 'a', 'AE', 'ae', 'O', 'o');
  return str_replace($a, $b, $str);
}
function remplaceGuillemets($texte)
{
    //Places chaque caractères du texte dans un tableau
    $listeCaracteres = str_split($texte, 1);
 
    //Créé un tableau contenant la position de chaque guillemets
    $rechercheGuillemets = array_keys($listeCaracteres, '"');
 
    //Parcours le tableau contenant la position des guillemets
    foreach ($rechercheGuillemets as $key => $value)
    {
        //Remplace alternativement par des guillemets ouvrante ou fermante
        if($key%2 == 0){ $listeCaracteres[$value] = '«'; }
        else{ $listeCaracteres[$value] = '»'; }
    }
 
    //Reconstitue le texte
    return implode('', $listeCaracteres);
}

/* Générateur de Slug (Friendly Url) : convertit un titre en une URL conviviale.*/
function Slug($str){
  return mb_strtolower(preg_replace(array('/[^a-zA-Z0-9 \'-]/', '/[ -\']+/', '/^-|-$/'),
  array('', '-', ''), remove_accent($str)));
}
// function create token
function createatoken($length) {
    $key = '';
    $keys = array_merge(range(0, 9), range('a', 'z'));

    for ($i = 0; $i < $length; $i++) {
        $key .= $keys[array_rand($keys)];
    }

    return $key;
}

// fonctions de récupération des infos du user
function prenomuser($id){
	include("5qzneboiykjnkxfpl76ocs01zmue8x.php");
	$sql = mysql_query("SELECT * FROM ".$prefix."users WHERE id = '$id'");
	while($col = mysql_fetch_array($sql)) { $return_info=$col[prenom]; }
	return ucfirst($return_info);
}
function nomuser($id){
	include("5qzneboiykjnkxfpl76ocs01zmue8x.php");
	$sql = mysql_query("SELECT * FROM ".$prefix."users WHERE id = '$id'");
	while($col = mysql_fetch_array($sql)) { $return_info=$col[nom]; }
	return ucfirst($return_info);
}
function statutuser($id){
	include("5qzneboiykjnkxfpl76ocs01zmue8x.php");
	$sql = mysql_query("SELECT * FROM ".$prefix."users WHERE id = '$id'");
	while($col = mysql_fetch_array($sql)) { $return_info=$col[statut]; }
	return $return_info;
}
function solidaireuser($id){
	include("5qzneboiykjnkxfpl76ocs01zmue8x.php");
	$sql = mysql_query("SELECT * FROM ".$prefix."users WHERE id = '$id'");
	while($col = mysql_fetch_array($sql)) { $return_info=$col[solidaire]; }
	return $return_info;
}
function emailuser($id){
	include("5qzneboiykjnkxfpl76ocs01zmue8x.php");
	$sql = mysql_query("SELECT * FROM ".$prefix."users WHERE id = '$id'");
	while($col = mysql_fetch_array($sql)) { $return_info=$col[email]; }
	return $return_info;
}
function twitteruser($id){
	include("5qzneboiykjnkxfpl76ocs01zmue8x.php");
	$sql = mysql_query("SELECT * FROM ".$prefix."users WHERE id = '$id'");
	while($col = mysql_fetch_array($sql)) { $return_info=$col[twitter]; }
	return $return_info;
}



// fonctions de récupération des infos du college du user
function nomcollege($id,$lang){
	include("5qzneboiykjnkxfpl76ocs01zmue8x.php");
	$sql = mysql_query("SELECT * FROM ".$prefix."colleges WHERE id = '$id'");
	while($col = mysql_fetch_array($sql)) { $titre=$col[titre];  $title=$col[title]; }
	if($titre=="" and $title==""){ 
	if($lang=="fr"){
	$return_college="Assemblée de tous les membres";
	}else{
	$return_college="Assembly of all members";
	} }
	if($lang=="fr"){
	$return_college=$titre;
	}else{
	$return_college=$title;
	} 
	return $return_college;
}

function tagcollege($id){
	include("5qzneboiykjnkxfpl76ocs01zmue8x.php");
	$sql = mysql_query("SELECT * FROM ".$prefix."colleges WHERE id = '$id'");
	while($col = mysql_fetch_array($sql)) { $return_college=$col[tag]; }
	return $return_college;
}

function tarif_simple_college($id){
	include("5qzneboiykjnkxfpl76ocs01zmue8x.php");
	$sql = mysql_query("SELECT * FROM ".$prefix."colleges WHERE id = '$id'");
	while($col = mysql_fetch_array($sql)) { $return_college=$col[tarif_simple]; }
	return $return_college;
}
function tarif_reduit_college($id){
	include("5qzneboiykjnkxfpl76ocs01zmue8x.php");
	$sql = mysql_query("SELECT * FROM ".$prefix."colleges WHERE id = '$id'");
	while($col = mysql_fetch_array($sql)) { $return_college=$col[tarif_reduit]; }
	return $return_college;
}
function tarif_structure_college($id){
	include("5qzneboiykjnkxfpl76ocs01zmue8x.php");
	$sql = mysql_query("SELECT * FROM ".$prefix."colleges WHERE id = '$id'");
	while($col = mysql_fetch_array($sql)) { $return_college=$col[tarif_structure]; }
	return $return_college;
}


// fonctions de récupération des infos d'une brève 
function titlebrief($id){
	include("5qzneboiykjnkxfpl76ocs01zmue8x.php");
	$sql = mysql_query("SELECT * FROM ".$prefix."briefs WHERE id = '$id'");
	while($col = mysql_fetch_array($sql)) { $return_info=$col[titre]; }
	return $return_info;
}
function authorbrief($id){
	include("5qzneboiykjnkxfpl76ocs01zmue8x.php");
	$sql = mysql_query("SELECT * FROM ".$prefix."briefs WHERE id = '$id'");
	while($col = mysql_fetch_array($sql)) { $return_info=$col[id_user]; }
	return $return_info;
}
function statutbrief($id){
	include("5qzneboiykjnkxfpl76ocs01zmue8x.php");
	$sql = mysql_query("SELECT * FROM ".$prefix."briefs WHERE id = '$id'");
	while($col = mysql_fetch_array($sql)) { $return_info=$col[statut]; }
	return $return_info;
}

// fonctions de récupération des infos d'un thread 
function college_thread($id){
	include("5qzneboiykjnkxfpl76ocs01zmue8x.php");
	$sql = mysql_query("SELECT * FROM ".$prefix."threads WHERE id = '$id'");
	while($col = mysql_fetch_array($sql)) { $return_info=$col[id_college]; }
	return $return_info;
}
function posted_thread($id){
	include("5qzneboiykjnkxfpl76ocs01zmue8x.php");
	$sql = mysql_query("SELECT * FROM ".$prefix."threads WHERE id = '$id'");
	while($col = mysql_fetch_array($sql)) { $return_info=$col[posted]; }
	return $return_info;
}

// fonctions de récupération des infos d'un article 
function titlearticle($id){
	include("5qzneboiykjnkxfpl76ocs01zmue8x.php");
	$sql = mysql_query("SELECT * FROM ".$prefix."articles WHERE id = '$id'");
	while($col = mysql_fetch_array($sql)) { $return_info=$col[titre]; }
	return $return_info;
}
function authorarticle($id){
	include("5qzneboiykjnkxfpl76ocs01zmue8x.php");
	$sql = mysql_query("SELECT * FROM ".$prefix."articles WHERE id = '$id'");
	while($col = mysql_fetch_array($sql)) { $return_info=$col[id_user]; }
	return $return_info;
}

// fonctions de récupération des infos d'un article 
function tag_fr($id){
	include("5qzneboiykjnkxfpl76ocs01zmue8x.php");
	$sql = mysql_query("SELECT * FROM ".$prefix."tags WHERE id = '$id'");
	while($col = mysql_fetch_array($sql)) { $return_info=$col[titre_fr]; }
	return $return_info;
}
function tag_en($id){
	include("5qzneboiykjnkxfpl76ocs01zmue8x.php");
	$sql = mysql_query("SELECT * FROM ".$prefix."tags WHERE id = '$id'");
	while($col = mysql_fetch_array($sql)) { $return_info=$col[titre_en]; }
	return $return_info;
}

// fonctions de récupération des infos d'une license 
function licence($license){
if($license == 'artlibre'){ $return_info="Free-Art License"; }
if($license == 'by'){ $return_info="Creative Commons BY"; }
if($license == 'by-sa'){ $return_info="Creative Commons BY-SA"; }	   
if($license == 'by-nc'){ $return_info="Free-Art License"; }
if($license == 'by-nc-sa'){ $return_info="Creative Commons BY-NC"; }
if($license == 'by-nc-nd'){ $return_info="Creative Commons BY-NC-ND"; }
	return $return_info;
}



// fonctions de récupération des infos d'une section (ressources) 
function titre_section($id){
	include("5qzneboiykjnkxfpl76ocs01zmue8x.php");
	$sql = mysql_query("SELECT * FROM ".$prefix."ressources_sections WHERE id = '$id'");
	while($col = mysql_fetch_array($sql)) { $return_info=$col[titre]; }
	return $return_info;
}


// fonctions de récupération des infos d'une cotisation 
function montantcotis($id){
	include("5qzneboiykjnkxfpl76ocs01zmue8x.php");
	$sql = mysql_query("SELECT * FROM ".$prefix."cotisations WHERE id = '$id'");
	while($col = mysql_fetch_array($sql)) { $return_info=$col[montant]; }
	return $return_info;
}
function usercotis($id){
	include("5qzneboiykjnkxfpl76ocs01zmue8x.php");
	$sql = mysql_query("SELECT * FROM ".$prefix."cotisations WHERE id = '$id'");
	while($col = mysql_fetch_array($sql)) { $return_info=$col[id_user]; }
	return $return_info;
}


// fonctions de récupération d'un vote user sur une publication 
function vote_user_article($id_user,$id_article){
	include("5qzneboiykjnkxfpl76ocs01zmue8x.php");
	$sql = mysql_query("SELECT * FROM ".$prefix."users WHERE id = '$id_user'");
	while($col = mysql_fetch_array($sql)) { $solidaire=$col[solidaire]; }
	$myvote=""; 
	if($solidaire=="oui"){ // Si membre solidaire 
		$sql_2 = mysql_query("SELECT * FROM ".$prefix."votes WHERE id_article = '".$id_article."' AND id_user = '".$id_user."'");
		if (mysql_num_rows($sql_2)!=0){  
		while($col = mysql_fetch_array($sql_2)) { $myvote = $col['vote']; }}
	}else{ // si membre simple
		$sql_3 = mysql_query("SELECT * FROM ".$prefix."avis WHERE id_article = '".$id_article."' AND id_user = '".$id_user."'");
		if (mysql_num_rows($sql_3)!=0){  
		while($col = mysql_fetch_array($sql_3)) { $myvote = $col['avis']; }}
	}//endif solidaire	
	return $myvote;
}



		
// FONCTIONS ARTICLE STATUS & VOTE
function statut_vote_article($id_article){
		include("5qzneboiykjnkxfpl76ocs01zmue8x.php");
		$sql_article = mysql_query("SELECT * FROM ".$prefix."articles WHERE id = '".$id_article."'");
		if (mysql_num_rows($sql_article)!=0){  
		while($article = mysql_fetch_array($sql_article)) { 
		$statut_article = $article['statut'];
		$deadline_fin_verif = date("YmdHis", strtotime($article['deadline_proposal'])); }
		$now_verif = date("YmdHis", time());
		//verif deadline
		$statut_vote="unknown";
		if($now_verif >= $deadline_fin_verif) 
		{ 
			$statut_vote="closed";
		}
		if($statut_thread == "locked") {
			$statut_vote="locked";
		}
		if($now_verif <= $deadline_fin_verif and $statut_article=="proposed") 
		{ 
			$statut_vote="open";
		}
		}

	return $statut_vote;
}
function result_vote_article($id_article){
	include("5qzneboiykjnkxfpl76ocs01zmue8x.php");
	$result_nono = mysql_query("SELECT * FROM ".$prefix."votes WHERE id_article = '".$id_article."' AND vote = 'nono'");
	$num_nono = mysql_num_rows($result_nono);
	if ($num_nono!=0){ $result_vote="declined";  }else{ $result_vote="accepted";  }
	return $result_vote;
}
function is_last_voter($id_article,$id_user){
	include("5qzneboiykjnkxfpl76ocs01zmue8x.php");
	//on récupère le nombre de membres solidaires
	$result_solidaires = mysql_query("SELECT * FROM ".$prefix."users WHERE solidaire = 'oui'");
	while($col = mysql_fetch_array($result_solidaires)) { $solidaire_u=$col['solidaire']; }	
	$nb_solidaires = mysql_num_rows($result_solidaires)-1;
	
	//on verifie si l'auteur de l'article fait partie des membres solidaires
	$sql = mysql_query("SELECT * FROM ".$prefix."articles WHERE id = '$id_article'");
	while($col = mysql_fetch_array($sql)) { $id_auteur=$col['id_user']; }
	if ($id_auteur!=0){
	$sql = mysql_query("SELECT * FROM ".$prefix."users WHERE id = '$id_auteur'");
	while($col = mysql_fetch_array($sql)) { $solidaire_a=$col['solidaire']; }
	if ($solidaire_a=="oui"){ $nb_solidaires=$nb_solidaires-1;  }	//si il en fait partie on retire 1 au total
	}
	//on check le nombre de votes enregistrés
	$sql = mysql_query("SELECT * FROM ".$prefix."votes WHERE id_article = '$id_article'");
	$nb_votes_solidaires = mysql_num_rows($sql);
	if ($nb_votes_solidaires==$nb_solidaires){ $result_voter="yes";  }else{ $result_voter="no";  }
	return $result_voter;
}
function everyone_voted($id_article){
	include("5qzneboiykjnkxfpl76ocs01zmue8x.php");
	//on récupère le nombre de membres solidaires
	$result_solidaires = mysql_query("SELECT * FROM ".$prefix."users WHERE solidaire = 'oui'");
	while($col = mysql_fetch_array($result_solidaires)) { $solidaire_u=$col['solidaire']; }	
	$nb_solidaires = mysql_num_rows($result_solidaires);
	
	//on verifie si l'auteur de l'article fait partie des membres solidaires
	$sql = mysql_query("SELECT * FROM ".$prefix."articles WHERE id = '$id_article'");
	while($col = mysql_fetch_array($sql)) { $id_auteur=$col['id_user']; }
	if ($id_auteur!=0){
	$sql = mysql_query("SELECT * FROM ".$prefix."users WHERE id = '$id_auteur'");
	while($col = mysql_fetch_array($sql)) { $solidaire_a=$col['solidaire']; }
	if ($solidaire_a=="oui"){ $nb_solidaires=$nb_solidaires-1;  }	//si il en fait partie on retire 1 au total
	}
	//on check le nombre de votes enregistrés
	$sql = mysql_query("SELECT * FROM ".$prefix."votes WHERE id_article = '$id_article'");
	$nb_votes_solidaires = mysql_num_rows($sql);
	if ($nb_votes_solidaires==$nb_solidaires){ $result_voter="yes";  }else{ $result_voter="no";  }
	return $result_voter;
}


// FONCTIONS THREAD STATUS & VOTE
function statut_vote_thread($id_thread){
		include("5qzneboiykjnkxfpl76ocs01zmue8x.php");
		$sql_thread = mysql_query("SELECT * FROM ".$prefix."threads WHERE id = '".$id_thread."'");
		if (mysql_num_rows($sql_thread)!=0){  
		while($thread = mysql_fetch_array($sql_thread)) { 
		$statut_thread = $thread['statut'];
		$deadline_fin_verif = date("YmdHis", strtotime($thread['deadline'])); }
		$now_verif = date("YmdHis", time());
		//verif deadline
		$statut_vote="unknown";
		if($now_verif >= $deadline_fin_verif) 
		{ 
			$statut_vote="closed";
		}
		if($statut_thread == "locked") {
			$statut_vote="locked";
		}
		if($now_verif <= $deadline_fin_verif and $statut_thread=="published") 
		{ 
			$statut_vote="open";
		}
		}

	return $statut_vote;
}
function result_vote_thread($id_thread) {
	include("5qzneboiykjnkxfpl76ocs01zmue8x.php");

	$result_thread = mysql_query("SELECT * FROM ".$prefix."threads WHERE id = '".$id_thread."'");
	while($col = mysql_fetch_array($result_thread)) { $id_college=$col[id_college]; }

	$result_all = mysql_query("SELECT * FROM ".$prefix."votes WHERE id_thread = '".$id_thread."'");
	$num_all = mysql_num_rows($result_all);
	if ($num_all==0){ $result_vote="declined";  }


	if ($id_college=="1"){ // vote college solidaire (consensus)

	$result_nono = mysql_query("SELECT * FROM ".$prefix."votes WHERE id_thread = '".$id_thread."' AND vote = 'nono'");
	$num_nono = mysql_num_rows($result_nono);
	if ($num_nono!=0){ $result_vote="declined";  }else{ $result_vote="accepted";  }
	
	}else{ // vote tout le monde (statistiques)
	$contre=0;
	$pour=0;
	if ($num_all!=0){ 
	//on liste les votes CONTRE
	$result_no = mysql_query("SELECT * FROM ".$prefix."votes WHERE id_thread = '".$id_thread."' AND vote = 'nono'");
		if (mysql_num_rows($result_no)!=0){ while($article = mysql_fetch_array($result_no)) { 
		$id_voter = $article['id_user']; 
		//on check le college de user
		$sql_college_user = mysql_query("SELECT * FROM ".$prefix."users WHERE id = '$id_voter'");
		while($col = mysql_fetch_array($sql_college_user)) { $id_college_voter=$col['id_college']; }
		if ($id_college_voter==2){ /* membres permanents */ $contre = $contre + 50; }
		if ($id_college_voter==3){ /* membres actifs */ $contre = $contre + 20; }
		if ($id_college_voter==4){ /* membres bienfaiteurs */ $contre = $contre + 10; }
		if ($id_college_voter==5){ /* membres d'honneur */ $contre = $contre + 10; }
		if ($id_college_voter==6){ /* membres spectateurs */ $contre = $contre + 10; }
		}}
	//on liste les votes POUR
	$result_yes = mysql_query("SELECT * FROM ".$prefix."votes WHERE id_thread = '".$id_thread."' AND vote = 'yesyes'");
		if (mysql_num_rows($result_yes)!=0){ while($article = mysql_fetch_array($result_yes)) { 
		$id_voter = $article['id_user']; 
		//on check le college de user
		$sql_college_user = mysql_query("SELECT * FROM ".$prefix."users WHERE id = '$id_voter'");
		while($col = mysql_fetch_array($sql_college_user)) { $id_college_voter=$col['id_college']; }
		if ($id_college_voter==2){ /* membres permanents */ $pour = $pour + 50; }
		if ($id_college_voter==3){ /* membres actifs */ $pour = $pour + 20; }
		if ($id_college_voter==4){ /* membres bienfaiteurs */ $pour = $pour + 10; }
		if ($id_college_voter==5){ /* membres d'honneur */ $pour = $pour + 10; }
		if ($id_college_voter==6){ /* membres spectateurs */ $pour = $pour + 10; }
		}}
	if ($contre >= $pour){ $result_vote="declined";  }else{ $result_vote="accepted";  }
	}else{ $result_vote="declined"; }
	
	}// fin vote tout le monde 

	$result_all = mysql_query("SELECT * FROM ".$prefix."votes WHERE id_thread = '".$id_thread."'");
	$num_all = mysql_num_rows($result_all);
	if ($num_all==0){ $result_vote="declined";  }
	
	return $result_vote;
}










?>