

<?php
include("variables.php");
include("login.php"); //include config file
//sanitize post value
$page_number = filter_var($_POST["page"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH);
$item_per_page=5;
//throw HTTP error if page number is not valid
if(!is_numeric($page_number)){
	header('HTTP/1.1 500 Invalid page number!');
	exit();
}

//get current starting point of records
$position = ($page_number * $item_per_page);

//Limit our results within a specified range. 
$results = mysql_query("SELECT * FROM ".$prefix."articles ORDER BY id DESC LIMIT $position, $item_per_page");




//output results from database
?>
<!-- democracy-table -->
<div class="table-responsive">
  <table class="table table-inbox table-hover">
    <tbody>
<?php
while($r = mysql_fetch_array($results)) { //on récupère les infos de l'article
$id_article = $r[id];
$id_auteur = $r[id_user];
$title_article = $r[titre];
$token_article = $r[token];
$token_user_article = $r[token_user];
$statut = $r[statut];
$edition = $r[edition];
$signature = $r[signature];
if($id_auteur !="0"){ $prenom_auteur = prenomuser($id_auteur); }else{ $prenom_auteur = $signature; }
$nom_auteur = nomuser($id_auteur);
$deadline_proposal_cd = date("Y/m/d H:i:s", strtotime($r['deadline_proposal']));
$statut_vote = statut_vote_article($id_article);
$resultat_vote=result_vote_article($id_article);
$voted=""; $voted=vote_user_article($id_user_online,$id_article);
$deadline = date("c",strtotime($r['deadline_proposal']));
if($statut=="published") { $posted = date("c",strtotime($r['date_published']));  }
elseif ($statut=="accepted" or $statut=="declined") { $posted = date("c",strtotime($r['deadline_proposal']));  }
elseif ($statut=="proposed") { $posted = date("c",strtotime($r['date_save']));   }

// on récupère le nb de réponses et la date de la dernière
$sql_replies = mysql_query("SELECT * FROM ".$prefix."replies WHERE id_article = '".$id_article."' ORDER BY id");
$nb_rep = mysql_num_rows($sql_replies);
if($nb_rep!="0"){
	
			while($reply = mysql_fetch_array($sql_replies)) { 
			$posted=date("c",strtotime($reply['posted']));
			$id_user_reply=$reply['id_user']; $prenom_auteur_reply=prenomuser($id_user_reply);
			}

}
// on récupère le nombre de votes
$result_votes = mysql_query("SELECT * FROM ".$prefix."votes WHERE id_article = '".$id_article."'");
$num_votes = mysql_num_rows($result_votes);
$result_avis = mysql_query("SELECT * FROM ".$prefix."avis WHERE id_article = '".$id_article."'");
$num_avis = mysql_num_rows($result_avis);
$urlt= "../article.php?tk=$token_article";

	$read="unread";
	$sql = mysql_query("SELECT * FROM ".$prefix."threads_views WHERE id_thread = '$id_thread' and id_user = '$id_user_online'");
	if(mysql_num_rows($sql)==0 or ($voted=="" and $statut=="proposed" and $statut_vote=="open")) { 
	$read="unread"; 	
	} else { 
	$read="read"; 
	 }





?>

<tr class="<?php echo $read; ?>" onclick="javascript:location.href='<?php echo $urlt; ?>'">

  
   <!-- star -->
   <td class="">
	      <?php 
	      if ($statut=="draft") { echo "<i class=\"cdimgrey fa fa-flask\"></i>"; }
	      if ($statut=="proposed" and $statut_vote=="open") {echo "<i class=\"cpurple fa fa-clock-o fa-spin\"></i>"; }
	      if ($statut=="proposed" and $statut_vote=="closed") {
	      
		       if($resultat_vote=="accepted") { 
		       mysql_query("UPDATE ".$prefix."articles SET statut = 'accepted' WHERE id = '".$id_article."'");
		       $statut="accepted";
		       }
		       if($resultat_vote=="declined"){
		       mysql_query("UPDATE ".$prefix."articles SET statut = 'declined' WHERE id = '".$id_article."'");
		       $statut="declined";
		       }
	      
	      
	      }
	      if ($statut=="published") { echo "<i class=\"cgreen fa fa-check-circle\"></i>"; }
	      if ($statut=="accepted") { echo "<i class=\"cgreen fa fa-clock-o fa-spin\"></i>"; }
	      if ($statut=="declined") { echo "<i class=\"credlight fa fa-ban\"></i>"; }
	      if ($statut=="unpublished") { echo "<i class=\"credlight fa fa-exclamation-triangle\"></i>"; }
	      if ($statut=="pb publication") { echo "<i class=\"credlight fa fa-exclamation-triangle\"></i>"; }
	      
	       ?>   
   </td>
   <!-- #star -->



  <!-- titre -->
  <td class="view-message ">
<?php 

if($voted=="" and $statut_vote=="open" and $token_user_article!=$token_user_online){
	if($lang=="fr"){ 
	echo "<span class='cpurple'>Votez : </span>";
	}else{ 
	echo "<span class='cpurple'>Vote: </span>";
	}
}//endif $voted
echo "<a href='$urlt'>$title_article</a>";?>
  </td>
  <!-- #titre -->
  

  
  <!-- date -->
  <td class="view-message inbox-small-cells  text-right">

<?php if ($statut_vote=="open") { ?>
	<?php if($lang=="fr"){ ?> 
	<span class="cpurple">Fin du vote <time datetime="<?php echo $deadline; ?>" class="age"><?php echo $deadline; ?></time></span> <span class="cgrey2"> | </span>
	<?php }else{  ?>
	<span class="cpurple">Voting deadline <time datetime="<?php echo $deadline; ?>" class="age"><?php echo $deadline; ?></time></span> <span class="cgrey2"> | </span>
	<?php } ?>
<?php }?>

	    <?php 
	    if($nb_avis!="0"){ $num_votes = $num_votes + $num_avis; }
	     if($nb_rep!="0"){echo "$nb_rep <i class='fa fa-comment'></i>";}
		 if($num_votes!="0"){echo " &nbsp; $num_votes <i class='fa fa-legal fa-flip-horizontal'></i>";}
		 if($vote=="oui" and $num_votes!="0"){echo " &nbsp; $num_votes <i class='fa fa-legal fa-flip-horizontal'></i>";}
		 if($nb_rep!="0" or $num_votes!="0"){echo " <span class='cgrey2'> | </span> ";}
	     ?> 
		
	    <?php
	    if($statut=="unpublished"){ 

			if($lang=="fr"){
			echo "<span class='ctomato'>Dépublié</span> ";
			}else{ 
			echo "<span class='ctomato'>Unpublished</span> ";
			}

			$sql_modifs = mysql_query("SELECT * FROM ".$prefix."articles_modifs WHERE id_article = '".$id_article."' AND detail = 'unpublish' ORDER BY id DESC LIMIT 1");
			$nb_modifs = mysql_num_rows($sql_modifs);
			if($nb_modifs!="0"){
				while($m = mysql_fetch_array($sql_modifs)) { 
				$posted=date("c",strtotime($m['date']));
				}	
			}//endif($nb_modifs!="0")

		    
	    }else{ 
		if ($nb_rep!="0") { echo $prenom_auteur_reply; }
		else { echo $prenom_auteur; }
		}
	     ?> - <time datetime="<?php echo $posted; ?>" class="age"><?php echo $posted; ?></time> 
		
	    <?php 
	      if ($edition=="fr") { //edition fr 
	      echo " <span class='label label-default pull-right'>FR</span>";
	      }
	      if ($edition=="en") { //edition fr 
	      echo " <span class='label label-default pull-right'>EN</span>";
	      }
	     ?> 

  </td>
  <!-- #date -->



  


</tr>




<?php
$nb_threadsVisibles=$nb_threadsVisibles+1;






}// End of Array threads

?>



</tbody>
</table>
</div>

<!-- #democracy-table -->


	<script type="text/javascript" src="<?php echo $http.$domain.$sandbox; ?>kitchen/js/jquery-2.1.1.min.js"></script>


	<script type="text/javascript" src="<?php echo $http.$domain.$sandbox; ?>public/js/jquery-2.1.1.min.js"></script>

		<?php if($lang=="fr"){  ?>
		<script src="<?php echo $http.$domain.$sandbox; ?>public/js/jquery.ageFR.js"></script>
		<?php }else{ ?>
		<script src="<?php echo $http.$domain.$sandbox; ?>public/js/jquery.age.js"></script>
		<?php } ?><script type="text/javascript">
  $('.age').age();
</script> 



