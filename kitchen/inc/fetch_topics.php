

<?php
include("variables.php");
include("login.php"); //include config file
//sanitize post value
$page_number = filter_var($_POST["page"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH);
$item_per_page=5;
//throw HTTP error if page number is not valid
if(!is_numeric($page_number)){
	header('HTTP/1.1 500 Invalid page number!');
	exit();
}

//get current starting point of records
$position = ($page_number * $item_per_page);

//Limit our results within a specified range. 


if($solidaire_user_online=="oui" and $college_user_online==2){ // solidaire et permament
$results = mysql_query("SELECT * FROM ".$prefix."threads WHERE statut != 'archived' ORDER BY last_reply DESC LIMIT $position, $item_per_page");
}elseif($solidaire_user_online=="oui" and $college_user_online!=2){ //solidaire mais pas permanent
$results = mysql_query("SELECT * FROM ".$prefix."threads WHERE id_college = '0' OR id_college = '1' AND statut != 'archived' ORDER BY last_reply DESC LIMIT $position, $item_per_page");
}else{ // pas solidaire
$results = mysql_query("SELECT * FROM ".$prefix."threads WHERE id_college = '0' AND statut != 'archived' ORDER BY last_reply DESC LIMIT $position, $item_per_page");
}



//output results from database
?>
<!-- democracy-table -->
<div class="table-responsive">
  <table class="table table-inbox table-hover">
    <tbody>
<?php
while($thread = mysql_fetch_array($results)) { //on récupère les infos du thread
$resultat_vote="";

$id_thread=$thread[id];
$id_auteur = $thread[id_user];
$destination = $thread[id_college];
$edition = $thread[edition];

$title_thread = $thread[title];
$posted = date("jS M Y h:i",$thread[posted]);
$ladate = date("c",strtotime($thread[posted]));
$id_auteur_question = $thread[id_user];
$deadline=date("c",strtotime($thread[deadline]));
$deadline_fin_display=date("d M Y", $thread[deadline]);
$statut_thread=$thread[statut];

$replies_thread=$thread[replies];
$last_reply=$thread[last_reply];

//COMPARAISON DE DATES
$datetime1 = new DateTime($last_reply);
$datetime2 = new DateTime($aujourdhui);
$interval = $datetime1->diff($datetime2);
$interval = $interval->format('%a');
$recent="non"; if($interval<5) {$recent="oui";}

//SI VOTE : formatage des dates deadline
$vote=$thread[vote]; 
if($vote=="oui"){
// on récupère le nombre de votes
$result_votes = mysql_query("SELECT * FROM ".$prefix."votes WHERE id_thread = '".$id_thread."'");
$num_votes = mysql_num_rows($result_votes);
$statut_vote = statut_vote_thread($id_thread);
$resultat_vote=result_vote_thread($id_thread);
}//endif vote==oui

// on récupère la date de la dernière réponse et l'auteur
if ($replies_thread!="0")
{
	$sql_replies = mysql_query("SELECT * FROM ".$prefix."replies WHERE id_thread = '".$id_thread."' ORDER BY posted DESC LIMIT 1");
	while($reply = mysql_fetch_array($sql_replies)) {
	$posted = date("jS M Y h:i",$reply['posted']);
	$ladate = date("c",strtotime($reply['posted']));
	$id_auteur_reply = $reply['id_user'];
	$id_reply=$reply['id'];
	}
}

//on récupère le nom de l'auteur
$prenom_auteur=""; $nom_auteur=""; $prenom_auteur_reply=""; $nom_auteur_reply=""; 
$prenom_auteur =prenomuser($id_auteur); $nom_auteur=nomuser($id_auteur);
$prenom_auteur_reply =prenomuser($id_auteur_reply); $nom_auteur_reply=nomuser($id_auteur_reply);


// Now we will show the available threads
include("autorisations-get.php"); // need $id_college and $statut_thread
$token="rw";
if($token=="r" or $token=="rw"){ // IF TOKEN ok-item
?>

	    <?php 
	      if ($vote=="oui") { 
	      $urlt= "vote.php?id=$id_thread";
	      //if($replies_thread!="0"){$urlt.= "#$id_reply";}
	      }
	      else{
	      $urlt= "topic.php?id=$id_thread";
	      //if($replies_thread!="0"){$urlt.= "#$id_reply";}
	      }     
	     ?> 



<?php
	$read="unread";
	$sql = mysql_query("SELECT * FROM ".$prefix."threads_views WHERE id_thread = '$id_thread' and id_user = '$id_user_online'");
	if(mysql_num_rows($sql)==0) { 
	$read="unread"; 	
	} else { 
	$read="read"; 
	 }
//on verifie les reply
if($replies_thread!="0"){
	
			$sql_replies = mysql_query("SELECT * FROM ".$prefix."replies WHERE id_thread = '".$id_thread."' ORDER BY posted DESC");
			while($reply = mysql_fetch_array($sql_replies)) { 
			$id_reply=$reply[id];
			$sql2 = mysql_query("SELECT * FROM ".$prefix."threads_views WHERE id_reply = '$id_reply' and id_user = '$id_user_online'");
			if(mysql_num_rows($sql2)==0) { $read="unread"; }
			}

}


?>

<tr class="<?php echo $read; ?>" onclick="javascript:location.href='<?php echo $urlt; ?>'">

  
   <!-- star -->
   <td class="">
<?php if($vote=="oui" and $statut_vote=="closed" and $resultat_vote=="accepted") {  ?>  
   <i class="fa fa-balance-scale cgreen"></i>
<?php }elseif($vote=="oui" and $statut_vote=="closed" and $resultat_vote=="declined") {  ?>   
   <i class="fa fa-balance-scale ctomato"></i>
<?php }elseif($vote=="oui" and $statut_vote=="open") {  ?>   
   <i class="fa fa-balance-scale cpurple"></i>
<?php }else{ ?>   
   <i class="fa fa-comment cgrey2"></i> 
<?php } ?>     
   </td>
   <!-- #star -->




  <!-- titre -->
  <td class="view-message ">
	      <?php 
	      if ($vote=="oui" or $vote=="article") {
			  // rappel si l'utilisateur n'a pas encore voté
			  $sqltestvote = mysql_query("SELECT * FROM ".$prefix."votes WHERE id_thread = '".$id_thread."' AND id_user = '".$id_user_online."'");
			  if(mysql_num_rows($sqltestvote)==0 and $statut_vote=="open") 
			  { if($id_auteur_question!=$id_user_online) {echo "<span class=\"cpurple\"><b>Votez : </b></span>"; }  } 
			  
		  echo "<a href='vote.php?id=$id_thread";
	     // if($replies_thread!="0" and $vote!="article"){echo "#$id_reply";}
	      echo "'>".$title_thread."</a>";
	      
	      
	      }
	      else{
	      echo "<a href='topic.php?id=$id_thread";
	      //if($replies_thread!="0"){echo "#$id_reply";}
	      echo "'>".$title_thread."</a>";
	      }
	      if ($vote=="oui" or $vote=="article") {


				if($statut_vote=="open")
				{ // le vote est toujours ouvert

				}elseif($statut_vote=="locked")
				{ // le vote est verrouillé
				echo "&nbsp; <span class=\"cgrey4\">[locked]</span>";
				}
				else
				{// le vote est clos
				echo "&nbsp; <span class=\"deepskyblue\">[Vote clos]</span>";
				}	
				
	      }
	       
	       ?>

  </td>
  <!-- #titre -->
  
  <td class="view-message">

	    <?php 
	      if ($destination=="2") { //membres permanents 
	      echo "<span class='label label-default pull-right'>Perm</span>";
	      }
	      elseif ($destination=="1") { //membres solidaires 
	      echo "<span class='label label-primary pull-right'>Solidaires</span>";
	      }elseif ($destination=="0") { //membres solidaires 
	      echo "<span class='label label-primary pull-right'>A.G.</span>";
	      }
	     ?>   
  
  </td>
  

 
  <!-- nb-reply -->
  <td class="view-message inbox-small-cells text-right">
	<?php if($statut_vote=="open"){ ?> 
		<?php if($lang=="fr"){ ?> 
		<span class="cpurple">Fin du vote <time datetime="<?php echo $deadline; ?>" class="age"><?php echo $deadline; ?></time></span> <span class="cgrey2"> | </span>
		<?php }else{  ?>
		<span class="cpurple">Voting deadline <time datetime="<?php echo $deadline; ?>" class="age"><?php echo $deadline; ?></time></span> <span class="cgrey2"> | </span>
		<?php } ?>
	<?php } ?>
	    <?php 
	      if($replies_thread!="0"){
		  echo " ".$replies_thread.""; 
		  echo " <i class='fa fa-comment'></i>";
		  }
		  if($vote=="oui" and $num_votes!="0"){echo " &nbsp; $num_votes <i class='fa fa-legal fa-flip-horizontal'></i>";}
		  if($replies_thread!="0" or $num_votes!="0"){echo "<span class='cgrey2'> | </span> ";}
	     ?> 

	    <?php 
		if ($replies_thread!="0") { echo $prenom_auteur_reply; }
		else { echo $prenom_auteur; }
	     ?> - <time datetime="<?php echo $ladate; ?>" class="age"><?php echo $ladate; ?></time>
  </td>

</tr>





<?php
$nb_threadsVisibles=$nb_threadsVisibles+1;
}// ENDIF TOKEN OK-item





}// End of Array threads

?>



</tbody>
</table>
</div>

<!-- #democracy-table -->


	<script type="text/javascript" src="<?php echo $http.$domain.$sandbox; ?>kitchen/js/jquery-2.1.1.min.js"></script>


	<script type="text/javascript" src="<?php echo $http.$domain.$sandbox; ?>public/js/jquery-2.1.1.min.js"></script>

		<?php if($lang=="fr"){  ?>
		<script src="<?php echo $http.$domain.$sandbox; ?>public/js/jquery.ageFR.js"></script>
		<?php }else{ ?>
		<script src="<?php echo $http.$domain.$sandbox; ?>public/js/jquery.age.js"></script>
		<?php } ?><script type="text/javascript">
  $('.age').age();
</script> 



