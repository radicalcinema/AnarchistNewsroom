<?php $item_per_page=5; ?>
<script type="text/javascript">
$(document).ready(function() {

	var track_click2 = 0; //track user click on "load more" button, righ now it is 0 click
	
	var total_pages2 = <?php echo $total_articles; ?>;
	$('#results2').load("inc/fetch_articles.php", {'page':track_click2}, function() {track_click2++;}); //initial data to load

	$("#load_more_button2").click(function (e) { //user clicks on button
	
		$(this).hide(); //hide load more button on click
		$('.animation_image2').show(); //show loading image

		if(track_click2 <= total_pages2) //make sure user clicks are still less than total pages
		{
			//post page number and load returned data into result element
			$.post('inc/fetch_articles.php',{'page': track_click2}, function(data) {
			
				$("#load_more_button2").show(); //bring back load more button
				
				$("#results2").append(data); //append data received from server
				
				//scroll page to button element
				$("html, body").animate({scrollTop: $("#load_more_button2").offset().top}, 500);
				
				//hide loading image
				$('.animation_image2').hide(); //hide loading image once data is received
	
				track_click2++; //user click increment on load button
			
			}).fail(function(xhr, ajaxOptions, thrownError) { 
				alert(thrownError); //alert any HTTP error
				$("#load_more_button2").show(); //bring back load more button
				$('.animation_image2').hide(); //hide loading image once data is received
			});
			
			
			if(track_click2 >= total_pages2-1)
			{
				//reached end of the page yet? disable load button
				$("#load_more_button2").attr("disabled", "disabled");
			}
		 }
		  
		});
});
</script>
<p class="petit_titre"><span>Articles</span></p>
<div id="results2"><div class="animation_image2"><img src="images/ajax-loader.gif"> Loading...</div></div>

<div align="center">
<button class="btn-grey-small" id="load_more_button2"><i class="fa fa-plus-square"></i> Afficher plus</button>
<div class="animation_image2" style="display:none;"><img src="images/ajax-loader.gif"> Loading...</div>
</div>
