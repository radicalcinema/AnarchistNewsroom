<?php
$private="yes"; // accès réservé
$title_page="Nouveau vote";
include("inc/variables.php");
require("inc/login.php"); // Contrôle d'identité :-/ 

    if($solidaire_user_online!="oui"){
        $errors[] = "Nos statuts prévoient que seuls les membres du collège solidaire peuvent proposer un vote d'orientation.";
    }


if(isset($_POST['postage'])){ //nouveau sujet
    if($_POST['titre'] == ''){
        $errors[] = "Le titre est vide.";
    }
    if($_POST['texte'] == ''){
        $errors[] = "Le texte est vide.";
    }
    if($_POST['id_college'] == ''){
        $errors[] = "La destination est vide.";
    }
    if($_POST['delai'] == ''){
        $errors[] = "Delai est vide.";
    }
    if($id_user_online == ''){
        $errors[] = "Probl&egrave;me d'identification, qui &ecirc;tes-vous??!";
    }
$id_college=$_POST['id_college'];
$titre=strip_tags($_POST['titre']); 
$texte=$_POST['texte'];
$delai=$_POST['delai'];
$time = time();
$maintenant = date("Y-m-d H:i:s", $time);
if($delai == '1'){ $deadline_proposal = date("Y-m-d H:i:s", strtotime('now +1 Hour')); }
elseif($delai == '2'){ $deadline_proposal = date("Y-m-d H:i:s", strtotime('now +2 Hours')); }
elseif($delai == '24'){ $deadline_proposal = date("Y-m-d H:i:s", strtotime('now +24 Hours')); }
elseif($delai == '48'){ $deadline_proposal = date("Y-m-d H:i:s", strtotime('now +48 Hours')); }
elseif($delai == '72'){ $deadline_proposal = date("Y-m-d H:i:s", strtotime('now +72 Hours')); }
else{ $deadline_proposal = date("Y-m-d H:i:s", strtotime('now +168 Hours')); }
if(count($errors) == 0){//si pas d'erreurs, on enregistre
mysql_query("INSERT INTO `".$prefix."threads` (`id`, `title`, `message`, `id_user`, `id_college`, `vote`, `deadline`, `replies`, `posted`, `last_reply`, `statut`) VALUES (NULL, '$titre', '".$texte."', '$id_user_online', '$id_college', 'oui', '$deadline_proposal', '0', '$maintenant', '$maintenant', 'published');");
$id_thread=mysql_insert_id();
header('Location: vote.php?id='.$id_thread);
}//fin pas d'erreur
}//fin nouveau sujet
include("inc/template_top.php"); // template
?>







<?php

?>





<h4 class="sub-header"><i class="fa fa-comments"></i> <?php echo $title_page; ?></h4>




	<form class="form-horizontal" role="form" action="" method="post">
	<div class="row">
    
        <div class="timeline-centered">


<!-- PREMIER SUJET -->
        <article class="timeline-entry">

            <div class="timeline-entry-inner">

                <div class="timeline-icon bg-thread">
                    <i class="fa fa-comment"></i>
                </div>

                <div class="timeline-label">
                	<p><input type="text" class="form-control" id="titre" name="titre" value="<?php echo $titre; ?>" maxlength="70" placeholder="titre" required></p>
                	<p>		<div class="radio" style="padding:0px 0px 0px 40px;">
		  <label>
		    <input type="radio" name="id_college" id="id_college0" value="0" required checked>
		     Assemblée générale de tous les membres
		  </label>
		</div>
		<?php
		//on recupere les colleges
		$sql = mysql_query("SELECT * FROM ".$prefix."colleges ORDER BY id");
		while($r = mysql_fetch_array($sql)) 
			{ 
			$id_college=$r[id];
			$titre_col=$r[titre];
			if($id_col==college_thread($id)){ $tchecked="yes";} else{ $tchecked="no";}
		
		$display_token="no";
		include("inc/autorisations-college.php");
		
		if($token=="rw"){ 
		?>
		<div class="radio" style="padding:0px 0px 0px 40px;">
		  <label>
		    <input type="radio" name="id_college" id="id_college<?php echo $id_college; ?>" value="<?php echo $id_college; ?>" required>
		     <?php echo $titre_col; ?> 
		  </label>
		</div>
		<?php
			}//#token
		?>
		<?php
			}//endwhile
		?></p>
           <script>
tinymce.init({
  selector: 'div#texte',
  inline: false,
  plugins: [
    'link',
    'code'
  ],
  toolbar: 'bold italic link',
  menubar: false
});
  </script>
                    <div id="texte" class="textesimple"></div>
	      <p><b>Délai pour le vote :</b>
	      <br />	<div class="radio" style="padding:0px 0px 0px 40px;">
	  <label>
	    <input type="radio" name="delai" id="delai2" value="2">
	    <span class=""> 2 heures</span>
	  </label>
	</div>
	<div class="radio" style="padding:0px 0px 0px 40px;">
	  <label>
	    <input type="radio" name="delai" id="delai3" value="24">
	    <span class=""> 24 heures</span>
	  </label>
	</div>
	<div class="radio" style="padding:0px 0px 0px 40px;">
	  <label>
	    <input type="radio" name="delai" id="delai4" value="48">
	    <span class=""> 2 jours</span>
	  </label>
	</div>
	<div class="radio" style="padding:0px 0px 0px 40px;">
	  <label>
	    <input type="radio" name="delai" id="delai5" value="72">
	    <span class=""> 3 jours</span>
	  </label>
	</div>  
	<div class="radio" style="padding:0px 0px 0px 40px;">
	  <label>
	    <input type="radio" name="delai" id="delai6" value="168" checked>
	    <span class=""> Une semaine</span>
	  </label>
	</div></p>        

	  <p class="txtright" style="padding:20px 0px 20px 0px;"><input type="hidden" name="postage" value="yes">
	  <input type="submit" class="btn-article-small" value="Publier" />  
	  </p>
                </div>
            </div>

        </article>
<!-- #PREMIER SUJET -->




        <article class="timeline-entry begin">

            <div class="timeline-entry-inner">

                <div class="timeline-icon  bg-black" style="-webkit-transform: rotate(-90deg); -moz-transform: rotate(-90deg);">
                    <i class="entypo-flight"></i> +
                </div>

            </div>

        </article>

    </div>

    
	</div><!-- .row -->


	</form>









<?php
include("inc/template_bottom.php"); // insert bas de page
?>

