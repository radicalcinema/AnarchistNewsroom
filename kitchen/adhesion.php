<?php
$private="yes"; // accès réservé
$private_rule="waiting"; // Page privée mais accessible aux statut user = waiting
include("inc/variables.php");
require("inc/login.php"); // Contrôle d'identité :-/ 


if($lang=="fr"){
$title_page="Adhesion";
}else{
$title_page="Subscription";
}
include("inc/template_top.php"); // template

?>
<?php
/////////////// Suppression de ligne
if(isset($_GET['del'])) {
$del=$_GET['del'];
$sql = mysql_query("SELECT * FROM ".$prefix."cotisations WHERE id = '$del'");
	$nb_rep_request = mysql_num_rows($sql);
	if($nb_rep_request!=0)
	{
		mysql_query("DELETE FROM ".$prefix."cotisations WHERE id = '$del'");
	}
}//ENDif(isset($_GET['del']))
?>



<?php
//on verifie si il est à jour de cotisation
include("inc/token-cotisation.php");

if($statut_user_online=="waiting"){
?>

<?php
if($token_cotisation=="outdated"){
?>
<div class="alert alert-info" role="alert">
Vous n'êtes plus à jour de cotisation..
</div>
<?php
}elseif($token_cotisation=="none"){
?>
	<?php if($lang=="fr"){ ?> 
	<div class="alert alert-info" role="alert">
	Votre compte n'est pas encore activé.
	<br />R&eacute;glez votre premi&egrave;re cotisation pour activer votre compte et acc&eacute;der &agrave; la <i>cuisine</i>.
	</div>
	<?php }else{  ?>
	<div class="alert alert-info" role="alert">
	Your account is not yet activated.
	<br />Set your first contribution to activate your account and access to the <i>kitchen</i>.
	</div>
	<?php } ?>
<?php
}elseif($token_cotisation=="waiting_ok"){
?>
	<?php if($lang=="fr"){ ?> 
	<div class="alert alert-info" role="alert">
	<i class="fa fa-cogs fa-spin"></i> Votre première cotisation est en cours de traitement.
	</div>
	<?php }else{  ?>
	<div class="alert alert-info" role="alert">
	<i class="fa fa-cogs fa-spin"></i> Your first contribution is being processed.
	</div>
	<?php } ?>
<?php
}elseif($token_cotisation=="canceled"){
?>
	<?php if($lang=="fr"){ ?> 
	<div class="alert alert-info" role="alert">
	Votre dernière cotisation a été rejetée. 
	</div>
	<?php }else{  ?>
	<div class="alert alert-info" role="alert">
	Your last contribution was rejected.
	</div>
	<?php } ?>
<?php
}
?>



<?php
}//endif($statut_user_online=="waiting")
?>





<div class="row no-gutters"><!-- row -->
<div class="col-md-8"><!-- left -->




<!-- INFOS USER -->

<h4><?php echo $prenom_user_online." ".$nom_user_online; ?></h4>
<?php
$date_inscription=date("d M Y", strtotime($date_user_online));
$heure_inscription=date("H:i", strtotime($date_user_online));
?>
<?php if($lang=="fr"){ ?> 
<p class="" style="line-height:20px;">Vous êtes membre de <i>Radical Cinema</i> depuis le <span class=""><?php echo $date_inscription." à ".$heure_inscription; ?></span></p>
<p class="">au sein du <span class=""><?php echo nomcollege($college_user_online,$lang); ?></span> 
<?php }else{  ?>
<p class="" style="line-height:20px;">You're member of <i>Radical Cinema</i> since <span class=""><?php echo $date_inscription." at ".$heure_inscription; ?></span></p>
<p class="">within the <span class=""><?php echo nomcollege($college_user_online,$lang); ?></span> 
<?php } ?>
<?php
if($solidaire_user_online=="oui"){ ?> 
	<?php if($lang=="fr"){ ?> 
	et du <span class="">Collège solidaire</span>
	<?php }else{  ?>
	and the <span class="">Solidarity College</span>
	<?php } ?>
<?php } ?>
</p>
<?php if($statut_user_online=="admin"){ ?>
<p class="cgrey4">Vous avez également le statut d'<span class="">administrateur technique</span>
<?php if($tresorier==$id_user_online){ ?> et celui de <span class="">trésorier</span><?php } ?>.</p>
<?php } ?>
<?php
// tarif de la cotisation
if($tarif_user_online=="tarif_simple"){ $tarif=tarif_simple_college($college_user_online); }
if($tarif_user_online=="tarif_reduit"){ $tarif=tarif_reduit_college($college_user_online); }
if($tarif_user_online=="tarif_structure"){ $tarif=tarif_structure_college($college_user_online); }
?>
<!-- # INFOS USER -->


</div><!-- #left -->
<div class="col-md-4"><!-- right -->
	<div class="blocadhesion"><!-- blocadhesion -->
<?php

if($token_cotisation=="ok")
{ ?>
	<?php if($lang=="fr"){ ?> 
	<p class="txtcenter cgrey4">Vous êtes à jour de cotisation.</p>
	<div class="padd10"><a href="payer.php" class="btn-article-small"><i class="fa fa-credit-card"></i> &nbsp; Régler la prochaine échéance</a></div>
	<?php }else{  ?>
	<p class="txtcenter cgrey4">You are to date of contribution.</p>
	<div class="padd10"><a href="payer.php" class="btn-article-small"><i class="fa fa-credit-card"></i> &nbsp; Pay the next one</a></div>
	<?php } ?>
<?php
}elseif($token_cotisation=="outdated"){
?>
	<?php if($lang=="fr"){ ?> 
	<p class="txtcenter cred"><i class="fa fa-exclamation-triangle"></i> Vous n'êtes plus à jour de cotisation.</p>
	<div class="padd10"><a href="payer.php" class="btn-article-small"><i class="fa fa-credit-card"></i> &nbsp; Régler</a></div>
	<?php }else{  ?>
	<p class="txtcenter cred"><i class="fa fa-exclamation-triangle"></i> You're not to date of contribution.</p>
	<div class="padd10"><a href="payer.php" class="btn-article-small"><i class="fa fa-credit-card"></i> &nbsp; Fix now</a></div>
	<?php } ?>
<?php
}elseif($token_cotisation=="none"){
?>
	<?php if($lang=="fr"){ ?> 
	<p class="txtleft"><i class="fa fa-bookmark"></i> Pour terminer votre inscription vous êtes invité-e à régler votre première cotisation. </p>
	<div class="padd10"><a href="payer.php" class="btn-article-small"><i class="fa fa-credit-card"></i> &nbsp; Régler</a></div>
	<?php }else{  ?>
	<p class="txtleft"><i class="fa fa-bookmark"></i> To complete your registration you are asked to adjust your first contribution. </p>
	<div class="padd10"><a href="payer.php" class="btn-article-small"><i class="fa fa-credit-card"></i> &nbsp; Fix now</a></div>
	<?php } ?>
<?php
}elseif($token_cotisation=="waiting_ok"){
?>
	<?php if($lang=="fr"){ ?> 
	<p class="txtleft"><i class="fa fa-cogs fa-spin"></i> Votre première cotisation est en cours de traitement</p>
	<div class="padd10"><a href="payer.php" class="btn-article-small"><i class="fa fa-eye"></i> &nbsp; Afficher</a></div>
	<?php }else{  ?>
	<p class="txtleft"><i class="fa fa-cogs fa-spin"></i> Your first contribution is being processed</p>
	<div class="padd10"><a href="payer.php" class="btn-article-small"><i class="fa fa-eye"></i> &nbsp; Display</a></div>
	<?php } ?>
<?php
}elseif($token_cotisation=="canceled"){
?>
	<?php if($lang=="fr"){ ?> 
	<p class="txtleft"><i class="fa fa-exclamation-triangle"></i> Votre dernière cotisation a été rejetée. Vous pouvez la supprimer dans le tableau ci-dessous et recommencer la procédure.
		<br /> Si vous estimez qu'il s'agit d'une erreur, contactez le trésorier à l'adresse <b>adminstration@radicalcinema.org</b>
	</p>
	<?php }else{  ?>
	<p class="txtleft"><i class="fa fa-exclamation-triangle"></i> Your last contribution was rejected. You can remove in the table below and start again.
		<br />If you feel that this is an error, contact the treasurer at <b>adminstration@radicalcinema.org</b>
	</p>
	<?php } ?>
<?php
}elseif($token_cotisation=="honneur"){
?>
	<?php if($lang=="fr"){ ?> 
	<p class="txtcenter cgrey4"><i class="fa fa-bookmark"></i> Vous êtes dispensé-e de cotisation car vous êtes membre d'honneur.</p>
	<?php }else{  ?>
	<p class="txtcenter cgrey4"><i class="fa fa-bookmark"></i> You are exempt contribution because you are honorary member.</p>
	<?php } ?>
<?php
}
?>


	</div><!-- #blocadhesion -->
</div><!-- #right -->
<div class="clear"></div>
</div><!-- #row -->




























<!-- COTISATIONS -->
<?php
if($token_cotisation!="none" and $token_cotisation!="honneur"){
?>

<h5 class="subheader">Vos cotisations</h5>
          <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th style="width:140px;" class="cgrey4 small">Mode de paiement</th>
                  <th style="width:130px;" class="cgrey4 small">Échéance</th>
                  <th class="cgrey4 small">Montant</th>
                  <th style="width:140px;" class="cgrey4 small">Statut</th>
                  <th class="cgrey4 small">Ref</th>
                  <th>&nbsp;</th>
                  <th>&nbsp;</th>
                </tr>
              </thead>
              <tbody>


<?php

$sql_cotisations = mysql_query("SELECT * FROM ".$prefix."cotisations WHERE id_user = '$id_user_online' ORDER BY date_debut DESC");
while($cotis = mysql_fetch_array($sql_cotisations)) { 
$id_cotisation=$cotis[id];
$montant_cotisation=$cotis[montant];
$paiement_cotisation=$cotis[paiement];
$statut_cotisation=$cotis[statut];
$ref_cotisation=$cotis[ref];
$date_cotis=$cotis[date_debut];
$date_cotisation=date("d M Y", strtotime($date_cotis));
$heure_cotisation=date("H:i", strtotime($date_cotis));



	$depart_cotis=date("YmdHi", strtotime($date_cotis));
	$now_verif = date("YmdHi", time());
	$annee_limite=date("Y", strtotime($date_cotis))+1;
	$fin_cotis=$annee_limite.date("mdHi", strtotime($date_cotis));

	$display_date_limite=date("d M", strtotime($date_cotis))." ".$annee_limite;

	if($now_verif > $depart_cotis and $now_verif < $fin_cotis) { $tk_cotisation="present";}
	if($now_verif < $depart_cotis) { $tk_cotisation="futur";}
	if($now_verif > $fin_cotis) { $tk_cotisation="past";}

?>

<tr style="background:white;">
<td class="cgrey4 small"><?php
if ($paiement_cotisation=="CB") { echo '<i class="fa fa-credit-card"></i> Carte bancaire '; }
if ($paiement_cotisation=="CASH") { echo '<i class="fa fa-money"></i> Esp&egrave;ces'; }
if ($paiement_cotisation=="PAYPAL") { echo '<i class="fa fa-paypal"></i> Paypal'; }
if ($paiement_cotisation=="CHECK") { echo '<i class="fa fa-leaf"></i> Ch&egrave;que'; }
if ($paiement_cotisation=="TRANSFER") { echo '<i class="fa fa-upload"></i> Virement'; }
?></td>
<td class="cgrey4 small"><?php echo "<span class='cgrey4'>".$display_date_limite."</span> - ".$heure_cotisation; ?></td>
<td class="cgrey4 small"><?php echo $montant_cotisation; ?> &euro;</td>
<?php


if ($statut_cotisation=="cashed" and $tk_cotisation=="present") { echo '<td class="cgreen small"><i class="fa fa-check-square"></i> En cours de validité. </td>'; }
if ($statut_cotisation=="cashed" and $tk_cotisation=="futur") { echo '<td class="cgrey4 small"><i class="fa fa-calendar"></i> Active le '.$date_cotisation.'. </td>'; }
if ($statut_cotisation=="cashed" and $tk_cotisation=="past") { echo '<td class="cgrey4 small"><i class="fa fa-archive"></i> Archivée. </td>'; }
if ($statut_cotisation=="waiting") { echo '<td class="cgrey4 small"><i class="fa fa-cogs fa-spin"></i> En traitement. </td>'; }
if ($statut_cotisation=="canceled") { echo '<td class="cgrey2 small"><i class="fa fa-ban"></i> Annul&eacute;e. </td>'; }
?>
<td class="cgrey4 small"><a href="payer.php?id=<?php echo $id_cotisation; ?>" class="grey2link"><?php echo $ref_cotisation; ?></a></td>
<td class="cgrey4 small"><?php 
if($statut_cotisation=="waiting" and ($paiement_cotisation=="CHECK" or $paiement_cotisation=="CASH"))
 {
	if($lang=="fr"){
	echo " <a href='payer.php' class='grey2link' title='Afficher'><i class='fa fa-eye'></i> Afficher</a>";
	}else{ 
	echo " <a href='payer.php' class='grey2link' title='Display'><i class='fa fa-eye'></i> Display</a>";
	} 
 }
 ?></td>
<td class="cgrey3 small"><?php 
if($statut_cotisation=="waiting" and ($paiement_cotisation=="CHECK" or $paiement_cotisation=="CASH"))
 {
	if($lang=="fr"){
	echo " <a href='adhesion.php?del=".$id_cotisation."' class='grey2link' title='Annuler'><i class='fa fa-ban'></i> Annuler</a>";
	}else{ 
	echo " <a href='adhesion.php?del=".$id_cotisation."' class='grey2link' title='Cancel'><i class='fa fa-ban'></i> Cancel</a>";
	}
 }elseif($statut_cotisation=="canceled") {
	if($lang=="fr"){
	echo " <a href='adhesion.php?del=".$id_cotisation."' class='grey2link' title='Supprimer'><i class='fa fa-times-circle'></i> Supprimer</a>";
	}else{ 
	echo " <a href='adhesion.php?del=".$id_cotisation."' class='grey2link' title='Delete'><i class='fa fa-times-circle'></i> Delete</a>";
	} 
 }
 ?></td>
</tr>





<?php
} // END while($cotis = mysql_fetch_array($sql_cotisations)) 
?>

              </tbody>
            </table>
<?php
}//ENDif($token_cotisation!="none")
?>
<!-- # COTISATIONS -->
















<?php
include("inc/template_bottom.php"); // insert bas de page
?>