<?php
$title_page="Statuts";
$private_rule="waiting";
$private="yes"; // accès réservé
include("inc/variables.php");
require("inc/login.php"); // Contrôle d'identité :-/ 
include("inc/template_top.php"); // template
?>









<h4 class="padd100">Statuts</h4>

<hr />
<div class="txtjustify padd100 textesimple"><!-- Statuts txt-justify -->


<h6>RADICAL CINEMA – ARTICLE PREMIER</h6>
<p>Il est fondé entre les adhérents aux présents statuts une association régie par la loi du 1er juillet 1901 et le décret du 16 août 1901, ayant pour titre : <em>Radical Cinema</em>.</p>
<hr />

<h6>ARTICLE 2 - OBJET</h6>
<p>Cette association a pour objet ; la gestion technique et éditoriale du site internet radicalcinema.org, qui a vocation à accueillir des publications de membres de l'association Radical Cinema ou d'auteurs invités. Les publications s'inscrivent dans des registres littéraires, artistiques, philosophiques ou politiques. Elles peuvent être composées de textes, d'images, de films, de vidéos et de sons. En lien avec ses objectifs éditoriaux, l'association a également pour objet la production phonographique, photographique, la production audiovisuelle et la production de spectacles. À terme, l'association pourra également avoir vocation à gérer des espaces artistiques tels que cafés-concerts, bars - restaurants, salles de spectacle, salles de cinéma, studios d'enregistrement, studios de répétitions, plateaux de danse, plateaux de réalisation audiovisuelle, etc.</p>
<hr />

<h6>ARTICLE 3 - SIÈGE SOCIAL</h6>
<p>Le siège social est domicilié à Paris.</p>
<p>Il pourra être transféré par décision du Collège Solidaire.</p>
<hr />

<h6>ARTICLE 4 - DURÉE</h6>
<p>La durée de l’association est illimitée.</p>
<hr />

<h6>ARTICLE 5 - COMPOSITION</h6>
<p>L'association se compose de 6 collèges de membres : </p>
<p>- collège des membres spectateurs</p>
<p>- collège des membres actifs</p>
<p>- collège des membres permanents</p>
<p>- collège des membres bienfaiteurs - collège des membres d'honneur</p>
<p>- collège solidaire.</p>
<hr />

<h6>ARTICLE 6 - ADMISSION</h6>
<p>Pour faire partie de l'association, il faut s'enregistrer (en ligne sur radicalcinema.org ou par courrier en complétant et envoyant le formulaire prévu à cet effet) et s’acquitter de la première cotisation. Toutes les adhésions sont acceptées à priori, mais peuvent être révoquées dans l'année suivant l'inscription, sur décision du Collège des membres permanents. Au delà d'un an (c'est à dire après le versement de la deuxième cotisation annuelle), une adhésion ne peut plus être révoquée et toute radiation doit obéir aux conditions fixées à l'article 8.</p>
<hr />

<h6>ARTICLE 7 - MEMBRES & COTISATIONS</h6>
<p>Le statut de membre spectateur s'obtient par le versement annuel d'une cotisation d'un montant de 15€ (ou 10 € pour les étudiants ou bénéficiaires des minima sociaux, ou 50 € pour une structure représentée par une personne morale).</p>
<p>Le statut de membre actif s'obtient par le versement annuel d'une cotisation d'un montant de 50 € (ou 20 € pour les étudiants ou bénéficiaires des minima sociaux, ou 100 € pour une structure représentée par une personne morale).</p>
<p>Le collège des membres permanents est composé au départ des membres fondateurs de l'association. Annuellement, chaque membre permanents doit être coopté au consensus de l'ensemble des autres membres permanents à l'issue d'une discussion. L'accession d'un nouveau membre au statut de membre permanent doit être proposé par au moins un membre permanent et faire consensus au sein du collège de membres permanents.</p>
<p>Le montant des cotisations annuelles pour les membres permanents est de 50€ (ou 20€ pour les étudiants ou bénéficiaires des minima sociaux, ou 100€ pour une structure représentée par une personne morale).</p>
<p>Le statut de membre bienfaiteur s'obtient par le versement annuel d'une cotisation d'un montant de 200€ pour un membre individuel, ou de 300€ pour une structure représentée par une personne morale.</p>
<p>Le collège des membres d'honneur est composé des personnes ayant rendu des services signalés à l'association, ou de personnalités à qui l'association souhaite rendre hommage. Ils sont dispensés de cotisation. Le statut de membre d'honneur doit être proposé par au moins un membre permanent et faire consensus au sein du collège de membres permanents.</p>
<p>Le collège des membres solidaires réunit l'ensemble des membres permanents et tout autre membre qui en fait la demande et dont celle-ci est validée à l'unanimité par le collège de membres permanents. Renouvelé tous les ans.</p>
<hr />

<h6>ARTICLE 8 – RADIATIONS</h6>
<p>La qualité de membre se perd par : </p>
<p>a) La démission;</p>
<p>b) Le décès;</p>
<p>c) La radiation.</p>
<p>Cette dernière est prononcée par le collège des membres solidaires au consensus. Cette décision peut intervenir pour non payement de la cotisation annuelle et ce après au moins un rappel adressé par mail au membre en question, ou pour motif grave, l'intéressé ayant dans ce cas été invité à s'expliquer devant le collège de membres solidaires.</p>
<hr />

<h6>ARTICLE 9 - AFFILIATION</h6>
<p>La présente association n'est affiliée à aucune fédération. Elle peut adhérer à d’autres associations, unions ou regroupements par consensus au sein du collège de membres permanents, à condition que cela n'affecte pas ses règles de fonctionnement décrites dans les présents statuts.</p>
<hr />

<h6>ARTICLE 10 - RESSOURCES</h6>
<p>Les ressources de l'association se composent :</p>
<p>- des cotisations de ses membres.</p>
<p>- des dons recueillis</p>
<p>- de l'ensemble des recettes engendrées par la diffusion de ses activités. - de la vente de supports multimédias produits ou non par l'association.</p>
<p>- de la perception des droits patrimoniaux liés à ses œuvres.</p>
<p>- de subventions accordées par l'Etat, l'Union européenne, les collectivités publiques ou les sociétés civiles.</p>
<p>- du revenu de ses biens.</p>
<p>- de recettes engendrées par les prestations fournies par l'association.</p>
<p>- de toutes autres ressources autorisées par les lois et règlements en vigueur.</p>
<hr />

<h6>ARTICLE 11 - COLLÈGE SOLIDAIRE</h6>
<p>Le collège solidaire est composé de tous les permanents et de tout autre membre qui en fait la demande et dont celle-ci est validée à l'unanimité par le collège de membres permanents. Le collège solidaire représente l'association dans les actes de la vie civile. En cas de poursuites judiciaires, les membres du collège solidaire en place au moment des faits prendront collectivement et solidairement leurs responsabilités devant les tribunaux compétents.</p>
<p>Les membres du collège solidaire peuvent en démissionner à tout moment en en informant formellement les autres membres du collège. La liste officielle des membres du Collège est actualisée après chaque modification et consultable en ligne par les membres de l'association. Chaque membre du collège solidaire est révocable à tout moment par consensus du collège des membres permanents. Un membre révoqué du collège solidaire ne peut pas être candidat à celui-ci avant qu'un délai de 6 mois se soit écoulé depuis sa révocation.</p>
<p>Le collège solidaire élabore son règlement interne qui est approuvé par l'assemblée permanente de tous les membres.</p>
<p>Au sein de ce collège solidaire sera désigné au consensus, un trésorier, c'est à dire quelqu'un en charge de la gestion financière de l'association et de sa communication auprès de l'assemblée générale. Chaque membre du collège solidaire dispose d'un droit de regard sur l'activité du trésorier et en est juridiquement responsable.</p>
<hr />

<h6>ARTICLE 12 – ASSEMBLEE GENERALE PERMANENTE</h6>
<p>Considérant les distances géographiques qui séparent ses membres, et le besoin structurel de l'association de pouvoir s'organiser rapidement pour accomplir les missions qu'elle s'est fixée,l'association organise ses prises de décisions au moyen d'une assemblée générale permanente en ligne.</p>
<p>Celle-ci se structure autour d'un outil de démocratie participative accessible sur Internet par tous les membres de l'association (à jour de cotisation) à l'adresse suivante ;</p>
<p>http://radicalcinema.org/kitchen/</p>
<p>Cet outil est accessible en permanence, et des décisions y sont prises en continu, toute l'année. L'assemblée générale permanente se divise en deux «groupes».</p>
<p>- l'assemblée des membres solidaires </p>
<p>- l'assemblée de tous les membres</p>
<p>(Note pour le lecteur : l'assemblée des membres solidaires remplace ce qui nommé « conseil d'administration » dans les autres associations, et l'assemblée de tous les membres remplace ce qui est communément nommé par « assemblée générale ordinaire ».)</p>
<p>A) Assemblée permanente des membres solidaires.</p>
<p>Le collège des membres solidaires se réunit tout au long de l'année, de manière asynchrone, sur une page Internet dont l'accès leur est réservé. De là, des discussions peuvent être engagées entre les membres, et des décisions peuvent être prises selon le fonctionnement suivants ;
Lorsqu'un membre veut soumettre une proposition à l'approbation du groupe, il utilise l'outil de démocratie participative pour créer une « fiche » qui doit contenir les informations suivantes :</p>
<p>- titre ; formulé sous la forme d'une question précise.</p>
<p>- texte : contient les détails exacts de la proposition, de façon à permettre aux membres de savoir exactement ce que veut dire être d'accord ou pas avec celle-ci.</p>
<p>- délai : choix du temps accordé aux discussions et prises de décisions. (Une heure, un jour, une semaine ou un mois).</p>
<p>Le membre ayant soumis la proposition ne peut pas voter sur celle-ci, en revanche les autres membres sont invités à se prononcer dans le délai imparti à l'aide d'un système de vote présentant les options suivantes ;</p>
<p>- Tout à fait d'accord</p>
<p>- Moyennement d'accord </p>
<p>- Plutôt défavorable</p>
<p>- Tout à fait défavorable</p>

<p>À la fin du délai imparti le vote est clos.</p>
<p>Si la proposition ne recueille aucun vote «tout à fait défavorable» elle est adoptée.</p>
<p>Si aucun membre n'a voté ; elle est rejetée.</p>
<p>Toute proposition rejetée peut-être reformulée en fonction des remarques faîtes par les membres et présentée à nouveau par son auteur en créant une nouvelle fiche Proposition.</p>
<p>Toutes les décisions prises lors de l'assemblée permanente des membres solidaires sont mises en œuvre par le collège de membres solidaires en fonction des différents consensus qui s'en sont dégagés.</p>
<p>B) Assemblée permanente de tous les membres.</p>
<p>L'assemblée permanente de tous les membres fonctionne avec les mêmes outils que la précédente mais d'un mode de calcul différent.</p>
<p>Tout membre peut ouvrir un nouveau sujet de discussion qui sera accessible à tous les membres. En revanche, seul le collège solidaire peut ouvrir des fiches «Proposition» auxquelles tous les membres peuvent voter.</p>
<p>Cette assemblée est généralement consultative. Le collège des membres solidaires, qui prend usuellement en son sein les décisions concernant le fonctionnement de l'association, peut (au consensus de ses membres) décider de consulter l'assemblée de tous les membres s'agissant de certaines prises de décisions. La fréquence de ces consultations ouvertes sera à l'appréciation des membres du collège solidaire. Cependant, le collège des membres solidaires a la responsabilité d'organiser au mois une fois par an une consultation de ce type dans laquelle tous les membres de l'association sont invités à se prononcer sur les orientations générales de l'association pour l'année qui vient de s'écouler (bilan) et sur celle à venir (prévisions).</p>
<p>Cette consultation annuelle est ouverte aux votes de tous les membres pendant une durée d'un mois. Passé ce délai les votes sont clos. Les autres consultations ouvertes que le collège solidaire est libre de soumettre peuvent avoir des délais différents (une heure, un jour, une semaine) selon l'urgence.</p>
<p>L'outil pour voter dispose ici d'un système de réponse classique "Pour" ou "Contre". Et, contrairement à l'assemblée permanente des membres solidaires, le résultat ne fait pas l'objet d'un consensus, mais procède de la répartition suivante ;</p>
<p>- collège des membres permanents; 50 % </p>
<p>- collège des membres spectateurs; 10 % </p>
<p>- collège des membres actifs; 20 %</p>
<p>- collège des membres bienfaiteurs; 10 % </p>
<p>- collège des membres d'honneur: 10 %</p>
<p>Et selon les répartitions suivantes, si la proposition reçoit une majorité de "Pour" elle est adoptée si non elle est rejetée. En cas d'égalité parfaite la proposition est rejetée.</p>
<hr />

<h6>ARTICLE 13 - DIRECTION ÉDITORIALE</h6>
<p>La direction éditoriale est assurée par l'ensemble des membres du collège solidaire.</p>
<p>Pour être publié, un contenu doit être accepté au consensus des membres du collège solidaire avec le même système de vote que l'assemblée permanente des membres solidaires. (Article 12-A)</p>
<p>Tous les membres de Radical Cinéma disposent d'une interface sur le site pour proposer un contenu. Cependant, tout auteur, membre ou non de l'association, peut envoyer librement ses propositions à la direction éditoriale, par email à l'adresse : redaction@radicalcinema.org
L'ensemble des membres solidaire ont accès à cette boite mail.</p>
<p>Les membres de Radical Cinema peuvent proposer un contenu via une interface dédiée sur le site web et proposer un délai de validation de 24, 48 ou 72 heures.</p>
<p>Cependant les membres du collège solidaire et les membres actifs peuvent également proposer un délai de validation de 3 heures.</p>

<p>Enfin les membres du collège solidaire peuvent également proposer un délai de validation d'une heure. Il est évident que les temps de validations courts (inférieure à 24h) doivent être privilégiés pour les articles ayants une urgence de publication.</p>

<p>La direction éditoriale choisit ce qui sera publié parmi les propositions qu'elle reçoit, et assure la mise en ligne sur radicalcinema.org
Les propositions envoyées par email à la rédaction seront soumises au vote du collège solidaire par l'un des membres du collège solidaire. Si un membre du collège solidaire souhaite publier une proposition reçue par mail il la soumettra au vote du collège des membres solidaires via l'interface dédiée sur le site et dans les même modalités de vote que les autres propositions. Cependant dans ce cas là, le délai de validation de la proposition ne pourra être inférieur à 24 heure. Chaque membre du collège Solidaire peut décider de proposer ou non une proposition reçue. </p>
<p>Si aucun membre solidaire ne propose de mettre au vote une proposition reçue, celle-ci ne sera pas soumise au vote et donc pas publiée.
Chaque membre du collège solidaire peut proposer de retirer un article, en faisant une proposition de suppression d'article à l'assemblée permanente des membres solidaire (article 12-A). Si la proposition est acceptée selon le mode de vote au consensus de cette assemblée, la direction éditoriale s'engage à retirer l'article du site radicalcinema.org dans un délai de 24 heures.</p>
<p>Il est de la responsabilité des membres-auteurs et de la direction éditoriale d'éviter au maximum les retraits de publications.</p>
<p>La direction éditoriale peut demander aux auteurs d'éventuelles adaptations de leurs propositions avant publication, dans le but de coller au plus près à ses choix éditoriaux. Néanmoins, il est demandé à la direction éditoriale de se montrer subtile et délicate dans sa relation aux auteurs ; elle est garante de la réputation de l'association auprès de ceux-ci.</p>
<p>La direction éditoriale présente chaque année à l'assemblée générale de tous les membres un rapport d'activité et expose ses choix de publications dans le but de dégager et communiquer une politique éditoriale.</p>
<p>Le choix de la langue française est privilégié mais aucune autre langue n'est exclue, et l'association s'efforcera - si ses ressources le permettent – de traduire en français les publications initialement publiées dans une autre langue, et de traduire au moins en anglais les publications d'abord écrites en français.</p>
<p>Par article ou contenu, nous entendons toute proposition qui fera l'objet d'une publication sur le site radicalcinema.org</p>
<p>Un article peut-être un texte mais aussi une vidéo, une bande son, une image etc.</p>
<hr />

<h6>ARTICLE 14 - PRODUCTION</h6>
<p>Dans le but d'assurer des publications de qualité, l'association peut financer </p>
<p>– si ses ressources le permettent – des films, enquêtes, reportages, spectacles, et recherches. Les désirs de réalisations (projets) doivent être présentés par leurs auteurs au collège de membres solidaires qui délibère sur leurs conditions possibles de production, et adopte une décision au consensus suivant les modalités de votes de l'assemblée permanente des membres solidaires (article 12-A)</p>
<p>Les réalisations peuvent prendre la forme d'un texte, d'un film, d'un spectacle vivant, d'une production sonore ou d'une série de photos ou d'images etc.</p>
<p>Si un désir de réalisation est accepté, il sera produit ou co-produit par Radical Cinéma. Le ou les auteurs pourront alors être rémunérés ainsi que toute personne travaillant sur la réalisation en question.</p>
<p>Si l'auteur de la réalisation est un membres du collège solidaire, lors de la proposition de son désir au collège solidaire, il ne pourra prendre par au vote, ainsi que tous les membres du collèges solidaire qui travailleront avec lui sur la réalisation proposé. Ceci créera un lien de subordination entre les membres votant et ceux réalisant le projet. Cette démarche assurera la possibilité de rémunération de l'ensemble des personnes travaillant sur un projet même si ceux-ci font parti des membres du collège solidaire et même s'ils sont rémunérés dans le cadre du régime des intermittents du spectacles conformément aux annexes 8 et 10 de l'assurance chômage.</p>
<p>En aucun cas les membres solidaires proposant un projet ne pourront influencer l'acceptation ou non du projet par les membres du collège solidaire votants.</p>
<hr />

<h6>ARTICLE 15 – EXPOSITION PUBLIQUE</h6>
<p>L'association à besoin d'une relation dialectique avec l’extérieur pour oeuvrer. Cette relation se construit, s'entretient, s'élabore. Dans ce but, l'association organisera le plus souvent possible l'exposition publique de ses œuvres.Dans la mesure du possible, tous les membres de l'association y sont conviés.</p>
<hr />

<h6>ARTICLE 16 - INDEMNITES</h6>
<p>Toutes les fonctions du collège solidaire, sont gracieuses et bénévoles. Seuls les frais occasionnés par l’accomplissement de leurs mandats sont remboursés sur justificatifs aux membres du collège solidaire. Le rapport financier présenté à l’assemblée générale ordinaire détaille, par bénéficiaire, les remboursements de frais de mission, de déplacement ou de représentation.</p>
<p>Les contributions des auteurs peuvent être rémunérées, si les ressources de l'association le permettent. Il peut s'agir d'une rémunération faite à un auteur ayant vu l'une de ses propositions acceptée et publiée, hors procédure de publication immédiate (moins de 24h), par la direction éditoriale, ou d'une commande faîte à un auteur par la direction éditoriale. Le montant des rémunérations des auteurs est décidé au cas par cas par le collège des membres solidaires, sauf si le cas est déjà prévu dans la charte de fonctionnement interne.</p>
<hr />

<h6>ARTICLE 17 - PROPRIÉTÉ INTELLECTUELLE</h6>
<p>Les publications non-rémunérées appartiennent à leurs auteurs. En accord avec sa volonté de rendre le savoir, l'art et la culture le plus largement et le plus facilement accessible à tous, l'association encourage le choix par les auteurs d'une licence libre. Dans le cas où ceux-ci préféreraient retenir leurs publications dans le régime classique de droits d'auteurs, un contrat de cession de droits est proposé à l'auteur dans le but de permettre à l'association de publier et diffuser le contenu proposé par l'auteur. Sauf décision exceptionnelle, toutes les publications pour lesquelles les auteurs sont rémunérés sont publiées sous licence libre, en accord avec les auteurs.</p>
<p>Tout le reste du site radicalcinema.org (graphisme + textes propres à l'association) sont mis à disposition sous licence Art-Libre. Un exemplaire de cette licence est disponible sur le site artlibre.org.</p>
<hr />

<h6>ARTICLE 18 – CHARTE DE FONCTIONNEMENT INTERNE</h6>
<p>Une charte de fonctionnement interne devra être rédigée le plus vite possible par le collège des membres solidaire en fonction des orientations dégagées au consensus de l'assemblée générale permanente. Le collège de membres solidaires se donne un délai maximum de deux ans à compter de la fondation de l'association pour avoir rédigé cette charte.</p>
<p>Cette charte est destinée à fixer les divers points non prévus par les présents statuts, notamment ceux qui ont trait à l'administration interne de l'association, au fonctionnement horizontal des discussions, etc... Cette charte de fonctionnement peut-être modifiée à tout moment si nécessaire, par décision au consensus des membres du collège solidaire.</p>
<hr />

<h6>ARTICLE 19 - DISSOLUTION</h6>
<p>La dissolution ne peut être prononcée qu'en assemblée générale ordinaire, si le point est inscrit à l'ordre du jour. Dans ce cas, un ou plusieurs liquidateurs sont nommés par le collège des membres permanents, et l'actif, s'il y a lieu, est dévolu conformément aux décisions de l’assemblée générale qui statue sur la dissolution.</p>
<hr />

<h6>ARTICLE 20 - LIBERALITES</h6>
<p>Le rapport et les comptes annuels, tels que définis à l’article 11 (y compris ceux des comités locaux) sont adressés chaque année au Préfet du département.</p>
<p>L’association s’engage à présenter ses registres et pièces de comptabilité sur toute réquisition des autorités administratives en ce qui concerne l’emploi des libéralités qu’elle serait autorisée à recevoir, à laisser visiter ses établissements par les représentants de ces autorités compétents et à leur rendre compte du fonctionnement desdits établissements.</p>

<p style="text-align:center;">« Première version des statuts finalisé à Paris, le 30 novembre 2014 après échanges de vues à Saint- Malo et Mazabrard. Puis modification de Statuts, articles 13, 14, 16 et 18 par décision du Collège Solidaire le 18 mars 2015 »</p>

<p style="text-align:center;">Logan de Carvalho - Gabriel Lechevalier - Joseph Paris</p>







<!-- #Statuts txt-justify --></div>


<?php
/*
$sql = mysql_query("SELECT * FROM `".$prefix."users` WHERE `solidaire` = 'oui' AND `statut` != 'waiting' AND `statut` != 'suspended' ");
while($r = mysql_fetch_array($sql)) {
$email_dest = $r['email'];
$lang_dest = $r['lang'];
echo "<p>".$email_dest." - ".$lang_dest."</p>";
} //fin while users
*/
?>


<?php
include("inc/template_bottom.php"); // insert bas de page
?>