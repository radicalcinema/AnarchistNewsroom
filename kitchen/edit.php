<?php
include("inc/variables.php");
require("inc/login.php"); // Contrôle d'identité :-/ 
$private="yes"; // accès réservé

$id=$_GET['id'];
$type=$_GET['type'];
$action=$_GET['action'];

if($id==""){ $errors[] = "id is missing"; }
if($type==""){ $errors[] = "type is missing"; }
if($action==""){ $errors[] = "action is missing"; }

if($_GET['action']!="edit" and $_GET['action']!="delete" and $_GET['action']!="lock" and $_GET['action']!="unlock" and $_GET['action']!="archive" and $_GET['action']!="unarchive" and $_GET['action']!="move")
	{ 
		$errors[] = "error";
	}

$id_auteur="";
$id_college="";
$num_rows=0;
// RECUP INFOS
if($type=="thread")
{
//on recupere les infos du thread
$sql = mysql_query("SELECT * FROM ".$prefix."threads WHERE id = '".$id."'");
$num_rows = mysql_num_rows($sql);
if($num_rows==0) { $errors[] = "Does not exist"; }
while($r = mysql_fetch_array($sql)) { $title=$r[title]; $id_auteur=$r[id_user]; $id_college=$r[id_college]; $statut_thread=$r[statut];$message=$r[message]; $vote=$r[vote]; $replies_thread=$r[replies]; }
}
elseif($type=="reply")
{
//on recupere les infos du reply
$sql = mysql_query("SELECT * FROM ".$prefix."replies WHERE id = '".$id."'");
$num_rows = mysql_num_rows($sql);
if($num_rows==0) { $errors[] = "Does not exist"; }
while($r = mysql_fetch_array($sql)) { $id_auteur=$r[id_user]; $id_thread=$r[id_thread]; $id_article=$r[id_article]; $message=$r[message]; }



if($id_thread!=0) {  //on recupere les infos du thread
$sql = mysql_query("SELECT * FROM ".$prefix."threads WHERE id = '".$id_thread."'");
while($r = mysql_fetch_array($sql)) { $id_college=$r[id_college]; $statut_thread=$r[statut]; $vote=$r[vote]; $replies_thread=$r[replies]; $posted_thread=$r[posted]; }
}//fin du thread
if($id_article!=0) {  //on recupere les infos de l'article
$sql = mysql_query("SELECT * FROM ".$prefix."articles WHERE id = '".$id_article."'");
while($r = mysql_fetch_array($sql)) { $token=$r['token'];  }
}//fin infos de l'article


}
	//on recupere les infos de la section
	$sql_section = mysql_query("SELECT * FROM ".$prefix."colleges WHERE id = '".$id_college."'");
	while($section = mysql_fetch_array($sql_section)) 
	{
	$title_college=$section[titre];
	}// fin while



//FORMAT DATES
	$time = time();
	$maintenant = date("Y-m-d H:i:s", $time);



if($id_thread!=0) {if($vote=="oui") { $url="vote.php?id=$id_thread"; } else { $url="topic.php?id=$id_thread"; } }
if($id_article!=0) { $url="../article.php?tk=$token"; }








switch ($action) {
    case "edit":
        $title_page="Editer";
		// VERIF DROITS SECTION + THREAD
		if($statut_thread=="locked")
		{
		$errors[] = "<i class=\"fa fa-lock\"></i> This topic is locked.";
		}
		if($statut_thread=="archived")
		{
		$errors[] = "<i class=\"fa fa-archive\"></i> This topic is archived.";
		}
		// VERIF DROITS USER
		if($id_auteur!=$id_user_online and $statut_user_online!="admin")
		{
		$errors[] = "<i class=\"fa fa-lock\"></i> You must the author or an admin.";
		}
		if(isset($_POST['postage']) and count($errors) == 0) {//si formulaire validé, on UPDATE les données
		
		
		
		
			$title=$_POST['title']; // balises html
			$message=$_POST['message'];
			
			//formatage des dates
			$time = time();
			$maintenant = date("Y-m-d H:i:s", $time);
			$delai=$_POST['delai'];
		if($delai == '1'){ $deadline = date("Y-m-d H:i:s", strtotime('now +1 Hour')); }
		elseif($delai == '2'){ $deadline = date("Y-m-d H:i:s", strtotime('now +2 Hours')); }
		elseif($delai == '24'){ $deadline = date("Y-m-d H:i:s", strtotime('now +24 Hours')); }
		elseif($delai == '48'){ $deadline = date("Y-m-d H:i:s", strtotime('now +48 Hours')); }
		elseif($delai == '72'){ $deadline = date("Y-m-d H:i:s", strtotime('now +72 Hours')); }
		else{ $deadline = date("Y-m-d H:i:s", strtotime('now +168 Hours')); }
		
		
			
			if($type=="thread")
			{
			//on update le thread
			mysql_query("UPDATE ".$prefix."threads SET title = '$_POST[title]' WHERE id = '$id'");
			mysql_query("UPDATE ".$prefix."threads SET message = '$message' WHERE id = '$id'");
				if($vote=="oui") { 
				// Si c'est un vote on update la deadline
				mysql_query("UPDATE ".$prefix."threads SET deadline = '$deadline' WHERE id = '$id'");
				// et on supprime les votes liés
				mysql_query("DELETE FROM ".$prefix."votes WHERE id_thread = '$id'");
				}
		
			if($vote=="oui"){ header('Location: vote.php?id='.$id); }
			else{ header('Location: topic.php?id='.$id); }
			}//END UPDATE THREAD


			if($type=="reply")
			{
			//on update le thread
			mysql_query("UPDATE ".$prefix."replies SET message = '$message' WHERE id = '$id'");
			if($vote=="oui" and $id_thread!="0"){ header('Location: vote.php?id='.$id_thread.'#'.$id); }
			elseif($vote!="oui" and $id_thread!="0"){ header('Location: topic.php?id='.$id_thread.'#'.$id); }
			elseif($id_article!="0" and $token!=""){ header('Location: ../article.php?tk='.$token.'#'.$id); }
			}//END UPDATE REPLY


		
		
		
		
		}// FIN POSTAGE
        break;
    case "delete":
        $title_page="Supprimer";
		// VERIF DROITS SECTION + THREAD
		if($statut_thread=="locked")
		{
		$errors[] = "<i class=\"fa fa-lock\"></i> This topic is locked.";
		}
		if($statut_thread=="archived")
		{
		$errors[] = "<i class=\"fa fa-archive\"></i> This topic is archived.";
		}
		// VERIF DROITS USER
		if($id_auteur!=$id_user_online and $statut_user_online!="admin")
		{
		$errors[] = "<i class=\"fa fa-lock\"></i> You must the author or an admin.";
		}
		if($_POST['deleteconfirm']=="Supprimer" and count($errors) == 0) {//si formulaire validé, on supprime 
		
			if($type=="thread")
			{ // on supprime le thread
			mysql_query("DELETE FROM ".$prefix."threads WHERE id = '$id'");
				if($vote=="oui")
				{// on supprime les votes
				mysql_query("DELETE FROM ".$prefix."votes WHERE id_thread = '$id'");
				}
				if($statut_user_online=="admin")
				{// on supprime les réponses aussi
				mysql_query("DELETE FROM ".$prefix."replies WHERE id_thread = '$id'");
				}
			if($vote=="oui"){ header('Location: vote.php?id='.$id); }
			else{ header('Location: topic.php?id='.$id); }
			}
			elseif($type=="reply")
			{ // on supprime le reply
			mysql_query("DELETE FROM ".$prefix."replies WHERE id = '$id'");
			// on update le thread associé
			mysql_query("UPDATE ".$prefix."threads SET replies = replies-1 WHERE id = '$id_thread'"); $replies_thread=$replies_thread-1;
				//on update la date de la derniere réponse.
				if($replies_thread>1){
					$sql_r = mysql_query("SELECT * FROM ".$prefix."replies WHERE id_thread = '".$id_thread."'");
					while($ra = mysql_fetch_array($sql_r)) { $last_posted=$ra[posted];  }		
					mysql_query("UPDATE ".$prefix."threads SET last_reply = '$last_posted' WHERE id = '$id_thread'");
				}elseif($replies_thread<=1){		
					mysql_query("UPDATE ".$prefix."threads SET last_reply = '$posted_thread' WHERE id = '$id_thread'");
				}
			if($vote=="oui" and $id_thread!=""){ header('Location: vote.php?id='.$id); }
			elseif($vote!="oui" and $id_thread!=""){ header('Location: topic.php?id='.$id); }
			elseif($id_article!="" and $token!=""){ header('Location: ../article.php?tk='.$token); }
			}
		
		
		if($type=="reply" and $vote=="yes" and $id_thread!="") { header('Location: vote.php?id='.$id_thread); }
		if($type=="reply" and $vote!="yes" and $id_thread!="") { header('Location: topic.php?id='.$id_thread); }
		if($type=="reply" and $token!="" and $id_article!="") { header('Location: ../article.php?tk='.$token); }
		if($type=="thread") { header('Location: index.php'); }
		}
        break;
    case "lock":
        $title_page="Verrouiller";
		if($statut_user_online!="admin") { $errors[] = "<i class=\"fa fa-lock\"></i> You must be an admin."; }
		if($_POST['blockconfirm']=="Verrouiller" and count($errors) == 0) {//si formulaire validé, on supprime 
		
			if($type=="thread" and $statut_user_online=="admin")
			{ // on update le thread
			mysql_query("UPDATE ".$prefix."threads SET statut = 'locked' WHERE id = '$id'");
			if($vote=="oui"){ header('Location: vote.php?id='.$id_thread.'#'.$id_reply); }
			else{ header('Location: topic.php?id='.$id_thread.'#'.$id_reply); }
			}
		
		}
        break;
    case "unlock":
        $title_page="Deverrouiller";
		if($statut_user_online!="admin") { $errors[] = "<i class=\"fa fa-lock\"></i> You must be an admin."; }
		if(count($errors) == 0){ 
			if($type=="thread" and $statut_user_online=="admin")
			{ // on update le thread
			mysql_query("UPDATE ".$prefix."threads SET statut = 'published' WHERE id = '$id'");
			if($vote=="oui"){ header('Location: vote.php?id='.$id_thread); }
			else{ header('Location: topic.php?id='.$id_thread); }
			}
		
		} //fin pas d'erreurs
        break;
    case "archive":
        $title_page="Archiver";
		if($statut_user_online!="admin") { $errors[] = "<i class=\"fa fa-lock\"></i> You must be an admin."; }
		if($_POST['blockconfirm']=="Archiver" and count($errors) == 0) {//si formulaire validé, on supprime 
			if($type=="thread" and $statut_user_online=="admin")
			{ // on update le thread
			mysql_query("UPDATE ".$prefix."threads SET statut = 'archived' WHERE id = '$id'");
			if($vote=="oui"){ header('Location: vote.php?id='.$id_thread); }
			else{ header('Location: topic.php?id='.$id_thread); }
			}
		}//fin blockconfirm
        break;
    case "unarchive":
		if($statut_user_online!="admin") { $errors[] = "<i class=\"fa fa-lock\"></i> You must be an admin."; }
		if($type=="thread" and $statut_user_online=="admin" and count($errors) == 0)
		{ // on update le thread
		mysql_query("UPDATE ".$prefix."threads SET statut = 'published' WHERE id = '$id'");
		}
        break;
    case "move":
        $title_page="Déplacer";
		 if($statut_user_online!="admin")
		{
		$errors[] = "<i class=\"fa fa-lock\"></i> You must be an admin.";
		}
		if($_POST['blockconfirm']=="Deplacer") {//si formulaire validé, on deplace 
		$id_col_move=$_POST['id_col_move'];
			if($type=="thread" and $statut_user_online=="admin" and $id_col_move!="")
			{ // on update le thread
			mysql_query("UPDATE ".$prefix."threads SET id_college = '$id_col_move' WHERE id = '$id'");
			// on update les réponses 
			$sqlreply = mysql_query("SELECT * FROM ".$prefix."replies WHERE id_thread = '".$id."' ORDER by id");
			while($rrep = mysql_fetch_array($sqlreply)) {
			$id_reply = $rrep[id];
		//	echo "<p>id_reply: ".$id_reply."</p>";
			mysql_query("UPDATE ".$prefix."replies SET privacy = '$id_col_move' WHERE id = '$id_reply'");
			 }
			}
			if($vote=="oui"){ header('Location: vote.php?id='.$id_thread); }
			else{ header('Location: topic.php?id='.$id_thread); }

		}// fin $_POST blockconfirm
        break;
}
?>



























<?php
include("inc/template_top.php"); // insert haut de page
?>


	<div class="row">
    
        <div class="timeline-centered">


<!-- PREMIER SUJET -->
        <article class="timeline-entry">

            <div class="timeline-entry-inner">

                <div class="timeline-icon bg-thread">
                    <i class="fa fa-comment"></i>
                </div>

                <div class="timeline-label">



<?php


switch ($action) {
    case "edit": /*--------------------- EDITER --------------------*/

	



//on affiche le formulaire de modification
?>

	<?php
	if($type=="thread" and $vote=="oui")
	{
	?>
		<p class="" style="text-align:center!important;">Attention ; Si l'énoncé du vote est modifié, le scrutin recommence à zéro.
		<br />C'est à dire que si vous modifiez le contenu ci-dessous, tous les votes liés qui ont déjà été enregistrés seront supprimés.
		</p>
	<?php
	}
	?>


<form class="form-horizontal" role="form" action="edit.php?id=<?php echo $id; ?>&type=<?php echo $type; ?>&action=edit" method="post">
  <div class="form-group">
    <label class="col-sm-1 control-label">De</label>
    <div class="col-sm-9">
      <p class="form-control-static"><?php echo $prenom_user_online." ".$nom_user_online; ?></p>
    </div>
  </div>
	<?php
	if($type=="thread")
	{
	?>
  <div class="form-group">
    <label for="nom" class="col-sm-1 control-label">Titre</label>
    <div class="col-sm-9">
      <input type="text" class="form-control" id="title" name="title" value="<?php echo $title; ?>" maxlength="70" placeholder="70 caractères max" required>
    </div>
  </div>
	<?php
	}
	?>
  <div class="form-group">
    <label for="nom" class="col-sm-1 control-label">Texte</label>
    <div class="col-sm-9">


  <script>
tinymce.init({
  selector: 'div#message',
  inline: false,
  plugins: [
    'link',
    'code'
  ],
  toolbar: 'bold italic link',
  menubar: false
});
  </script>
                    <div id="message" class="textesimple"><?php echo $message; ?></div>

    </div>
  </div>
	<?php
	if($type=="thread" and $vote=="oui")
	{
	?>
  <div class="form-group">
    <label for="nom" class="col-sm-1 control-label">Delai</label>
    <div class="col-sm-9">
		<div class="radio">
		  <label>
		    <input type="radio" name="delai" id="delai2" value="2">
		    <span class=""> 2 heures</span>
		  </label>
		</div>
		<div class="radio">
		  <label>
		    <input type="radio" name="delai" id="delai3" value="24">
		    <span class=""> 24 heures</span>
		  </label>
		</div>
		<div class="radio">
		  <label>
		    <input type="radio" name="delai" id="delai4" value="48">
		    <span class=""> 2 jours</span>
		  </label>
		</div>
		<div class="radio">
		  <label>
		    <input type="radio" name="delai" id="delai5" value="72">
		    <span class=""> 3 jours</span>
		  </label>
		</div>  
		<div class="radio">
		  <label>
		    <input type="radio" name="delai" id="delai6" value="168">
		    <span class=""> Une semaine</span>
		  </label>
		</div>  
    </div>
  </div>
	<?php
	}
	?>


  <hr>
  <input type="hidden" name="postage" value="yes">
  <p class="txtcenter"><input type="submit" class="btn-article-small" value="Modifier" /></p>
</form>
<?php



        break;/*--------------------- FIN EDITER --------------------*/
    case "delete":/*--------------------- SUPPRIMER --------------------*/
//on on affiche le formulaire
?>
<p class="txtcenter">Voulez-vous vraiment supprimer ce message?</p>
<form method="post" action="edit.php?id=<?php echo $id; ?>&type=<?php echo $type; ?>&action=delete">
  <p class="txtcenter"><input type="submit" class="btn-delete-small" name="deleteconfirm" value="Supprimer" />
<?php if($type=="thread"){ ?>
  <a href="<?php echo $url; ?>?id=<?php echo $id; ?>" class="btn-article-small">Annuler</a></p>
<?php }elseif($type=="reply"){ ?>
  <a href="<?php echo $url; ?>?id=<?php echo $id_thread; ?>#<?php echo $id; ?>" class="btn-article-small">Annuler</a></p>
<?php } ?>

</form>



<?php









        break;/*--------------------- FIN SUPPRIMER --------------------*/
    case "lock":/*--------------------- LOCK --------------------*/






?>
<div class="container"><!-- container -->
<p class="txtcenter">Voulez-vous vraiment verrouiller ce message?</p>
<p class="txtcenter cgrey4">Le sujet sera visible uniquement par le collège solidaire, il ne sera plus possible de répondre.</p>
<form method="post" action="democracy-edit.php?id=<?php echo $id; ?>&type=<?php echo $type; ?>&action=lock">
  <p class="txtcenter"><input type="submit" class="btn btn-sm btn-danger" name="blockconfirm" value="Verrouiller" />
  <a href="<?php echo $url; ?>?id=<?php echo $id; ?>" class="btn btn-sm btn-default">Annuler</a></p>
</form>
</div> <!-- /container -->
<?php


        
        break;/*--------------------- FIN LOCK --------------------*/
    case "unlock":/*--------------------- UNLOCK --------------------*/


        
        break;/*--------------------- FIN UNLOCK --------------------*/





    case "archive":/*--------------------- ARCHIVE --------------------*/





        
        break;/*--------------------- FIN ARCHIVE --------------------*/
    case "unarchive":/*--------------------- UNARCHIVE --------------------*/




	?>


<?php   
        break;/*--------------------- FIN UNARCHIVE --------------------*/



    case "move":/*--------------------- MOVE --------------------*/

?>
<div class="petit_container_form"><!-- petit_container_form -->

<p class="txtleft" style="padding:20px 0px 10px 0px;"><i class="fa fa-arrows-alt"></i> <b>Où voulez-vous déplacer ce message ?</b></p>
<form method="post" action="edit.php?id=<?php echo $id; ?>&type=<?php echo $type; ?>&action=move" class="form-horizontal">

<?php
if(college_thread($id)==0){ 
?>
<div class="radio">
  <label>
    <input type="radio" name="id_col_move" id="id_col_move0" value="0" checked required>
     Assemblée de tous les membres
  </label>
</div>
<?php
	}else{ 
?>
<div class="radio">
  <label>
    <input type="radio" name="id_col_move" id="id_col_move0" value="0" required>
     Assemblée de tous les membres
  </label>
</div>
<?php
	} 
?>




<?php
//on recupere les colleges
$sql = mysql_query("SELECT * FROM ".$prefix."colleges ORDER BY id");
while($r = mysql_fetch_array($sql)) 
	{ 
	$id_col=$r[id];
	$titre_col=$r[titre];
	if($id_col==college_thread($id)){ $tchecked="yes";} else{ $tchecked="no";}
?>

  
<div class="radio">
  <label>
    <input type="radio" name="id_col_move" id="id_col_move<?php echo $id_col; ?>" value="<?php echo $id_col; ?>" required<?php if($tchecked=="yes") { echo " checked"; } ?>>
     <?php echo $titre_col; ?> 
  </label>
</div>
<?php
	}
?>

  <p class="txtleft" style="padding:20px 0px;"><input type="submit" class="btn btn-sm btn-danger" name="blockconfirm" value="Deplacer" />
  <a href="<?php echo $url; ?>?id=<?php echo $id; ?>" class="btn btn-sm btn-default">Annuler</a></p>
</form>
</div><!-- #petit_container_form -->

<?php
        break;/*--------------------- FIN MOVE --------------------*/


}//fin switch













?>

                </div>
            </div>

        </article>
<!-- #PREMIER SUJET -->




        <article class="timeline-entry begin">

            <div class="timeline-entry-inner">

                <div class="timeline-icon  bg-black" style="-webkit-transform: rotate(-90deg); -moz-transform: rotate(-90deg);">
                    <i class="entypo-flight"></i> +
                </div>

            </div>

        </article>

    </div>

    
	</div><!-- .row -->







<script type="text/javascript">
    $(document).ready(function() {
        $('html, body').hide();

        if (window.location.hash) {
            setTimeout(function() {
                $('html, body').scrollTop(0).show();
                $('html, body').animate({
                    scrollTop: $(window.location.hash).offset().top
                    }, 1000)
            }, 0);
        }
        else {
            $('html, body').show();
        }
    });
</script>





<?php
include("inc/template_bottom.php"); // insert bas de page
?>