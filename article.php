<?php
include("kitchen/inc/variables.php"); // ouverture des variables obligatoires
require("kitchen/inc/login.php");
if($_GET['tk']=="" and $_POST['submitform']==""){ $errors[] = "token missing"; }
if($lang=="fr"){ 
$toutafaitdaccord="Tout à fait favorable";
$daccord="Favorable";
$plutotdefavorable="Plutôt défavorable";
$absolumentdefavorable="Absolument défavorable";
}else{ 
$toutafaitdaccord="Totally favorable";
$daccord="Favorable";
$plutotdefavorable="Rather unfavorable";
$absolumentdefavorable="Absolutely unfavorable";
}

$title=$_POST['titre']; $title=remplaceGuillemets($title); $title=strip_tags($title); $title = str_replace(":", ";", $title);
$description=$_POST['description']; $description=remplaceGuillemets($description); $description=strip_tags($description); 
$hashtag=$_POST['hashtag']; $hashtag=strip_tags($hashtag);   $hashtag = preg_replace("#[^a-zA-Z]#", "", $hashtag);
$texte=$_POST['texte'];  
$vimeo1=$_POST['vimeo1'];
$vimeo2=$_POST['vimeo2'];
$youtube1=$_POST['youtube1'];
$youtube2=$_POST['youtube2'];
if($youtube1=="" and $youtube2!=""){ $youtube1=$youtube2; $youtube2=""; }
if($vimeo1=="" and $vimeo2!=""){ $vimeo1=$vimeo2; $vimeo2=""; }
$edition=$_POST['edition']; if($_POST['edition']=="fr"){ $lang="fr"; }else{ $lang="en"; }
$soundcloud=$_POST['soundcloud_code'];
$token=$_GET['tk'];

$strip_title=strip_tags($title);
$strip_text=strip_tags($texte);


	if($token=="" and $_POST['submitform']!=""){ // Nouvel article
	if($hashtag=="Hashtagoptionnel" or $hashtag=="Hashtagoptional"){ 
		$hashtag="";
	 }
	if($description=="Sous-titre" or $description=="Subtitle"){ 
		$description="";
	 }
	if($title=="" or $strip_title=="Commencez ici en indiquant un titre" or $strip_title=="Start with a title"){ 
		if($lang=="fr"){
		$errors[] = "Il faut quand même indiquer un titre, c'est la moindre des choses.";
		}else{ 
		$errors[] = "You still have to specify a title, it's the least of it.";
		}
	 }
	if(($texte=="" or $strip_text=="Puis posez votre texte ici." or $strip_text=="Then write your text here.") and count($errors) == 0){ 
		if($lang=="fr"){
		$errors[] = "Vous n'avez rien à dire.";
		}else{ 
		$errors[] = "You have nothing to say.";
		}	
	
	 }
	$length_text=strlen($texte);
	if($length_text <= 140 and $texte!="" and $strip_text!="Puis posez votre texte ici." and $strip_text!="Then write your text here."){  
		if($lang=="fr"){
		$errors[] = "Si tout ce que vous avez à dire n'excède pas 140 caractères, postez-le plutôt sur Twitter.";
		}else{ 
		$errors[] = "If all you have to say does not exceed 140 characters, post it on Twitter instead.";
		}
	}
		if(count($errors) == 0){ //si pas d'erreurs
		$date_save = date("Y-m-d H:i:s", strtotime('now'));
		$deadline_proposal = date("Y-m-d H:i:s", strtotime('now +12 Hours'));
		$length = 70;
		$newtoken = createatoken($length);
		//on insert
		$request_insert="INSERT INTO `".$prefix."articles` (`token`, `token_user`, `edition`, `tag`, `titre`, `description`, `texte`, `statut`, `date_save`, `deadline_proposal`, `date_published`, `license`, `soundcloud`, `vimeo1`, `vimeo2`, `youtube1`, `youtube2`, `identity`) VALUES('".$newtoken."', '".$token_user_online."', '".$edition."', '".$hashtag."', '".$title."', '".$description."', '".$texte."', 'proposed', '$date_save', '$deadline_proposal', '0000-00-00 00:00:00', 'by', '$soundcloud', '$vimeo1', '$vimeo2', '$youtube1', '$youtube2', '');";
		
		if (mysql_query($request_insert)or die(mysql_error())) {
		  header('Location: article.php?tk='.$newtoken);
		} else {
		  echo 'Erreur..';
		  exit;
		} 


		
		} // fin pas d'erreur
	} // fin nouvel article



if($token!=""){ // Article bdd
$sql_article = mysql_query("SELECT * FROM ".$prefix."articles WHERE token = '".$token."'");
if (mysql_num_rows($sql_article)==0) { $errors[] = "Wrong token."; }
else{  while($article = mysql_fetch_array($sql_article)) {
$id_article = $article['id'];
$id_user_article = $article['id_user'];
$token_user_article = $article['token_user'];
$title = $article['titre']; $title=stripcslashes($title); $title=strip_tags($title);
$statut = $article['statut'];
$thumb = $article['thumb'];
$license = $article['thumb_license'];
$photographe = $article['thumb_photographer'];
$legende = $article['thumb_legende'];
$description = $article['description']; $description=stripcslashes($description); $description=strip_tags($description);
$texte = $article['texte']; 
$hashtag=$article['tag'];
$vimeo1=$article['vimeo1'];
$vimeo2=$article['vimeo2'];
$vimeo3=$article['vimeo3'];
$vimeo4=$article['vimeo4'];
$youtube1=$article['youtube1'];
$youtube2=$article['youtube2'];
$youtube3=$article['youtube3'];
$youtube4=$article['youtube4'];
$edition=$article['edition'];
if($edition!="") {
$slug=$article['slug'];
$index_slug=$article['index_slug'];
if($edition=="fr") { $lang_dir="1.fr"; }
if($edition=="en") { $lang_dir="2.en"; }
}
$soundcloud=$article['soundcloud'];
if($statut=="published" or $statut=="unpublished") { $ladate = date("c",strtotime($article['date_published'])); }
else{ $ladate = date("c",strtotime($article['deadline_proposal'])); }
$deadline_proposal=date("c",strtotime($article['deadline_proposal']));
$deadline_fin_display = date("d M Y à  H:i", strtotime($article['deadline_proposal']));
$deadline_proposal_cd = date("Y/m/d H:i:s", strtotime($article['deadline_proposal']));
$signature = $article['signature'];
if($signature=="Anonymous"){ if($lang=="fr"){ $signature="Anonyme"; }else{  $signature="Anonymous"; } }
$twauteur = $article['twauteur'];
$identity=$article['identity'];
} }

if($edition=="fr"){ $lang="fr"; }else{ $lang="en"; }

if($identity=="" and $id_user_article==0 and count($errors) == 0){ header('Location: article_identity.php?tk='.$token); }

if(isset($_POST['repondre'])){
include("kitchen/inc/inc_post_reply.php");
}// fin isset repondre







//thumb size
$min_image_width 		= 1280; //Maximum image size (height and width)
$min_image_height 		= 720; //Maximum image size (height and width)
 if($thumb!="" and file_exists($thumb)){ 
	$image_size_info 	= getimagesize($thumb); //get image size
	
	if($image_size_info){
		$image_width 		= $image_size_info[0]; //image width
		$image_height 		= $image_size_info[1]; //image height
		$image_type 		= $image_size_info['mime']; //image type
	}	 
 }
 
 
 
 	 
 //#thumb

$resultatvote=result_vote_article($id_article);
$statut_vote=statut_vote_article($id_article);
$everyone = everyone_voted($id_article);

include("kitchen/inc/inc_article_updatestatut.php");

$result_votes = mysql_query("SELECT * FROM ".$prefix."votes WHERE id_article = '".$id_article."'");
$num_votes = mysql_num_rows($result_votes);
if ($num_votes!=0){ $someone_voted="yes";  }else{ $someone_voted="no";  }
// ########################################################## VOTE #########################################################
$id_auteur_question=$user_article;
if(isset($_POST['voter'])){ $secure_vote=md5("banded morpho"); include("kitchen/inc/vote_article.php"); }
include("kitchen/inc/getvotes.php"); // Get votes
} // fin article bdd
// ########################################################## AFFICHAGE #########################################################
$title_page=$title;
include("kitchen/inc/template_top.php");
?>							









<?php if((($statut=="accepted" or $statut=="published") and $logged!="yes") or ($logged!="yes" and $token_user_article==$token_user_online)){ ?>
<article style="padding:0px;border:0;">
<h4 style=""><?php echo $title; ?></h4>
<span class="divblack2"></span>
</article>
<?php  } ?>

<?php if($logged!="yes" and $token_user_article!=$token_user_online and $statut!="accepted" and $statut!="published"){ ?>
<article style="padding:0px;border:0;">
<h4 style=""><?php echo $token; ?></h4>
<span class="divblack2"></span>
</article>
<?php  } ?>





<div class="row no-gutters"><!-- row -->
<div class="col-md-1 hidden-xs">&nbsp;</div>
    <div class="col-md-3 col-sm-12 col-xs-12"><!-- col avis -->

	<p class="txtcenter"><canvas id="avis" height="150" width="150" style="margin:0px auto;padding:0px;"></canvas></p>
	<script>
	var percent;
	percentyesyes2 = <?php echo $percent_yesyes2; ?>;
	percentyes2 = <?php echo $percent_yes2; ?>;
	percentno2 = <?php echo $percent_no2; ?>;
	percentnono2 = <?php echo $percent_nono2; ?>;
	percentDiff2 = 100-percentyesyes2-percentyes2-percentno2-percentnono2;
		var doughnutData2 = [
				{
			        value: percentnono2,
			        color:"#fd0080", 
			        label: "Pas du tout d'accord"
			    },
			    {
			        value: percentno2,
			        color: "#f1c40f",
			        label: "Moyennement d'accord"
			    },
			    {
			        value: percentyes2,
			        color: "#3498db",
			        label: "Plutôt d'accord"
			    },
			    {
			        value: percentyesyes2,
			        color: "#1abc9c",
			        label: "Tout à fait d'accord"
			    },
			    {
			        value: percentDiff2,
			        color: "#ccc",
			        label: "vide"
			    }
			];
			var myDoughnut2 = new Chart(document.getElementById("avis").getContext("2d")).Doughnut(doughnutData2,{segmentShowStroke : false, percentageInnerCutout : 60, animationSteps : 100, animationEasing : "easeOutBounce", animateRotate : true, animateScale : true});
    
   	</script>

		<div class="legendgraph" style="margin:10px 0px 0px 0px;padding:0px;">
<?php
if($lang=="fr"){ 
echo "<p><b>Avis des membres</b></p>";
}else{ 
echo "<p><b>Advisory</b></p>";
}
?>		
		<div class="item yesyes">
		<?php echo $toutafaitdaccord; ?> <span class="cgrey3">-</span> <b id="yesyes2"><?php echo $percent_yesyes2; ?></b>%
		</div>
		<div class="item yes">
		<?php echo $daccord; ?> <span class="cgrey3">-</span> <b id="yes2"><?php echo $percent_yes2; ?></b>%
		</div>
		<div class="item no">
		<?php echo $plutotdefavorable; ?> <span class="cgrey3">-</span> <b id="no2"><?php echo $percent_no2; ?></b>%
		</div>
		<div class="item nono">
		<?php echo $absolumentdefavorable; ?> <span class="cgrey3">-</span> <b id="nono2"><?php echo $percent_nono2; ?></b>%
		</div>
<?php
if($lang=="fr"){ 
echo '<p class="details">Les avis sont consultatifs.</p>';
}else{ 
echo '<p class="details">These reviews are advisory.</p>';
}
include("kitchen/inc/display_avis.php");
?>
		
		</div>
    </div><!-- #col avis -->
    <div class="col-md-4 col-sm-12 col-xs-12 "><!-- col centre -->




<?php

if($statut=="proposed" and $statut_vote=="open"){ include("kitchen/inc/inc_article_proposed.php"); }
if($statut=="proposed" and $statut_vote=="clos"){ include("kitchen/inc/inc_article_waiting.php"); }
if($statut=="accepted"){ include("kitchen/inc/inc_article_accepted.php"); }
if($statut=="declined"){ include("kitchen/inc/inc_article_declined.php"); }
if($statut=="published"){ include("kitchen/inc/inc_article_published.php"); }
if($statut=="unpublished"){ include("kitchen/inc/inc_article_unpublished.php"); }




?>



    	

    </div><!-- #col centre -->
    <div class="col-md-3 col-sm-12 col-xs-12"><!-- col votes -->



	<p class="txtcenter"><canvas id="votes" height="150" width="150" style="margin:0px;padding:0px;"></canvas></p>
	<script>
	var percent;
	percentyesyes = <?php echo $percent_yesyes; ?>;
	percentyes = <?php echo $percent_yes; ?>;
	percentno = <?php echo $percent_no; ?>;
	percentnono = <?php echo $percent_nono; ?>;
	percentDiff = 100-percentyesyes-percentyes-percentno-percentnono;
		var doughnutData = [
				{
			        value: percentnono,
			        color:"#fd0080", 
			        label: "Pas du tout d'accord"
			    },
			    {
			        value: percentno,
			        color: "#f1c40f",
			        label: "Moyennement d'accord"
			    },
			    {
			        value: percentyes,
			        color: "#3498db",
			        label: "Plutôt d'accord"
			    },
			    {
			        value: percentyesyes,
			        color: "#1abc9c",
			        label: "Tout à fait d'accord"
			    },
			    {
			        value: percentDiff,
			        color: "#ccc",
			        label: "vide"
			    }
			];
			var myDoughnut = new Chart(document.getElementById("votes").getContext("2d")).Doughnut(doughnutData,{segmentShowStroke : false, percentageInnerCutout : 60, animationSteps : 100, animationEasing : "easeOutBounce", animateRotate : true, animateScale : true});
    
   	</script>

		<div class="legendgraph" style="margin:10px 0px 0px 0px;padding:0px;">
<?php
if($lang=="fr"){ 
echo '<p><b>Votes du comité de rédaction</b></p>';
}else{ 
echo '<p><b>Votes of the Editorial Board</b></p>';
}
?>
		
		<div class="item yesyes">
		<?php echo $toutafaitdaccord; ?> <span class="cgrey3">-</span> <b id="yesyes"><?php echo $percent_yesyes; ?></b>%
		</div>
		<div class="item yes">
		<?php echo $daccord; ?> <span class="cgrey3">-</span> <b id="yes"><?php echo $percent_yes; ?></b>%
		</div>
		<div class="item no">
		<?php echo $plutotdefavorable; ?> <span class="cgrey3">-</span> <b id="no"><?php echo $percent_no; ?></b>%
		</div>
		<div class="item nono">
		<?php echo $absolumentdefavorable; ?> <span class="cgrey3">-</span> <b id="nono"><?php echo $percent_nono; ?></b>%
		</div>
<?php
if($lang=="fr"){ 
echo '<p class="details">Fonctionnement au consensus avec droit de véto. Un seul vote "Absolument défavorable" bloque la publication.</p>';
}else{ 
echo '<p class="details">Operation with the consensus veto. One vote "Absolutely unfavorable" to block publication.</p>';
}
include("kitchen/inc/display_votes.php");

?>
		
		</div>

	  
    </div><!-- #col votes -->
</div><!-- #row -->















<!-- ///////////////////////// AFFICHAGE ARTICLE //////////////// -->
<?php if($token_user_article==$token_user_online or $logged=="yes"){ /* if author or RC member */ ?>


<div class="row no-gutters"><!-- row -->

    <div class="col-sm-2 col-xs-12"><!-- col gauche -->

<div class="dateleft">
<?php if($signature!="" and $identity=="confirmed"){ ?>
<?php if($twauteur!=""){ ?><i class="fa fa-twitter"></i> <a href="https://twitter.com/<?php echo $twauteur; ?>" target="_blank"><?php }else{ ?><span class="auteur"><?php } ?>
<?php echo stripslashes($signature); ?>
<?php if($twauteur!=""){ ?></a><?php }else{ ?></span><?php } ?>
<?php } ?>
<span class="date"> - <time datetime="<?php echo $ladate; ?>" class="age"><?php echo $ladate; ?></time></span>


</div>


    </div><!-- #col gauche -->


    <div class="col-sm-10 col-xs-12"><!-- col droite -->

<article>

<?php if($hashtag!=""){ ?><span class="hashtag">#<?php echo $hashtag; ?></span><?php } ?>

<?php if(strlen($texte)<=700){ ?>
<h4><?php echo $title; ?></h4>
<?php }elseif(strlen($texte)>=701 and strlen($texte)<=1000){ ?>
<h3><?php echo $title; ?></h3>
<?php }else{ ?>
<h1><?php echo stripslashes($title); ?></h1>
<?php } ?>
<?php $description=strip_tags($description); ?>
<p class="post-meta"> 
<?php if($description!=""){ ?><span class="post-description"><?php echo stripslashes($description); ?></span><span class="divblack2"></span><?php } ?>

<?php
 
$sql_modifs = mysql_query("SELECT * FROM ".$prefix."articles_modifs WHERE id_article = '".$id_article."' ORDER BY id DESC LIMIT 1");
$nb_modifs = mysql_num_rows($sql_modifs);
if($nb_modifs!="0"){
	
	while($m = mysql_fetch_array($sql_modifs)) { 
	$date_m=date("c",strtotime($m['date']));
	$id_user_m=$m['id_user']; 
	$detail_m=$m['detail'];
		if($detail_m=="unpublish"){
		if($lang=="fr"){
		echo " - <span class='date'><span class='ctomato'><i class='fa fa-exclamation-triangle'></i> Dépublié </span> par <span class='auteur'>".prenomuser($id_user_m).", <time datetime='$date_m' class='age ctomato'>$date_m</time></span></span>";	
		}else{ 
		echo " - <span class='date'><span class='ctomato'><i class='fa fa-exclamation-triangle'></i> Unpublished </span> by <span class='auteur'>".prenomuser($id_user_m).", <time datetime='$date_m' class='age ctomato'>$date_m</time></span></span>";	
		}		
		}else{
		if($lang=="fr"){
		echo " - <span class='date'>Dernière modification: <span class='auteur'>".prenomuser($id_user_m).", <time datetime='$date_m' class='age'>$date_m</time></span>  ($detail_m)</span>";	
		}else{ 
		echo "<span class='date'>Last edit: <span class='auteur'>".prenomuser($id_user_m).", <time datetime='$date_m' class='age'>$date_m</time></span> ($detail_m)</span>";	
		}
		}
	}

}//endif($nb_modifs!="0")

?>

</p>
<span class="divblack2"></span>




<?php if(($statut=="proposed" and $token_user_online==$token_user_article and $num_votes==0) or ($statut!="declined" and $statut!="proposed" and $solidaire_user_online=="oui")){ ?>
<?php if($index_slug!="0" or $statut=="proposed"){ ?>
	<div class="padd10">


<?php
 if($thumb!="" and $statut=="published" and $image_width >= $min_image_width and $image_height >= $min_image_height and $solidaire_user_online=="oui" and $image_width!=""){ //compatible
$nomfichier="content/".$lang_dir."/edition.yml";
$actual_slug=$edition."/".$slug."/";
$file_handle = fopen($nomfichier, "r");
while (!feof($file_handle) ) {
  $line_of_text = fgets($file_handle);
  $parts = explode(': ', trim($line_of_text) );
  switch($parts[0]){
  case 'bypass_cache':
      $bypass_cache = $parts[1];
      break;
  case 'title':
      $title_edition = $parts[1];
      break;
  case 'featured':
      $type_featured = $parts[1];
      break;
  case 'bigpicture_url':
      $bigpicture_url = $parts[1];
      break;
}//end switch($parts[0])
}//end while (!feof($file_handle) )
fclose($file_handle);

if($bigpicture_url==$actual_slug){
?>
<?php if($lang=="fr"){ ?> 
<a href="kitchen/edit-article?tk=<?php echo $token; ?>&action=unfeatured" class="btn-article-small"><i class="fa fa-star-half"></i> Retirer de la Une</a>
<?php }else{  ?>
<a href="kitchen/edit-article?tk=<?php echo $token; ?>&action=unfeatured" class="btn-article-small"><i class="fa fa-star-half"></i> Remove from the head</a>
<?php } ?>
<?php
 }else{
?>
<?php if($lang=="fr"){ ?> 
<a href="kitchen/edit-article?tk=<?php echo $token; ?>&action=featured" class="btn-article-small"><i class="fa fa-star"></i> Mettre à la Une</a>
<?php }else{  ?>
<a href="kitchen/edit-article?tk=<?php echo $token; ?>&action=featured" class="btn-article-small"><i class="fa fa-star"></i> Put featured</a>
<?php } ?>
<?php
 }

 	 
 }//#compatible	 
 ?>



	<?php if($thumb=="") { ?>
		<?php if($lang=="fr"){ ?>
		<a href="<?php echo $http.$domain.$sandbox; ?>article_cover.php?tk=<?php echo $token; ?>" class="btn-article-small"><i class="fa fa-picture-o"></i> Ajouter une photo</a>
		<?php }else{  ?>
		<a href="<?php echo $http.$domain.$sandbox; ?>article_cover.php?tk=<?php echo $token; ?>" class="btn-article-small"><i class="fa fa-picture-o"></i> Add a picture</a>
		<?php } ?>
	<?php } /*endif $thumb=="" */ ?> 


	<?php if($thumb!=""){ ?>
		<?php if($lang=="fr"){ ?> 
		<a href="<?php echo $http.$domain.$sandbox; ?>article_cover.php?tk=<?php echo $token; ?>" class="btn-article-small"><i class="fa fa-picture-o"></i> Modifier la photo</a>
		<?php }else{  ?>
		<a href="<?php echo $http.$domain.$sandbox; ?>article_cover.php?tk=<?php echo $token; ?>" class="btn-article-small"><i class="fa fa-picture-o"></i> Modify the picture</a>
		<?php } ?>
	<?php }  /*endif $thumb!="" and $legende=="" */ ?>

	
		<?php if($lang=="fr"){ ?> 
		 <a href="kitchen/edit-article.php?tk=<?php echo $token; ?>" class="btn-article-small"><i class="fa fa-pencil-square-o"></i> Modifier le contenu</a>
 <?php if($statut=="published" and $solidaire_user_online=="oui"){ ?><a href="kitchen/edit-article.php?tk=<?php echo $token; ?>&action=del"  class="btn-delete-small">Dépublier</a><?php } ?>
		<?php }else{  ?>
		 <a href="kitchen/edit-article.php?tk=<?php echo $token; ?>" class="btn-article-small"><i class="fa fa-pencil-square-o"></i> Edit content</a>
		 <?php if($statut=="published" and $solidaire_user_online=="oui"){ ?><a href="kitchen/edit-article.php?tk=<?php echo $token; ?>&action=del"  class="btn-delete-small">Un-publish</a><?php } ?>
		<?php } ?>

	</div>
<span class="divblack2"></span>
<?php } /*fin index_slug */ ?>
<?php } ?>




<?php if($soundcloud!=""){ echo stripslashes($soundcloud); } ?>












<!-- PAGE THUMB -->
<?php if($thumb!="" and $legende!="" and $photographe!=""){ ?>
		<div class="row no-gutters"><!-- row -->
		  <div class="col-md-8 col-sm-8 col-xs-12"><!-- image -->
			<img src="<?php echo $http.$domain.$sandbox.$thumb; ?>" alt="" class="img-responsive">
		  </div><!-- #image -->
		  <div class="col-md-4 col-sm-4 col-xs-12"><!-- legende -->
			<!-- #LEGENDE -->
						<p class="legendethumb"><i class="fa fa-camera"></i> <?php echo $legende; ?>
				<br /><span>&copy; <?php echo $photographe; ?> - <?php echo $license; ?>
					</span>
				</p>			<!-- #LEGENDE -->
		  </div><!-- #legende -->
		</div><!-- #row -->
<span class="divblack"></span>
<?php } ?>
<!-- #PAGE THUMB -->




	
	
<!-- VIMEO 1 & 2 -->
<?php
if($vimeo1!="" or $vimeo2!=""){

if($vimeo1!="" and $vimeo2==""){
?>

<section class="article-onevideo">
			<div class="flex-video widescreen vimeo">
			<iframe src="https://player.vimeo.com/video/<?php echo $vimeo1; ?>?title=0&amp;amp;color=ffffff&amp;amp;byline=0&amp;amp;portrait=0" width="400" height="225" frameborder="0" webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen=""></iframe>
			</div>
</section>

<?php
}elseif($vimeo1!="" and $vimeo2!=""){
?>


	<section class="article-media">
	<div class="container-fluid"><!-- container-fluid -->
		<div class="row no-gutters"><!-- row -->
			<div class="col-md-6 col-sm-6 col-xs-12">
				<div class="flex-video widescreen vimeo">
				<iframe src="https://player.vimeo.com/video/<?php echo $vimeo1; ?>?title=0&amp;amp;color=ffffff&amp;amp;byline=0&amp;amp;portrait=0" width="400" height="225" frameborder="0" webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen=""></iframe>
				</div>
			</div>

			<div class="col-md-6 col-sm-6 col-xs-12">
				<div class="flex-video widescreen vimeo">
				<iframe src="https://player.vimeo.com/video/<?php echo $vimeo2; ?>?title=0&amp;amp;color=ffffff&amp;amp;byline=0&amp;amp;portrait=0" width="400" height="225" frameborder="0" webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen=""></iframe>
				</div>
			</div>
		</div><!-- #row -->
	</div><!-- #container-fluid --> 
	</section>


<?php
}//endif deux videos
?>
<?php
}//endif videos
?>
<!-- #VIMEO 1 & 2 -->







<!-- VIMEO 3 & 4 -->
<?php
if($vimeo3!="" or $vimeo4!=""){

if($vimeo3!="" and $vimeo4==""){
?>

<section class="article-onevideo">
			<div class="flex-video widescreen vimeo">
			<iframe src="https://player.vimeo.com/video/<?php echo $vimeo3; ?>?title=0&amp;amp;color=ffffff&amp;amp;byline=0&amp;amp;portrait=0" width="400" height="225" frameborder="0" webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen=""></iframe>
			</div>
</section>

<?php
}elseif($vimeo3!="" and $vimeo4!=""){
?>


	<section class="article-media">
	<div class="container-fluid"><!-- container-fluid -->
		<div class="row no-gutters"><!-- row -->
			<div class="col-md-6 col-sm-6 col-xs-12">
				<div class="flex-video widescreen vimeo">
				<iframe src="https://player.vimeo.com/video/<?php echo $vimeo3; ?>?title=0&amp;amp;color=ffffff&amp;amp;byline=0&amp;amp;portrait=0" width="400" height="225" frameborder="0" webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen=""></iframe>
				</div>
			</div>

			<div class="col-md-6 col-sm-6 col-xs-12">
				<div class="flex-video widescreen vimeo">
				<iframe src="https://player.vimeo.com/video/<?php echo $vimeo4; ?>?title=0&amp;amp;color=ffffff&amp;amp;byline=0&amp;amp;portrait=0" width="400" height="225" frameborder="0" webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen=""></iframe>
				</div>
			</div>
		</div><!-- #row -->
	</div><!-- #container-fluid --> 
	</section>


<?php
}//endif deux videos
?>
<?php
}//endif videos
?>
<!-- #VIMEO 3 & 4 -->


<?php if($vimeo1!="" or $vimeo2!="" or $vimeo3!="" or $vimeo4!=""){ ?><span class="divblack"></span><?php } ?>


<!-- YOUTUBE 1 & 2 -->
<?php
if($youtube1!="" or $youtube2!=""){

if($youtube1!="" and $youtube2==""){
?>
<div class="flex-video">
    <iframe width="420" height="315" src="https://www.youtube.com/embed/<?php echo $youtube1; ?>" frameborder="0" allowfullscreen></iframe>
</div>
<?php
}elseif($youtube1!="" and $youtube2!=""){
?>


	<section class="article-media">
	<div class="container-fluid"><!-- container-fluid -->
		<div class="row no-gutters"><!-- row -->
			<div class="col-md-6 col-sm-6 col-xs-12">
				<div class="flex-video">
				    <iframe width="420" height="315" src="https://www.youtube.com/embed/<?php echo $youtube1; ?>" frameborder="0" allowfullscreen></iframe>
				</div>
			</div>

			<div class="col-md-6 col-sm-6 col-xs-12">
				<div class="flex-video">
				    <iframe width="420" height="315" src="https://www.youtube.com/embed/<?php echo $youtube2; ?>" frameborder="0" allowfullscreen></iframe>
				</div>
			</div>
		</div><!-- #row -->
	</div><!-- #container-fluid --> 
	</section>


<?php
}//endif deux videos
?>
<?php
}//endif videos
?>
<!-- #YOUTUBE 1 & 2 -->





<!-- YOUTUBE 3 & 4 -->
<?php
if($youtube3!="" or $youtube4!=""){

if($youtube3!="" and $youtube4==""){
?>
<div class="flex-video">
    <iframe width="420" height="315" src="https://www.youtube.com/embed/<?php echo $youtube4; ?>" frameborder="0" allowfullscreen></iframe>
</div>
<?php
}elseif($youtube3!="" and $youtube4!=""){
?>


	<section class="article-media">
	<div class="container-fluid"><!-- container-fluid -->
		<div class="row no-gutters"><!-- row -->
			<div class="col-md-6 col-sm-6 col-xs-12">
				<div class="flex-video">
				    <iframe width="420" height="315" src="https://www.youtube.com/embed/<?php echo $youtube3; ?>" frameborder="0" allowfullscreen></iframe>
				</div>
			</div>

			<div class="col-md-6 col-sm-6 col-xs-12">
				<div class="flex-video">
				    <iframe width="420" height="315" src="https://www.youtube.com/embed/<?php echo $youtube4; ?>" frameborder="0" allowfullscreen></iframe>
				</div>
			</div>
		</div><!-- #row -->
	</div><!-- #container-fluid --> 
	</section>


<?php
}//endif deux videos
?>
<?php
}//endif videos
?>
<!-- #YOUTUBE 3 & 4 -->


<?php if($youtube1!="" or $youtube2!="" or $youtube3!="" or $youtube4!=""){ ?><span class="divblack"></span><?php } ?>






<?php if(strlen($texte)<=1000){ ?>
<div class="simplebrief">
<?php echo stripslashes($texte); ?>
<?php }else{ ?>
<div class="articlesimple">
<?php echo stripslashes($texte); ?>
</div>
<?php } ?>




</article>
    </div><!-- #col droite -->

</div><!-- #row -->

<?php  } /*end if author or RC member */ ?>

<?php
if($logged == 'yes'){ 
?>
<div class="row"> 
	<div class="timeline-centered">
		<?php include("kitchen/inc/inc_article_thread.php"); ?>
        <article class="timeline-entry begin">
            <div class="timeline-entry-inner">
                <div class="timeline-icon  bg-black" style="-webkit-transform: rotate(-90deg); -moz-transform: rotate(-90deg);">
                    <i class="entypo-flight"></i> +
                </div>
            </div>
        </article>
    </div>
</div><!-- .row -->

<?php } ?>

<?php
include("kitchen/inc/template_bottom.php");
?>