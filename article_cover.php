<?php
$title_page="Photo";
include("kitchen/inc/variables.php"); // ouverture des variables obligatoires
require("kitchen/inc/login.php");

    if($_GET['tk'] == ''){
        $errors[] = "Token missing.";
    }
$token = $_GET['tk'];




if($token != ''){
include("kitchen/inc/5qzneboiykjnkxfpl76ocs01zmue8x.php"); // ouverture de la base SQL
$sql_article = mysql_query("SELECT * FROM ".$prefix."articles WHERE token = '".$token."'");
if (mysql_num_rows($sql_article)==0) { $errors[] = "Wrong token."; }
else{  while($article = mysql_fetch_array($sql_article)) {
$statut=$article['statut'];
$thumb = $article['thumb']; $cut_url = substr($thumb, -9);
$legende = $article['thumb_legende']; 
$photographe = $article['thumb_photographer'];
$license = $article['thumb_license'];
$lang = $article['edition']; 
$token_user_article = $article['token_user']; }}

if($token_user_article!=$token_user_online and $solidaire_user_online!="oui"){  $errors[] = "Access denied, sorry."; }
if($token_user_article==$token_user_online and $solidaire_user_online!="oui"){ 
if($statut!="proposed"){ $errors[] = "Access granted only when article = proposed."; }
}
if($solidaire_user_online=="oui"){ 
if($statut!="published" and $statut!="proposed"){ $errors[] = "Access denied for the moment."; }
}


	
	if($_GET['del']=="yes" and ($statut=="proposed" or $statut_user_online=="admin") and count($errors) == 0){
	
	if($cut_url!="thumb.jpg"){ unlink('public/uploads/'.$thumb);  }else{ unlink($thumb); }
	mysql_query("UPDATE ".$prefix."articles SET thumb = '' WHERE token = '".$token."'");
	mysql_query("UPDATE ".$prefix."articles SET thumb_photographer = '' WHERE token = '".$token."'");
	mysql_query("UPDATE ".$prefix."articles SET thumb_license = '' WHERE token = '".$token."'");
	mysql_query("UPDATE ".$prefix."articles SET thumb_legende = '' WHERE token = '".$token."'");
	header('Location: article.php?tk='.$token);
	exit;
	}//ENDIF DELETE


if($statut=="published" and $solidaire_user_online=="oui"){ 
if($lang=="fr"){
	$messages[] = "Attention, cet article est déjà publié. Vous pouvez modifier la photo car vous êtes membre du collège solidaire, mais le changement sera visible sur le site public.";
	}else{ 
	$messages[] = "Careful, this article is already published. You can change the picture because you are a member of the solidarity college, but the change will be visible on the public site.";
	}
}

		
}//endif token






include("kitchen/inc/template_top.php"); // template
?>






<div id="upload-wrapper">




<form action="kitchen/inc/processupload.php" method="post" enctype="multipart/form-data" id="MyUploadForm">




<div class="row"><!-- row -->
  <div class="form-group">
    <div class="col-sm-4">

<?php if($thumb){ ?>
	<style>
	#blah{
	display: block;
	}
	</style>
<?php
if($cut_url=="thumb.jpg"){
?>
	<img id="blah" src="<?php echo $thumb; ?>" />
<?php }else{ ?>
	<img id="blah" src="<?php echo $thumb; ?>" />
<?php } ?>

<?php }else{ ?>
	<style>
	#blah{
	display: none;
	}
	</style>
	<img id="blah" src="#" />
<?php } ?>
<div id="imageInputWrap">
<input name="image_file" id="imageInput" type="file"  class="btn-article-small" required />
<input name="token" value="<?php echo $token; ?>" type="hidden" />
</div>

    </div>
    <div class="col-sm-4 txtright">
    <img src="public/images/ajax-loader.gif" id="loading-img" style="display:none;" alt="Please Wait"/>
<?php
if($lang=="fr"){ 
?>
    <div id="output">Taille recommandée <br />(pour mettre un article à la Une)<br /> 1280px / 720px</div>
<?php
}else{ 
?>
    <div id="output">Recommended size <br />to put the article featured: <br />1280px / 720px</div>
<?php
}
?>
    </div>
    <div class="col-sm-4 txtright">
	        <p><button type="submit" class="btn-article-small" id="submit-btn"><i class="fa fa-upload"></i> Upload</button>
    
    <button class="btn-article-small" id="cancel-btn" onclick="javascript:location.href='article.php?tk=<?php echo $token; ?>'"><i class="fa fa-undo"></i> <?php if($lang=="fr"){ ?>Annuler<?php }else{ ?>Cancel<?php } ?></button></p>

<?php if($thumb){ ?>
<?php if($lang=="fr"){ ?><div style="padding-top:5px;float:left;" id="delbutbut"><a href="article_cover.php?tk=<?php echo $token; ?>&del=yes" class="btn-delete-small">Supprimer</a></div><?php }else{ ?><div style="padding-top:5px;float:left;" id="delbutbut"><a href="article_cover.php?tk=<?php echo $token; ?>&del=yes" class="btn-delete-small">Delete</a></div><?php } ?>
<?php } ?>

    </div>
  </div>
</div><!-- #row -->



<hr />


<div class="row"><!-- row -->
  <div class="form-group">
    <label for="nom" class="col-sm-2 control-label"></label>
    <div class="col-sm-6">
      <span class="cgrey4"><i class="fa fa-level-down"></i> <?php if($lang=="fr"){ ?>Si les champs ci-dessous sont laissés vides, alors l'image téléchargée sera utilisée comme illustration sur Twitter et Facebook mais n'apparaitra pas sur la page de l'article.<?php }else{ ?>If the fields below are left empty, so the downloaded image will be used as illustration on Twitter and Facebook but will not appear on the page of the article.<?php } ?></span>
     
    </div>
  </div>
</div><!-- #row -->


<div class="row"><!-- row -->
  <div class="form-group">
    <label for="nom" class="col-sm-2 control-label"><?php if($lang=="fr"){ ?>Légende<?php }else{ ?>Caption<?php } ?></label>
    <div class="col-sm-6">
      <input type="text" class="form-control" id="legende" name="legende" value="<?php echo $legende; ?>" maxlength="250">
      <smaller>max 250 <?php if($lang=="fr"){ ?>caractères<?php }else{ ?>characters<?php } ?>. </smaller>
    </div>
  </div>
</div><!-- #row -->


<div class="row"><!-- row -->
  <div class="form-group">
    <label for="nom" class="col-sm-2 control-label"><?php if($lang=="fr"){ ?>Photographe<?php }else{ ?>Photographer<?php } ?></label>
    <div class="col-sm-6">
      <input type="text" class="form-control" id="photographe" name="photographe" value="<?php echo $photographe; ?>" maxlength="50">
      <smaller>max 50 <?php if($lang=="fr"){ ?>caractères<?php }else{ ?>characters<?php } ?>.</smaller>
    </div>
  </div>
</div><!-- #row -->

<div class="row"><!-- row -->
  <div class="form-group">
    <label for="prenom" class="col-sm-2 control-label"><?php if($lang=="fr"){ ?>Licence<?php }else{ ?>License<?php } ?></label>
    <div class="col-sm-6">
<select class="form-control" name="license">
  <option value="nothing">- -</option>
  <option value="copyright"<?php if($license == 'copyright'){ echo " selected"; } ?>>All rights reserved</option>
  <option value="courtesy"<?php if($license == 'courtesy'){ echo " selected"; } ?>>Courtesy</option>
  <option value="artlibre"<?php if($license == 'artlibre'){ echo " selected"; } ?>>Free-Art License</option>
  <option value="by"<?php if($license == 'by'){ echo " selected"; } ?>>Creative Commons BY</option>
  <option value="by-sa"<?php if($license == 'by-sa'){ echo " selected"; } ?>>Creative Commons BY-SA</option>
  <option value="by-nc"<?php if($license == 'by-nc'){ echo " selected"; } ?>>Creative Commons BY-NC</option>
  <option value="by-nc-sa"<?php if($license == 'by-nc-sa'){ echo " selected"; } ?>>Creative Commons BY-NC-SA</option>
  <option value="by-nc-nd"<?php if($license == 'by-nc-nd'){ echo " selected"; } ?>>Creative Commons BY-NC-ND</option>
</select>
    </div>
  </div> 
</div><!-- #row -->





<hr />















</form>


</div>







<?php
include("kitchen/inc/template_bottom.php"); // insert bas de page
?>




























